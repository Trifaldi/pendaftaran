<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="login page rich">
  <meta property="og:image" content="<?= base_url()?>assets/img/icon2.png" />
  <meta name="google-site-verification" content="OVBLj339q6UzvWhWkQK2mMq_XZPP1yYfPMlj1SlzfmQ" />
  <meta name="author" content="villacorp.systems">
  <noscript><meta http-equiv="refresh" content="1;url=<?= base_url()?>home/noscript"></noscript>
  <link rel="shortcut icon" href="<?= base_url()?>assets/img/icon2.png"/>

  <title>ERP Villacorp</title>

  <!-- Bootstrap Core CSS -->
  <link href="<?= base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="<?= base_url()?>assets/css/AdminLTE.min.css" rel="stylesheet">

  <!-- Login CSS -->
  <link rel="stylesheet" href="<?= base_url()?>assets/css/login.css"/>

  <!-- iCheck -->
  <link href="<?= base_url()?>assets/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css"/>

  <!-- Custom Fonts -->
  <link href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- jQuery Version 1.11.0 -->
  <script src="<?= base_url()?>assets/js/jquery-1.11.0.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?= base_url()?>assets/parsley/parsley.min.js"></script>
  <script src="<?= base_url()?>assets/parsley/i18n/id.js"></script>
  <style>
    .parsley-errors-list{
      color:red; list-style: none; margin: 0px; padding: 0px; padding-left:5px;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?= base_url()?>assets/js/html5shiv.js"></script>
  <script src="<?= base_url()?>assets/js/respond.min.js"></script>
<![endif]-->
</head>
<style>
  .col-container {
    display: table; /* Make the container element behave like a table */
    width: 100%; /* Set full-width to expand the whole page */
    height: 100vh;
  }

  body.login-page{
    background-position: center top;
    background-size:cover;
    background-repeat: no-repeat;
    background: #f1f1f1;
  }

  .login-box{
    width: 60%;
  }

  .col{
    vertical-align:middle;
    display: table-cell;
  }

  .col-mobile{
    display: none;
  }

  .text_color_1{
    color:orange;
    font-weight: bold;
    text-shadow: 1px 1px 2px black, 0 0 25px orange, 0 0 5px orange;
    font-size: 30px;
  }
  .text_color_2{
    color:#f1f1f1;
    font-weight: bold;
    text-shadow: 1px 1px 2px black, 0 0 25px orange, 0 0 5px orange;
    font-size: 30px;
  }

  ::selection {
    background: orange; /* WebKit/Blink Browsers */
    color:#fff;
  }
  ::-moz-selection {
    background: orange; /* Gecko Browsers */
    color:#fff;
  }
  a{
    color:orange;
  }
  a:hover{
    color:#222;
  }
  .form-control:focus {
    border-color: orange;
    box-shadow: none;
  }

  .form_login{
    padding:20px;margin-left:5px;
  }

  @media only screen and (max-width: 767px) {
    body.login-page {
    /* background-position: center;
    background-size:cover; */
    /* background-repeat: no-repeat; */
  }
  .login-box{
    width: auto;
  }

  #bg_log, .ring-content, #slides_mood_log{
    display: none;
  }

  .form_login{
    padding:0px;
  }
}

#bg_log, .ring-content, #slides_mood_log{
  display: block;
}

.ring-content{
  position: absolute;
  padding:10px;
  font-size: 20px;
  z-index: 2;
}

#bg_log{
  position: fixed;
  filter: brightness(50%);
  border-right:1px solid #fff;
  /* background: url(<?= base_url()?>assets/img/bg-login.jpg); */
  background-size:cover;
  height:100%;
}


#pesan_default, #pesan_default_2{
  padding-top:10px;padding-bottom:10px;text-align:left;
}
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select: none; /* Safari */
  -khtml-user-select: none; /* Konqueror HTML */
  -moz-user-select: none; /* Old versions of Firefox */
  -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
            supported by Chrome, Opera and Firefox */
          }
          img {
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -o-user-select: none;
            user-select: none;
          }
        </style>
        <link href="<?= base_url()?>assets/css/style-load.css" rel="stylesheet">
        <body class="login-page">
          <div class="col-container">
            <div class="ringx" hidden>
              <!-- Loading -->
              <div class="img"></div>
              <span id="txt_span"></span>
            </div>
            <div class="ringx-content" hidden>
            </div>
            <div class="col-sm-7 col-md-8 ring-content" hidden>
            </div>
            <div class="col-sm-7 col-md-8" id="bg_log" style="background-color: #222;">
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4" id="bg_log_2" style="position: absolute;right:0px;padding-top:0px;background:#f1f1f1;height:100%;z-index:3">
              <div class="form_login" style="min-height:90%">
                <center>
                  <!-- <img src="<?= base_url()?>assets/img/icon2.png" draggable="false" alt="" width="150" style="margin-bottom:-10px">  -->
                  <img class="img-thumbnail" src="<?= base_url()?>assets/img/villa_logo.png" alt="logo" draggable="false" style="background:none; border:none;margin-bottom:-30px">
                  <br>
                  <!-- <span class="text_color_1 noselect">VILLA </span><span class="text_color_2 noselect"> CORP</span> -->
                  <br><br>
                </center>
                <?php
                if (!empty($this->session->flashdata('failed'))) {
                  echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>' . $this->session->flashdata('failed'). '</div>';
                }
                ?>
                <form class="" action="<?php echo base_url()?>login/post_register" method="post" >
                  <div id="pesan_1"></div>
                  <div class="form-group has-feedback">
                    <span class="glyphicon glyphicon-user form-control-feedback" style="color: #d9d9d9;margin-top:8px;"></span>
                    <input onkeyup="this.value = this.value.toUpperCase()" required onkeypress='return harusHuruf(event)' type="text" name="fullname" value="<?php if(!empty(get_session('fullname'))){ echo get_session('fullname');}?>"   class="form-control" placeholder="Fullname ect: BAMBANG PAMUNGKAS NUGROHO" style="border-radius: 2px;height:50px;" autofocus/>
                    <input type="hidden" value="<?php  echo new_ns('VS'); ?>" name="uniqe" >
                  </div>
                  <div class="form-group has-feedback">
                    <span class="glyphicon glyphicon-phone form-control-feedback" style="color: #d9d9d9;margin-top:8px;"></span>
                    <input  minlength="10" maxlength="15" type="text" name="hp_1" id="no_hp" class="form-control" value="<?php if(!empty(get_session('hp_1'))){ echo get_session('hp_1');}?>" placeholder="Handphone" style="border-radius: 2px;height:50px;"  onkeypress='return angka(event)' >
                  </div>
                  <script type="text/javascript">
                    /*document.addEventListener("keydown", function(event) {
                      console.log(event.which);
                    });*/
                  </script>
                  <script type="text/javascript">
                    $('#no_hp').click(function(event) {
                      var val = $('#no_hp').val();
                      if(val==''){
                        $('#no_hp').val('08');
                      }

                    });

                    $('#no_hp').focus(function(event) {
                      $('#no_hp').val();
                    });

                    $("#no_hp").focusout(function(){
                      $('#no_hp').val();
                    });

                    $("#no_hp").keyup(function(){
                      var val = $('#no_hp').val();
                      //var valx = '085834246342';
                      var valx = val.replace(/-/gi, "");
                      console.log(valx);
                      arr = valx.match(/.{1,4}/g);

                      //bersihkan array jika ada yg undefined
                      var i = 0;
                      var string = "";
                      if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
                        string = arr[0];
                      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
                        string = arr[0]+"-"+arr[1];
                      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
                        string = arr[0]+"-"+arr[1]+"-"+arr[2];
                      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
                        string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
                      }


                      //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
                      console.log(string);
                      $('#no_hp').val('');
                      $('#no_hp').val(string);

                      if(val=='08'){
                       $('#no_hp').val('08');
                     }else if(val=='0'){
                       $('#no_hp').val('08');
                     }else if(val==''){
                       $('#no_hp').val('08');
                     }
                   });
                 </script>
                 <button onclick="phonenumber(document.form1.text1)" type="submit" class="btn btn-block btn-lg" style="border-radius: 2px; background: #DAA520; color:white">Sign In</button>
                 <a data-placement="left" data-toggle="tooltip"  data-target="#exampleModal" id="modal-release" title="Sudah Punya Akun, Silahkan Masuk Lagi" class="btn btn-lg pull-right tip unic" onclick="$('#exampleModal').modal('show')"><i class="fa fa-home" style="color: green;font-size: 50px;"></i></a>
               </form>
               <br>
                <!-- <button data-toggle="modal" data-target="#exampleModal" title="Sudah Punya Akun, Silahkan Masuk Lagi" id="modal-release" class="btn btn-lg">
                  <i class="fa fa-home" style="color: green;font-size: 50px;"></i>
                </button> -->

              </div>
              <div class="modal fade" id="modal-release" style="display: none;">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="judul_release" style="font-weight:bold;"></h4>
                      </div>
                      <div class="modal-body">
                        <div id="isi_release">

                        </div>
                      </div>
                      <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button> -->
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <script type="text/javascript">
                  function phonenumber(inputtxt)
                  {
                    return 0;
                  /*var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                  if((inputtxt.value.match(phoneno))
                        {
                      return true;
                        }
                      else
                        {
                        alert("message");
                        return false;
                      }*/
                    }
                  </script>
                  <script>
                    function harusHuruf(evt){
                      var charCode = (evt.which) ? evt.which : event.keyCode
                      if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
                        return false;
                      return true;
                    }

                    function angka(evt){
                      var charCode = (evt.which) ? evt.which : event.keyCode
                      if ((charCode < 48 || charCode > 57)&&charCode>32)
                        return false;
                      return true;
                    }
                  </script>
                  <script type="text/javascript">
                    function get_release(stt='')
                    {
                      if(stt==''){stt='new';}
                      get_loading(1);
                      $.ajax({
                        type: "POST",
                        url : '<?= base_url()?>auth/get_release/'+stt,
                        data: 'p=1',
                        cache: false,
                        dataType : 'json',
                        beforeSend: function()
                        {
                          $('#judul_release').html('<i class="fa fa-refresh fa-spin"></i> Loading . . .');
                          $('#isi_release').html('');
                        },
                        success: function(param){
                          $('#modal-release').modal('show');
                          get_loading();
                          if (stt=='new') {
                            $('#judul_release').html("Release "+param.versi);
                            AmbilData = param.deskripsi;
                            $.each(AmbilData, function(index, loaddata) {
                              $('#isi_release').append(loaddata+"<br />");
                            });
                          }
                        }
                      });
                    }
                  </script>
                  <style> a{ color:chocolate; } a:hover{ color:#222; } </style><p class="text-center" style="bottom:0px;text-align: center;width: 100%;"> Copyright &copy;2017-2019.  <b><a onclick="get_release();" style="cursor:pointer"></a></b>
                  </p>    </div>
                </div>

                <div class="modal fade" id="modal-loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="blackbg"></div>
                  <div class="loader"></div>
                </div>


                <script type="text/javascript">


                  function pesan(id,alert='',judul='',pesan='',html='')
                  {
                    if (alert=='') {
                      $('#pesan_'+id).html(pesan);
                    }else {
                      $('#pesan_'+id).html(''
                        +'<div class="alert alert-'+alert+' alert-dismissible" role="alert" style="padding-top:10px;padding-bottom:10px;text-align:left">'
                        +'   <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        +'     <span aria-hidden="true">&times;</span>'
                        +'   </button>'
                        +'   <strong>'+judul+'</strong> '+pesan
                        +'</div>'+html);
                    }
                  }

  //desktop
  un   = $('[name="identity"]');
  pass = $('[name="password"]');
  btn  = $('[name="btnlogin"]');
  //mobile
  un2   = $('.identity_2');
  pass2 = $('.password_2');
  btn2  = $('[name="btnlogin"]');
  v_cek(false);

  function v_cek(data)
  {
    un.attr('disabled',data);
    pass.attr('disabled',data);
    btn.attr('disabled',data);
    un2.attr('disabled',data);
    pass2.attr('disabled',data);
    btn2.attr('disabled',data);
  }

  function attr_hide(attr,val='')
  {
    if(val==''){val=5;}
    setTimeout(function(){
      attr.hide();
    }, val*1000);
  }

  $(document).ready(function(){
    if($("#pesan_default").length){
      attr_hide($("#pesan_default"));
      attr_hide($("#pesan_default_2"));
    }
    if($("#pesan_default_2").length){
      attr_hide($("#pesan_default"));
      attr_hide($("#pesan_default_2"));
    }

    get_quotes();
    function slidenya(quotes)
    {
      return '<div id="slides_mood_log" class="text_color_2" style="font-size: 30px;">'+quotes+'</div>';
    }

    function get_quotes()
    {
      var dataString = 'p=1';
      $.ajax({
        type: "POST",
        url : '<?= base_url('login/get_quotes'); ?>/log',
        data: dataString,
        cache: false,
        dataType : 'json',
        beforeSend: function()
        {
          $('.ring-content').html(slidenya('<i class="fa fa-refresh fa-spin"></i> Loading . . .'));
        },
        success: function(param){
          $('#bg_log').css('background-image', 'url(' + param.file[0] + ')');
          $('#bg_log_2, .login-page').css('background-image', 'url(' + param.file[1] + ')');
          $('.ring-content').html('');
          AmbilData = param.plus;
          $.each(AmbilData, function(index, loaddata) {
            $(".ring-content").append(slidenya(loaddata));
          });

          $('.ring-content').show();
          $('.ring-content #slides_mood_log:gt(0)').hide();
          setInterval(function(){
              // var x = window.matchMedia("(max-width: 767px)");
              // if (!x.matches) {
                $('.ring-content :first-child').fadeOut(2000).next('#slides_mood_log').fadeIn(2000)
                .end().appendTo('.ring-content');
              // }
            }, 21*1000);

        }
      });
    }

  });


  function cek_login(id)
  {
    if (id==1) {
      un   = $('[name="identity"]');
      pass = $('[name="password"]');
    }else {
      un   = $('.identity_2');
      pass = $('.password_2');
    }
    if (un.val()!='' && pass.val()!='') {
      ajax_cek(id);
    }
  }

  function ajax_cek(v_form)
  {
    $("#pesan_"+v_form).show();
    if (v_form==1) {
      un   = $('[name="identity"]');
      pass = $('[name="password"]');
    }else {
      un   = $('.identity_2');
      pass = $('.password_2');
    }
    $.ajax({
      type: "POST",
      data: "identity="+un.val()+"&password="+pass.val()+"&btnlogin=kirim",
      url: "<?= base_url('login/login_atemp'); ?>",
      cache: false,
      dataType: "JSON",
      beforeSend:function()
      {
       v_cek(true);
       pesan(v_form,'warning','<b>Loading . . . . </b>');
     },
     success: function(data){
       v_cek(false);
       if (data.stt=='1') {
         v_cek(true);
         pesan(v_form,data.alert,data.judul,data.pesan);
         window.location.href="<?= base_url(); ?>";
       }else {
         pesan(v_form,data.alert,data.judul,data.pesan);
         pass.val('');
         pass.focus();
         attr_hide($("#pesan_"+v_form),10);
       }
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
       v_cek(false);
       pesan(v_form,'danger','Error','Silahkan coba lagi! Terimakasih.');
       attr_hide($("#pesan_"+v_form),10);
     }
   });
  }

  function get_loading(stt='')
  {
    if (stt==0) {
      // console.log(0);
      $('.ringx, .ringx-content').hide();
      $('#modal-loading').modal('hide');
      $('.modal-backdrop').hide();
    }else{
      // console.log(1);
      $('.ringx, .ringx-content').show();
      $('#modal-loading > .loader').hide();
      $('#modal-loading').modal({show: true,backdrop: 'static',keyboard: false});
      $('.ringx-content #slides_mood:gt(0)').hide();
      setInterval(function(){
        $('.ringx-content :first-child').fadeOut(2000).next('#slides_mood').fadeIn(2000)
        .end().appendTo('.ringx-content');
      }, 21*1000);
    }
  }

  get_quotes_x();
  function slidenya_x(quotes)
  {
    return '<div id="slides_mood" class="text_color_2" style="font-size: 20px;padding:50px;">'+quotes+'</div>';
  }

  function get_quotes_x()
  {
    var dataString = 'p=1';
    $.ajax({
      type: "POST",
      url : '<?= base_url(); ?>login/get_quotes',
      data: dataString,
      cache: false,
      dataType : 'json',
      beforeSend: function()
      {
        $('.ringx-content').html('');
      },
      success: function(param){
        $('.ringx-content').html('');
        AmbilData = param.plus;
        $.each(AmbilData, function(index, loaddata) {
          $(".ringx-content").append(slidenya_x(loaddata));
        });
      }
    });
  }
</script>
<style>
  #bg-watermark{
    position:fixed; z-index:99999; background:rgba(0,0,0,0.8); display:block;
    padding:10px; padding-top:5px; margin-bottom: -15px; right:0; bottom:0; cursor: pointer;
  }
  #text-watermark{
    color:lightgrey; font-size:20px; cursor: pointer;
  }
</style>
<!--       <div id="bg-watermark" onclick="bg_wm()" class="noselect">
      <p id="text-watermark">
        <i class="fa fa-desktop"></i>&nbsp;192.168.9.7        <span id="h-wm"><br /></span>
        <i class="fa fa-database"></i>&nbsp;&nbsp;vs_villacorp_testing_2019      </p>
      </div> -->
      <script type="text/javascript">
        function setCookie(key, value, expiry) {
          var expires = new Date();
          expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
          document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        function getCookie(key) {
          var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
          return keyValue ? keyValue[2] : null;
        }

        function eraseCookie(key) {
          var keyValue = getCookie(key);
          setCookie(key, keyValue, '-1');
        }
        bg_wm(1);
        function bg_wm(aksi='')
        {
          bg = $('#bg-watermark');
          if (aksi=='1') {
            if (getCookie('watermark')==1) {
              $('#h-wm').html('<br />');
              $('#text-watermark').css('font-size',20);
            }else {
              $('#h-wm').html('&nbsp;');
              $('#text-watermark').css('font-size',14);
            }
          }else {
            if (getCookie('watermark')==0) {
              setCookie('watermark','1','1');
              bg.removeClass('min-wm');
              $('#h-wm').html('<br />');
              $('#text-watermark').css('font-size',20);
            }else {
              setCookie('watermark','0','1');
              bg.addClass('min-wm');
              $('#h-wm').html('&nbsp;');
              $('#text-watermark').css('font-size',14);
            }
          }
        }
      </script>
      <div class="modal fade" id="exampleModal" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"></button>
              <h4 class="modal-title"><label class="label label-warning">Silahkan Masuk Kembali</label></h4>
            </div>
            <div class="modal-body">
              <div class="validation-message" data-field="hp_1"></div>
              <form id="form-signin">
                <div class="form-group has-feedback">
                  <span class="glyphicon glyphicon-phone form-control-feedback" style="color: #d9d9d9;margin-top:8px;"></span>
                  <input minlength="10" maxlength="15" type="text" name="hp_1" id="no_hp2"  class="form-control" placeholder="Handphone" style="border-radius: 2px;height:50px; margin-top: 20px;"  onkeypress='return angka(event)' >
                </div>
                <script type="text/javascript">
                  $('#no_hp2').click(function(event) {
                    var val = $('#no_hp2').val();
                    if(val == ""){
                      $('#no_hp2').val('08');
                    }
                  });

                    // $('#no_hp2').focus(function(event) {
                    //   if(val == ""){
                    //     $('#no_hp2').val('08');
                    //   }
                    // });

                    // $("#no_hp2").focusout(function(){
                    //     if(val == ""){
                    //     $('#no_hp2').val('08');
                    //   }
                    // });

                    $("#no_hp2").keyup(function(){
                      var val = $('#no_hp2').val();
                      //var valx = '085834246342';
                      var valx = val.replace(/-/gi, "");
                      console.log(valx);
                      arr = valx.match(/.{1,4}/g);

                      //bersihkan array jika ada yg undefined
                      var i = 0;
                      var string = "";
                      if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
                        string = arr[0];
                      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
                        string = arr[0]+"-"+arr[1];
                      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
                        string = arr[0]+"-"+arr[1]+"-"+arr[2];
                      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
                        string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
                      }


                      //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
                      console.log(string);
                      $('#no_hp2').val('');
                      $('#no_hp2').val(string);

                      if(val=='08'){
                       $('#no_hp2').val('08');
                     }else if(val=='0'){
                       $('#no_hp2').val('08');
                     }else if(val==''){
                       $('#no_hp2').val('08');
                     }
                   });
                 </script>
                 <button id="sign-in" class="btn btn-block btn-lg" style="border-radius: 2px; background: #DAA520; color:white">Sign In</button>
               </form>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
      <script type="text/javascript">
        $('#sign-in').on("click", function() {
          login();
        });
        $("#form-signin").keypress(function(event) {
          if (event.which == 13) {
            login();
          }
        });

        function login() {
          $('#sign-in').html("Authenticating...").attr('disabled', true);
          var data = $('#form-signin').serialize();
          $.post("<?php echo base_url() . 'login/login_attempt'; ?>", data).done(function(data) {
            if (data.status == "success") {
              window.location.replace("<?php echo base_url('profil'); ?>");
            } else {
              $('#sign-in').html("Login").attr('disabled', false);
              $('.validation-message').html('');
              $('.validation-message').each(function() {
                for (var key in data) {
                  if ($(this).attr('data-field') == key) {
                    $(this).html(data[key]);
                  }
                }
              });
            }
          });
        }

        $(document).ready(function () {
          $('.tip').tooltip({
            container: 'body'
          });
    // $('.unic').tooltip('show');
    // setTimeout(function(){ $('.unic').tooltip('hide'); }, 5000);
  });
</script>
<script src="<?php echo base_url();?>assets/js/bootstrap-tooltip.js"></script>
</body>

</html>
