<?php
function get_nomor_surat($nomor_surat='',$kode='')
{
  $CI = &get_instance();
  if ($nomor_surat=='') {
    return 0; //null
  }else {
    $CI->db->limit(1);
    $data = $CI->db->get_where('a_nomor_surat',array('nomor_surat'=>$nomor_surat))->row();
    if (empty($data)) {
      return 0; //null
    }else {
      $tabel=$data->tabel; $id=$data->kolom;
      if ($tabel=='' || $id=='') { return 0; }
      $CI->db->limit(1);
      $cek = $CI->db->get_where($tabel,array($id=>$kode))->num_rows();
      if ($cek==0) {
        return 2; //belum ada
      }else {
        return 1; //sudah ada
      }
    }
  }
}

function new_ns($nomor_surat='',$max='')
{
  $CI = &get_instance();
  if($max==''){$max=13;} //default
  $max=$max-strlen($nomor_surat);
  if ($nomor_surat=='') {
    return 0;
  }else {
    $characters  = "0123456789";
    $characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    // $characters .= "abcdefghijklmnopqrstuvwxyz";
    $charactersLength = strlen($characters);
    // if (strlen($nomor_surat)==1) { $max=$max+1; }
    $randomString = '';
    for ($i = 0; $i < $max; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    $kode = $nomor_surat.$randomString;
    $cek_nomor = get_nomor_surat($nomor_surat,$kode);
    if ($cek_nomor==0) { //null
      return 0;
    }elseif($cek_nomor==1){ //jika sudah ada, buat baru lagi
      new_ns($nomor_surat,$max);
      // return 1;
    }else { //buat baru
      $CI->db->limit(1);
      $data_temp = $CI->db->get_where('a_ns_temp',array('ns'=>$kode))->row();
      if(empty($data_temp)){
        $CI->db->insert('a_ns_temp',array('ns'=>$kode));
        return $kode;
      }else {
        new_ns($nomor_surat,$max);
      }
    }
  }
}

?>
