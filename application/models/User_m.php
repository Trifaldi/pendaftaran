<?php

class User_m extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->library('datagrid');
	}

	/**
     * Check User Credentials
     *
     * @access 	public
     * @param 	
     * @return 	json(array)
     */
	
	public function attempt($input)
	{
		$query = $this->db->from('b_personal u')
						//->select('u.*, g.group_name')
						//->where('username', $input['username'])
						->where('hp_1',  $input['hp_1'])
						//->join('groups as g', 'g.id = u.id', 'left')
						->get();

		return $query->row();
	}

	/**
     * Get User by ID
     *
     * @access 	public
     * @param 	
     * @return 	json(array)
     */

	public function get_user($id)
	{
		$query = $this->db->from('b_personal u')
						//->select('u.*, g.group_name')
						->where('u.id', $id)
						//->join('groups as g', 'g.id = u.id', 'left')
						->get();

		return $query->row();
	}

	/**
     * Datagrid Data
     *
     * @access 	public
     * @param 	
     * @return 	json(array)
     */

	public function getJson($input)
	{
		$table  = 'b_personal as a';
		//$select = 'a.*, g.group_name';

		$replace_field  = [
			['old_name' => 'hp_1', 'new_name' => 'a.hp_1'],
			//['old_name' => 'group_name', 'new_name' => 'g.group_name']
		];

		$param = [
			'input'     => $input,
			//'select'    => $select,
			'table'     => $table,
			'replace_field' => $replace_field
		];

		$data = $this->datagrid->query($param, function($data) use ($input) {
			// return $data->join('groups as g', 'g.id = a.group_id', 'left')
			// 			->where('a.id !=', $this->session->userdata('active_user')->id);
		});

		return $data;
	}
	public function get($no_hp){
        $this->db->where('hp_1', $no_hp); // Untuk menambahkan Where Clause : username='$username'
        $result = $this->db->get('b_personal')->row(); // Untuk mengeksekusi dan mengambil data hasil query
        return $result;
    }

}