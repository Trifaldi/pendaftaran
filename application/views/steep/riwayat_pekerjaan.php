<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">


<form class="responsive-height" name="myForm" action="<?= base_url('riwayat_pekerjaan/create'); ?>" method="POST" style="
    background: white;">
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav animated bounceInLeft">
				<div class="step active complete" data-form="#form-1"></div>
				<div class="step active complete" data-form="#form-2"></div>
				<div class="step active complete" data-form="#form-3"></div>
				<div class="step active complete" data-form="#form-4"></div>
				<div class="step active complete" data-form="#form-5"></div>
				<div class="step active complete" data-form="#form-6"></div>
				<div class="step active complete" data-form="#form-7"></div>
				<div class="step active complete" data-form="#form-8"></div>
				<div class="step active complete" data-form="#form-9"></div>
				<div class="step active complete" data-form="#form-10"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
			</div>
		</div>

		<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

			<div class="row">
				<div class="col-sm-6 jarak_kanan animated bounceInRight">
					<div class="col-md-12">
						<div class="form-group">
							<label >Pekerjaan Terakhir</label>
							<select name="pekerjaan" class="form-control" id="mySelect" required onchange="myFunction(event)" >
								<option value="">Pilih</option>
								<option value="1">Ada</option>
								<option value="0">Belum ada</option>
							</select>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<div id="demo"></div>
					<div id="demo2"></div>
				</div>
				<div class="col-sm-6 jarak_kiri animated bounceInLeft">
					<p id="demo3"></p>
				</div>
			</div>


		</div>
	</div>
	<div class="content-box" style="margin-top: 160px;">
		<a href="<?php echo base_url(); ?>skill_bahasa_lain" class="btn btn-warning">Back</a>
		<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
	</div>
</form>
<script type="text/javascript">
	function myFunction(e) {
		var x = document.getElementById("mySelect").value;
		if (x == '1') {
			document.getElementById("demo").innerHTML='<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >Nama Perusahaan </label>'+
			'<input class="form-control" type="text"  name="perusahaan" placeholder="Nama Perusahaan" required="required">'+
			'<div class="help-block form-text text-muted form-control-feedback"></div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >Posisi</label>'+
			'<input class="form-control" type="text" name="posisi" placeholder="Posisi Di perusahaan Tersebut" required="required">'+
			'<div class="help-block form-text text-muted form-control-feedback">'+
			'</div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >Gaji</label>'+
			'<input class="form-control" type="text" placeholder="Gaji" name="gaji" required="required">'+
			'<div class="help-block form-text text-muted form-control-feedback">'+
			'</div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >Alasan Keluar</label>'+
			'<input class="form-control" type="text" placeholder="Alasan Keluar" name="alasan" required="required">'+
			'<div class="help-block form-text text-muted form-control-feedback">'+
			'</div>'+
			'</div>'+
			'</div>';
			document.getElementById("demo3").innerHTML='<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >Nama Atasan</label>'+
			'<input class="form-control" type="text" name="atasan" onkeypress="return angka(event)" required="required" >'+
			'<div class="help-block form-text text-muted form-control-feedback"></div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >No Hape Atasan</label>'+
			'<input class="form-control" maxlength="16" onclick="myNumberIbu()" id="hp_ibu" type="text" name="hp_atasan" placeholder="No Hape Atasan" data-mask="0000-0000-0000-0" required="required">'+
			'<div class="help-block form-text text-muted form-control-feedback">'+
			'</div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >HR Manager</label>'+
			'<input class="form-control" type="text"  name="hr_man" >'+
			'<div class="help-block form-text text-muted form-control-feedback">'+
			'</div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label >No Hp HR Manager</label>'+
			'<input class="form-control" type="text"  name="hp_hr">'+
			'<div class="help-block form-text text-muted form-control-feedback">'+
			'</div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12">'+
			'<div class="form-group">'+
			'<label>Uraian Tugas & Tanggung Jawab di Perusahaan sekarang / Terakhir</label>'+
			'<input class="form-control" type="text"  name="utugas" required="required">'+
			'<div class="help-block form-text text-muted form-control-feedback">'+
			'</div>'+
			'</div>'+
			'</div>';
		} else {
			document.getElementById("demo").innerHTML='';
			document.getElementById("demo3").innerHTML='';
		}
	}
</script>
<script>
	function myNumber() {
		document.getElementById("hp_ayah").value = "08";
	}
	function myNumberIbu() {
		document.getElementById("hp_ibu").value = "08";
	}
</script>

<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
