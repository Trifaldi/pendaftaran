<!DOCTYPE html>
<html class="app">
<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<meta content="template language" name="keywords">
	<meta content="John Doe" name="author">
	<meta content="Admin Template" name="description">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<link href="http://localhost/calon_karyawan/favicon.png" rel="shortcut icon">
	<link href="http://localhost/calon_karyawan/apple-touch-icon.png" rel="apple-touch-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/bootstrap/dist/css/bootstrap.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/batch-icons/style.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/dashicons/style.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/dripicons/webfont.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/eightyshades/style.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/foundation-icons/foundation-icons.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/metrize-icons/style.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/simple-line-icons/css/simple-line-icons.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/themify-icons/themify-icons.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/type-icons/style.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/weather-icons/css/weather-icons.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/animate/animate.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/fullcalendar/dist/fullcalendar.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/datatable/media/css/jquery.dataTables.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/datatable/media/css/dataTables.bootstrap4.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/dropzone/dist/dropzone.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/select2/dist/css/select2.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/bootstrap-daterangepicker/daterangepicker.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/fancybox/dist/jquery.fancybox.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/sweetalert/dist/sweetalert.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/exort/uploader.min.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/plugins/jquery-treegrid/jquery.treegrid.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/css/main.css"/>
	<link rel="stylesheet" href=" <?= base_url();?>template/css/custom.css"/>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/jquery/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/jquery-count-to/jquery.countTo.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/moment/min/moment.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/fullcalendar/dist/fullcalendar.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/chart.js/dist/Chart.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/datatable/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/datatable/media/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/dropzone/dist/dropzone.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/select2/dist/js/select2.full.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/bootstrap-validator/dist/validator.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/fancybox/dist/jquery.fancybox.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/disabler-enabler/disabler.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/disabler-enabler/enabler.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/super-datagrid/datagrid.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/exort/uploader.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/jquery-treegrid/jquery.treegrid.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/tether/dist/js/tether.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src=" <?= base_url();?>template/js/app.js"></script>
	<script type="text/javascript" src="<?= base_url();?>assets/datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<link rel="stylesheet" href="<?= base_url();?>assets/datetimepicker/bootstrap-datetimepicker.min.css"/>
</head>
<body>
	<style type="text/css">
		#myModal {
			width: 100%;
			height: 100%;
			position: fixed;
			background: rgba(0,0,0,.7);
			top: 0;
			left: 0;
			z-index: 9999;
		}
	</style>
	<style>
		#bg-watermark{
			position:fixed; z-index:99999; background:rgba(0,0,0,0.8); display:block;
			padding:10px; padding-top:4px; padding-bottom:4px; margin-bottom: 43%; right:0; top:0; cursor: pointer;
			margin-top:10px; padding:10px;
		}
		#text-watermark{
			color:black; font-size:20px; cursor: pointer;
		}
	</style>
	<style type="text/css">
		.select2-container--default .select2-selection--single {
			background-color: #fff;
			border: 1px solid #0000ff;
			border-top: none;
			border-left: none;
			border-right: none;
			border-radius: 0px;
		}
		.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
			background-color: #0275d8;
		}
		.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
		label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
		.form-group{margin-bottom:30px; margin-top: 15px;}
		::-webkit-input-placeholder { /* Edge */
			color: #a2a5a7;
		}
		:-ms-input-placeholder { /* Internet Explorer 10-11 */
			color: #a2a5a7;
		}
		::placeholder {
			color: #a2a5a7 !important;
		}
		.responsive-height{ min-height: 480px; margin-bottom: -10px;}
		@media screen and (max-width: 700px)
		{
			.responsive-height{ min-height: 400px; }
		}
		@media screen and (max-width: 800px)
		{
			.responsive-height{ min-height: 500px; }
		}
		.jarak_kiri{ padding-left:40px; }
		.jarak_kanan{ padding-right:40px; }
		@media screen and (max-width: 575px)
		{
			.jarak_kiri{ padding-left:0px;padding-right:0px; }
			.jarak_kanan{ padding-left:0px;padding-right:0px; }
		}
	</style>
	<script src=" <?= base_url();?>template/datepicker.min.js"></script>
	<script src=" <?= base_url();?>template/i18n/datepicker.en.js"></script>
	<div id="myModal" class="modal fade animated bounceInDown" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content" style="width: 1000px;right: 106px;top: 47px;border-top-width: 0px;margin-top: 0px;">
				<div class="modal-header" style="padding-top: 27px;border-bottom-width: 0px;">
					<button type="button" class="close" data-dismiss="modal"></button>
					<h4 class="modal-title" style="font-size: 29px;font-style: normal;color: red;"><label class="label label-warning" style="margin-right: 62px; font-size: 31px;">HAI, <?php echo $this->session->userdata('fullname') ?></label></h4>
				</div>
				<div class="modal-body" style="text-align: center;font-size: 29px;margin-left: 21px;margin-right: 8px;">
					<h4 style="text-align: center;">Anda akan melakukan registrasi kekaryawanan villacorp.</h4>
					<br>
					<p>Jika terjadi logout, login kembali dengan nomor handphone <label style = "color: red; font-size: 22px;"><?php echo $this->session->userdata('hp_1') ?> </label> yang sudah anda daftarkan.</p>
					<p>Dengan cara klik tombol home <a><i class="fa fa-home" style="color: green;font-size: 30px;"></i></a> pada form register.</p>
				</div>
				<div class="modal-footer" style=" padding-bottom: 20px; padding-top: 85px;">
					<button type="button" class="btn btn-success" data-dismiss="modal">NEXT</button>
				</div>
			</div>
		</div>
	</div>
	<div id="bg-watermark" onclick="bg_wm()" class="animated bounceInLeft" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;background: #5bc0de;">
		<p id="text-watermark" style="font-size: 20px;">
			<i class="fa fa-user" style="font-size: 20px;"></i>&nbsp;<span id="h-wm"><br></span>
	</div>
		<div class="wrapper" style="margin-top: 0px;" onclick="bg_wn()">
			<div class="main">
				<style type="text/css">
					.select2-container--default .select2-selection--single {
						background-color: #fff;
						border: 1px solid #0000ff;
						border-top: none;
						border-left: none;
						border-right: none;
						border-radius: 0px;
					}
					.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
						background-color: #0275d8;
					}
					.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
					label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
					.form-group{margin-bottom:30px; margin-top: 15px;}
					::-webkit-input-placeholder { /* Edge */
						color: #a2a5a7;
					}
					:-ms-input-placeholder { /* Internet Explorer 10-11 */
						color: #a2a5a7;
					}
					::placeholder {
						color: #a2a5a7 !important;
					}
					.responsive-height{ min-height: 480px; margin-bottom: -10px;}
					@media screen and (max-width: 700px)
					{
						.responsive-height{ min-height: 400px; }
					}
					@media screen and (max-width: 800px)
					{
						.responsive-height{ min-height: 500px; }
					}
					.jarak_kiri{ padding-left:40px; }
					.jarak_kanan{ padding-right:40px; }
					@media screen and (max-width: 575px)
					{
						.jarak_kiri{ padding-left:0px;padding-right:0px; }
						.jarak_kanan{ padding-left:0px;padding-right:0px; }
					}
				</style>
				<script src=" <?= base_url();?>template/datepicker.min.js"></script>
				<script src=" <?= base_url();?>template/i18n/datepicker.en.js"></script>
				<div class="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
					<?php $this->load->view($subview);?>
				</div>
			</div>
		</div>
			<script type="text/javascript">
				$('#myModal').modal('show');
			</script>
			<script type="text/javascript">
				function setCookie(key, value, expiry) {
					var expires = new Date();
					expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
					document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
				}
				function getCookie(key) {
					var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
					return keyValue ? keyValue[2] : null;
				}
				function eraseCookie(key) {
					var keyValue = getCookie(key);
					setCookie(key, keyValue, '-1');
				}
				bg_wm(1);
				function bg_wm(aksi='')
				{
					bg = $('#bg-watermark');
					if (aksi=='1') {
						if (getCookie('watermark')==1) {
			            $('#h-wm').html('<?php echo $this->session->userdata('nama') ?><br/><i class="fa fa-phone">&nbsp;&nbsp;</i><?php echo $this->session->userdata('no_hp')?></p>');
			            $('#text-watermark').css('font-size',20);
			          }else {
							$('#h-wm').html('&nbsp;');
							$('#text-watermark').css('font-size',14);
						}
					}else {
						if (getCookie('watermark')==0) {
							setCookie('watermark','1','1');
							bg.removeClass('min-wm');
							$('#h-wm').html('<?php echo $this->session->userdata('fullname') ?><br/><i class="fa fa-phone">&nbsp;&nbsp;</i><?php echo $this->session->userdata('hp_1')?></p>');
							$('#text-watermark').css('font-size',20);
						}else {
							setCookie('watermark','0','1');
							bg.addClass('min-wm');
							$('#h-wm').html('&nbsp;');
							$('#text-watermark').css('font-size',14);
						}
					}
				}
				bg_wn(0);
				function bg_wn(aksi='')
				{
					setCookie('watermark','0','1');
					bg.addClass('min-wm');
					$('#h-wm').html('&nbsp;');
					$('#text-watermark').css('font-size',14);
				}
			</script>
		</body>
		</html>
