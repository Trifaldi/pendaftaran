<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">


<form class="responsive-height" name="myForm" action="<?= base_url('gaji/create'); ?>" method="POST" style="background: white;" >
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav animated bounceInLeft">
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
			</div>
		</div>

		<div class="col-md-12 animated bounceInRight">
			<div class="form-group">
				<label >Bila diterima, berapa gaji ?</label>
				<div class="form-group">
					<input type="text" maxlength="9" data-mask="Rp.0.000,000 -," class="form-control" name="10harpgaji" required>	

				</div>
				
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
			<div class="form-group">
				<label >Fasilitas apa yang diharapkan ?</label>
				<div class="form-group">
					<input type="text" class="form-control" name="11fasilitas" required>	

				</div>
				
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	</div>
	<div class="content-box" style="margin-top: 272px;">
		<a href="<?php echo base_url(); ?>jeran" class="btn btn-warning">Back</a>
		<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
	</div>
</form>	
<script src="<?php echo base_url()?>assets/jsuites.js">

</script>
