<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saudara extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('M_personal');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->helper('url');
		$uniqe = $this->session->userdata('uniqe');
		$data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
		$data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
		$data ['saudara'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' AND fam_kategori = 4 ")->row();
		$data ['subview'] = 'steep/saudara';
		if ($data ['personal']->jumlah_saudara==0) {
			redirect(base_url('keluarga'));
		} else {
			$this->load->view('step',$data);
		}
		
	}

	public function get(){
		$data=$this->M_personal->get_saudara();
		echo json_encode($data);
	}
	public function save(){
		$data=$this->M_personal->save_saudara();

		echo json_encode($data);
	}
	public function delete(){
		$data=$this->M_personal->delete_product();
		echo json_encode($data);
	}
	// public function delete($id_fam)
 //   {
 //       $result = $this->db->delete('p_familiy', array('id_fam' => $id_fam));

 //      return $result;
 //      echo json_encode($result);
 //   }

}
