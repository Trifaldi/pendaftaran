<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('M_personal');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{

		$uniqe = $this->session->userdata('uniqe');
		$this->load->helper('url');
		$data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
		$data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
		$data ['subview'] = 'steep/gaji';
		$this->load->view('step',$data);

	}

	public function create()
	{	
		$check_id = $this->session->userdata('uniqe');
		$id_unix['unix'] = $this->db->query("SELECT * FROM p_uraian WHERE id_unix = '$check_id'")->row();
		$data = array(
			'id_unix'		=> $this->session->userdata('uniqe'),
			'10harpgaji' 		=> $this->input->post('10harpgaji'),
			'11fasilitas' 		=> $this->input->post('11fasilitas'),
		);
		if ($check_id == $id_unix['unix']->id_unix) {
			$insert = $this->db->update('p_uraian', $data, ['id_unix' => $check_id]);
		} else {
			$insert = $this->db->insert('p_uraian',$data);
		}
		
		if ($insert == true ) {
			redirect('kapan');
		}else{
			redirect('gaji');
		}
	}


}