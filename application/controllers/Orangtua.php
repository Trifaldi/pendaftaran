<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orangtua extends CI_Controller {

		public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->helper('url');
		$uniqe = $this->session->userdata('uniqe');
		$data['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
		$data['orangtua'] = $this->db->query("SELECT * FROM p_familiy  WHERE id_unix = '$uniqe'")->row();
		$data['ayah'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' AND fam_ket = 'Ayah' ")->row();
		$data['ibu'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' AND fam_ket = 'Ibu' ")->row();
		$data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
		if ($data['ayah']->id_unix == $data ['personal']->uniqe) {
			$data ['subview'] = 'steep/orangtua2';
		}else {
			$data ['subview'] = 'steep/orangtua';
		}
		
		$this->load->view('step',$data);
	}

	function create(){
	$uniqe = $this->session->userdata('uniqe');
			// Ambil data yang dikirim dari form
	$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
	$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
	$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
	$fam_sttus = $_POST['fam_sttus'];
	$fam_jekel = $_POST['fam_jekel'];
	$fam_no_hp = $_POST['fam_no_hp'];
	$fam_usia = $_POST['fam_usia'];
	$fam_pen = $_POST['fam_pen'];
	$fam_job = $_POST['fam_job'];
	$fam_per = $_POST['fam_per'];
	$fam_kategori = $_POST['fam_kategori']; // Ambil data alamat dan masukkan ke variabel alamat
	$data = array();

	$x = 0;
	while($x<count($fam_nama)){

		array_push($data, array(
			'id_unix'=> $id_unix[$x],
			'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
			'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
			'fam_sttus'=> $fam_sttus[$x],
			'fam_jekel'=> $fam_jekel[$x],
			'fam_no_hp'=> $fam_no_hp[$x],
			'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
			'fam_pen'=> $fam_pen[$x],
			'fam_job'=> $fam_job[$x],
			'fam_per'=> $fam_per[$x],
			'fam_kategori'=> $fam_kategori[$x],
		));
		$x++;
	}


	$this->load->model('M_personal');
	$this->db->query(" UPDATE b_personal SET proses_status = 1 WHERE uniqe = '$uniqe' ");
	//if ($uniqe !== $id_unix) {
	$insert = $this->M_personal->save_batch($data);

	if ($insert == true ) {
		redirect('saudara');
	}

}
	function update(){
		//$uniqe = $this->session->userdata('uniqe');
				// Ambil data yang dikirim dari form
		$fam_kategori = $_POST['fam_kategori'];
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
		$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$fam_sttus = $_POST['fam_sttus'];
		$fam_jekel = $_POST['fam_jekel'];
		$fam_no_hp = $_POST['fam_no_hp'];
		$fam_usia = $_POST['fam_usia'];
		$fam_pen = $_POST['fam_pen'];
		$fam_job = $_POST['fam_job'];
		$fam_per = $_POST['fam_per']; // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();

		$x = 0;
		foreach($fam_kategori as $dataid_fam){
			array_push($data, array(
				'fam_kategori'=> $dataid_fam,
				'id_unix'=> $id_unix[$x],
				'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'fam_sttus'=> $fam_sttus[$x],
				'fam_jekel'=> $fam_jekel[$x],
				'fam_no_hp'=> $fam_no_hp[$x],
				'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
				'fam_pen'=> $fam_pen[$x],
				'fam_job'=> $fam_job[$x],
				'fam_per'=> $fam_per[$x],
			));
			$x++;
		}
		$this->db->where_in('id_unix',$id_unix);
		$update = $this->db->update_batch('p_familiy', $data,'fam_kategori');
		if ($update == true ) {
			redirect('saudara');
		}else{
			redirect('saudara');
		}
	}

}
