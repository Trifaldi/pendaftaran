<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluarga extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
    $this->load->model('M_personal');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{
		$uniqe = $this->session->userdata('uniqe');
    $this->load->helper('url');
    $data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
    $data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
    $data ['subview'] = 'steep/keluarga';
    if ($data ['personal']->marital==1) {
      redirect(base_url('emergency'));
    } else {
  		$this->load->view('step',$data);
    }
	}

  public function pasangan()
  {
    $uniqe = $this->session->userdata('uniqe');
    $this->load->helper('url');
    $data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
    $data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
    $this->load->view('steep/pasangan',$data);
  }

    public function anak()
  {

    $uniqe = $this->session->userdata('uniqe');
    $this->load->helper('url');
    $data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
    $data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
    $this->load->view('steep/anak',$data);
  }

  public function get(){
  $data=$this->M_personal->get_keluarga();
  echo json_encode($data);
  }
  public function save(){
  $data=$this->M_personal->save_keluarga();

  echo json_encode($data);
  }
  public function delete(){
  $data=$this->M_personal->delete_product();
  echo json_encode($data);
  }

}
