<div class="content-box" class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px; ">
	<div class="col-md-12">
		<div class="form-wizard-nav animated bounceInLeft">
			<div class="step active complete" data-form="#form-1"></div>
			<div class="step active complete" data-form="#form-2"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
		</div>
	</div>

	<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">
		<form class="responsive-height" name="myForm" action="<?= base_url('sosmed/create'); ?>" method="POST" id="form-user1">
			<div class="row">
				<div class="col-sm-6 jarak_kanan">
					<div class="col-md-12">
							<div class="form-group">
								<label >Gol. Darah</label>
								<select class="form-control" name="darah" name="darah" required="required">
									<option></option>
									<option <?php $darah='O';if($darah==$personal->darah){echo 'selected="selected"';} ?> value="O">O</option>
									<option <?php $darah='A';if($darah==$personal->darah){echo 'selected="selected"';} ?> value="A">A</option>
									<option <?php $darah='B';if($darah==$personal->darah){echo 'selected="selected"';} ?> value="B">B</option>
									<option <?php $darah='AB';if($darah==$personal->darah){echo 'selected="selected"';} ?> value="AB">AB</option>
									<option <?php $darah='Tidak Tahu';if($darah==$personal->darah){echo 'selected="selected"';} ?> value="Tidak Tahu">Tidak Tahu</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Email</label>
								<input class="form-control" type="email" name="email"  value="<?php echo $personal->email ?>" >
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Telegram</label>
								<input class="form-control" onkeypress="return angka(event)" type="text" name="telegram"  value="<?php echo $personal->telegram ?>" >
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Facebook</label>
								<input class="form-control" type="text" name="facebook" value="<?php echo $personal->facebook ?>">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Twitter</label>
								<input class="form-control" type="text" name="twitter"  value="<?php echo $personal->twitter ?>" >
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Instagram</label>
								<input class="form-control" type="text" name="instagram" value="<?php echo $personal->instagram ?>" >
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
				</div>
				<div class="col-sm-6 jarak_kiri">

					<div class="col-md-12">
	<div class="form-group">
		<label>Kendaraan Yang Di Miliki</label>
		<select class="form-control" name="kendaraan" required="">
			<option></option>
			<option<?php $kendaraan ='Tidak Punya';if($kendaraan==$personal->kendaraan){echo' selected="selected"';}?> value="Tidak Punya">Tidak Punya</option>
			<option<?php $kendaraan ='Motor';if($kendaraan==$personal->kendaraan){echo' selected="selected"';}?> value="Motor">Motor</option>
			<option<?php $kendaraan ='Mobil';if($kendaraan==$personal->kendaraan){echo' selected="selected"';}?> value="Mobil">Mobil</option>
			<option<?php $kendaraan ='Motor dan Mobil';if($kendaraan==$personal->kendaraan){echo' selected="selected"';}?> value="Motor dan Mobil">Motor dan Mobil</option>
		</select>
		<div class="help-block form-text text-muted form-control-feedback"></div>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<label>Apakah Anda Mempunya Saudara</label>
		<select class="form-control" name="jumlah_saudara" required>
			<option value="">Pilih..</option>
			<option <?php $saudara ='0';if($saudara==$personal->jumlah_saudara){echo' selected="selected"';}?> value="0">Tidak</option>
			<option <?php $saudara ='1';if($saudara==$personal->jumlah_saudara){echo' selected="selected"';}?> value="1">Ya</option>
		</select>
		<!-- <input type="text" onkeypress="return angka(event)" min="0" max="6" maxlength="1" maxlength="0" name="jumlah_saudara" class="form-control" required> -->
		<div class="help-block form-text text-muted form-control-feedback"></div>
	</div>
</div>
<div id="my_photo_booth">
	<div id="my_camera"></div>
	<!-- First, include the Webcam.js JavaScript Library -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/webcamjs-master/webcam.min.js"></script>
	<!-- Configure a few settings and attach camera -->
	<script language="JavaScript">
		Webcam.set({
				// live preview size
				width: 300,
				height: 200,

				// device capture size
				dest_width: 600,
				dest_height: 400,

				// final cropped size
				crop_width: 400,
				crop_height: 400,

				// format and quality
				image_format: 'jpeg',
				jpeg_quality: '90%',

				// flip horizontal (mirror mode)
				flip_horiz: true
			});
		Webcam.attach( '#my_camera' );
	</script>
	<!-- A button for taking snaps -->
	<!-- <form> -->
		<div id="pre_take_buttons">
			<!-- This button is shown before the user takes a snapshot -->
			<input type=button value="Take Snapshot" onClick="preview_snapshot()">
		</div>
		<div id="post_take_buttons" style="display:none">
			<!-- These buttons are shown after a snapshot is taken -->
			<input type=button value="&lt; Take Another" onClick="cancel_preview()">
			<input type=button value="Save Photo &gt;" onClick="save_photo()" style="font-weight:bold;">
		</div>
		<!-- </form> -->
	</div>
	<div id="results" style="display:none">
		<!-- Your captured image will appear here... -->
	</div>
	<!-- Code to handle taking the snapshot and displaying it locally -->
	<script language="JavaScript">
		// preload shutter audio clip
		var shutter = new Audio();
		shutter.autoplay = false;
		shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

		function preview_snapshot() {
			// play sound effect
			try { shutter.currentTime = 0; } catch(e) {;} // fails in IE
			shutter.play();

			// freeze camera so user can preview current frame
			Webcam.freeze();

			// swap button sets
			document.getElementById('pre_take_buttons').style.display = 'none';
			document.getElementById('post_take_buttons').style.display = '';
		}
		function cancel_preview() {
			// cancel preview freeze and return to live camera view
			Webcam.unfreeze();

			// swap buttons back to first set
			document.getElementById('pre_take_buttons').style.display = '';
			document.getElementById('post_take_buttons').style.display = 'none';
		}
		function save_photo() {
			// actually snap photo (from preview freeze) and display it
			Webcam.snap( function(data_uri) {
				// display results in page
				document.getElementById('results').innerHTML =
				'<h2>Here is your large, cropped image:</h2>' +
				'<img src="'+data_uri+'"/><br/></br>' +
				'<input type="file" value="'+data_uri+'" name="foto">';

				Webcam.upload( data_uri, 'upload.php', function(code, text) {} );

                // tampilkan hasil gambar yang telah di ambil
                document.getElementById('hasil').innerHTML =
                '<p>Hasil : </p>' +
                '<img src="'+data_uri+'"/>';
				// shut down camera, stop capturing
				Webcam.reset();

				// show results, hide photo booth
				document.getElementById('results').style.display = '';
				document.getElementById('my_photo_booth').style.display = 'none';

			});

		}

	</script>


				</div>
			</div>
			<div class="content-box-footer">
				<a href="<?php echo base_url(); ?>profil" class="btn btn-warning">Back</a>
				<button  class="btn btn-success pull-right submit" id="simpan_gambar" type="submit" name="submit">Next</button>
			</div>
		</form>
				<script type="text/javascript">
					$(document).ready(function(){
						$("#simpan_gambar").click(function(){
							Webcam.snap( function(data_uri) {
								Webcam.upload( data_uri, 'sosmed/uploud', function(code, text) {

								} );
				                // tampilkan hasil gambar yang telah di ambil
				                Webcam.unfreeze();
				            } );
						});
					});
				</script>
					</div>

					<script>
										function harusHuruf(evt){
											var charCode = (evt.which) ? evt.which : event.keyCode
											if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
												return false;
											return true;
										}
										function angka(evt){
											var charCode = (evt.which) ? evt.which : event.keyCode
											if (charCode <33 || charCode > 57) {
												return false;
											}
											return true;
										}
									</script>
