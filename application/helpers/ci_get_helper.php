<?php
///START SESSION
function set_session($nama='',$key='') //add or update session
{ $CI = &get_instance();
  if ($nama!='') { $CI->session->set_userdata($nama, $key); }
}

function get_session($ket='') //get session
{ $CI = &get_instance();
  if ($ket!='') { return $CI->session->userdata($ket); }
}

function del_session($ket='') //remove session
{ $CI = &get_instance();
  if ($ket!='') { $CI->session->has_userdata($ket); }
}

function del_all_session() //remove all session
{ $CI = &get_instance();
  $CI->session->sess_destroy();
}
//END SESSION


//START POST
function get_post($ket,$stt='')
{ $CI = &get_instance();
  if ($stt=='1') {
    return $CI->input->post($ket);
  }else {
    return htmlentities(strip_tags($CI->input->post($ket)));
  }
}

function get_post_all($no_post='')
{ $CI = &get_instance();
  $post = array();
  if (!is_array($no_post)) { $no_post = array($no_post); }
  // log_r($no_post);
  foreach ( $_POST as $key => $value )
  {
    if(!in_array($key,$no_post)){
      $post[$key] = get_post($key);
    }
  }
  return $post;
}
//END POST

//START MODEL
function get_model($model,$func='',$data='')
{ $CI = &get_instance();
  return $CI->$model->$func($data);
}
//END MODEL

//START GET FIELD
function get($tbl='',$where_arr='')
{ $CI = &get_instance(); $get='';
  if ($tbl!='') {
    if ($where_arr!='') {
      foreach ($where_arr as $key => $value) {
        $CI->db->where($key,$value);
      }
    }
    $get = $CI->db->get($tbl);
  }
  return $get;
}

function field($tbl,$field,$where_arr='')
{ $CI = &get_instance();
  $CI->db->select($field);
  if ($where_arr!='') {
    foreach ($where_arr as $key => $value) {
      $CI->db->where($key,$value);
    }
  }
  $v = $CI->db->get($tbl);
  return $v;
}

function get_field($tbl,$where_arr='')
{ $CI = &get_instance();
  $fields = $CI->db->list_fields($tbl);
  $field_ar = array();
  foreach ($fields as $field)
  {
    $field_ar [$field] = '';
    if ($where_arr!='') {
      $data=field($tbl,$field,$where_arr);
      if ($data->num_rows()!=0) {
        $field_ar [$field] = $data->row()->$field;
      }
    }
  }
  return $field_ar;
}
//END GET FIELD

//START CRUD
function add_data($tbl,$data,$batch='')
{ $CI = &get_instance();
  if ($batch=='') {
    return $CI->db->insert($tbl,$data);
  }else {
    return $CI->db->insert_batch($tbl,$data);
  }
}
function update_data($tbl,$data,$d1='')
{ $CI = &get_instance();
  return $CI->db->update($tbl,$data,$d1);
}
function delete_data($tbl,$where)
{ $CI = &get_instance();
  return $CI->db->delete($tbl,$where);
}
//END CRUD


//======================== UPLOAD =============================
function upload_config($path='',$size='',$tipe='')
{ $CI = &get_instance();
  if($path==''){$path='assets/uploads';} if($tipe==''){$tipe='*';} if($size==''){$size=1;}
  $file_size = 1024 * $size;
  $CI->upload->initialize(array(
    "upload_path"   => "./$path",
    "allowed_types" => "$tipe",
    "max_size" => "$file_size",
    "remove_spaces" => TRUE,
    "encrypt_name" => TRUE,
  ));
}

function upload_file($filename,$path='',$url='',$file='')
{ $CI = &get_instance();
  if($path==''){$path='assets/uploads';}
  if ($_FILES[$filename]['error'] <> 4) {
      if ( $CI->upload->do_upload($filename))
      {
        if ($file!='') {
          if (file_exists($path."/$file")) {
            unlink($file);
          }
        }
          $uploadData = $CI->upload->data();
          $filename = "$path/".$uploadData['file_name'];
          $file = preg_replace('/ /', '_', $filename);
          return $file;
      }else {
        $error = $CI->upload->display_errors();
        pesan('danger','msg','Gagal!',$error,"$url");
      }
  }else {
    return $file;
  }
}
// ============================================================

//======================== TANGGAL ============================
function tgl_now($aksi='')
{
 date_default_timezone_set('Asia/Jakarta');
   if ($aksi=='tgl') {
      $v = date('Y-m-d');
   }elseif ($aksi=='jam') {
      $v = date('H:i:s');
   }elseif ($aksi=='x') {
      $v = date('YmdHis');
   }else {
      $v = date('Y-m-d H:i:s');
   }
   return $v;
}

function tgl_id($date)
{
 date_default_timezone_set('Asia/Jakarta');
   $str = explode('-', $date);
   $hasil = $str['0'] . " " . bln_id($str[1]) . " " .$str[2];
   return $hasil;
}

function bln_id($bln)
{
  date_default_timezone_set('Asia/Jakarta');
    $bulan = array(
      '01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni',
      '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember',
    );
  return $bulan[$bln];
}

function hari_id($tanggal)
{
  $day = date('D', strtotime($tanggal));
  $dayList = array( 'Sun'=>'Minggu', 'Mon'=>'Senin', 'Tue'=>'Selasa', 'Wed'=>'Rabu', 'Thu'=>'Kamis', 'Fri'=>"Jum'at", 'Sat'=>'Sabtu');
  return $dayList[$day];
}

function waktu($data)
{
  $tgl_n = date('d-m-Y H:i:s',strtotime($data));
  $hari = hari_id($tgl_n);
  $tgl  = tgl_id($tgl_n);
  return $hari.", ".$tgl;
}

function tgl_format($tgl='',$format='')
{
  if ($tgl=='') { $tgl = tgl_now(); }
  if ($format!='') {
    return date($format,strtotime($tgl));
  }else {
    return $tgl;
  }
}
//=============================================================================

function get_uri($no='')
{ $CI = &get_instance();
  if ($no!='') {
    return $CI->uri->segment(preg_replace("/[^0-9]/", "",$no));
  }else {
    return '';
  }
}

function get_last_query($stt='')
{ $CI = &get_instance();
  if ($stt!='') {
    $CI->db->last_query();
  }else {
    log_r($CI->db->last_query());
  }
}

function set_memory($no='')
{
  if ($no=='') { $no='-1'; }
  ini_set('memory_limit', '-1');
}

function sql_comment($tbl='',$field='',$db='')
{ $CI = &get_instance();
  if ($db=='') { $db=$CI->db->database; }
  if ($tbl=='') { return ''; }
  $CI->db->select('COLUMN_COMMENT AS ket');
  $CI->db->where('TABLE_SCHEMA',$db);
  $CI->db->where('TABLE_NAME',$tbl);
  if ($field!='') { $CI->db->where('COLUMN_NAME',$field); }
  $cek = $CI->db->get("INFORMATION_SCHEMA.COLUMNS");
  if (!empty($cek)) {
    if ($field=='') {
      return $cek->result();
    }else {
      return $cek->row()->ket;
    }
  }else {
    return '';
  }
}


//START PESAN
function pesan($alert,$msg,$judul='',$isi='',$url='',$html='')
{ $CI = &get_instance();
  $CI->session->set_flashdata($msg,
    '
    <div class="alert alert-'.$alert.' alert-dismissible" role="alert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;&nbsp;</span>
       </button>
       <strong>'.$judul.'</strong> '.$isi.'
    </div>
    '.$html.''
  );
  return redirect($url);
}

function get_pesan($msg)
{ $CI = &get_instance();
  echo $CI->session->flashdata($msg);
}

function pesan_alert($alert,$msg,$judul='',$isi='')
{
  return '<div class="alert alert-'.$alert.' alert-dismissible" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;&nbsp;</span>
     </button>
     <strong>'.$judul.'</strong> '.$isi.'
   </div>';
}
//END PESAN

?>
