<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->helper('url');
		$uniqe = $this->session->userdata('uniqe');
		$data ['martial'] = $this->db->query('SELECT * FROM b_payroll_ptkp_setahun')->result();
		$data ['kota'] = $this->db->query('SELECT * FROM kota_kota ORDER BY kota ASC;')->result();
		$data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
		$data ['subview'] = 'steep/profil';

		if ($data ['personal']->no_ktp == null) {
			$this->load->view('welcome_message',$data);
		}else{
			$this->load->view('step',$data);
		}
		
	}
		public function create()
	{		

		$uniqe = $this->session->userdata('uniqe');
		$data = array(
			'fullname' 		=> $this->input->post('fullname'), 
			'no_ktp'  		=> $this->input->post('no_ktp'),
			'place_birth'  	=> $this->input->post('place_birth'),
			'date_birth'  	=> $this->input->post('date_birth'),
			'jenis_kelamin'	=> $this->input->post('jenis_kelamin'),
			'address'  		=> $this->input->post('address'),
			'hp_1' 			=> $this->input->post('hp_1'),
			'hp_2' 			=> $this->input->post('hp_2'),
			'religion'  	=> $this->input->post('religion'),
			'marital'  		=> $this->input->post('marital'),
			'hobby' 		=> $this->input->post('hobby'),
			'kacamata'   	=> $this->input->post('kacamata')
		);
		//log_r($data);
		$this->load->model('M_personal');
		$update = $this->M_personal->update($data,$uniqe);
		
		if ($update == true ) {
			redirect('sosmed');
		}
	}

}