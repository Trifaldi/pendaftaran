<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat_pekerjaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('M_personal');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{

		$uniqe = $this->session->userdata('uniqe');
		$this->load->helper('url');
		$data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
		$data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
		$data ['subview'] = 'steep/riwayat_pekerjaan';
		$this->load->view('step',$data);

	}

	public function create()
	{	
		$check_id = $this->session->userdata('uniqe');
		$id_unix['unix'] = $this->db->query("SELECT * FROM p_education WHERE id_unix = '$check_id'")->row();
		$data = array(
			'id_unix'		=> $this->session->userdata('uniqe'),
			'pekerjaan' 		=> $this->input->post('pekerjaan'), 
			'perusahaan'  	=> $this->input->post('perusahaan'),
			'posisi'  	=> $this->input->post('posisi'),
			'gaji'  	=> $this->input->post('gaji'),
			'alasan'	=> $this->input->post('alasan'),
			'atasan'  		=> $this->input->post('atasan'),
			'hp_atasan' 	=> $this->input->post('hp_atasan'),
			'hr_man' 	=> $this->input->post('hr_man'),
			'hp_hr'  	=> $this->input->post('hp_hr'),
			'utugas'  	=> $this->input->post('utugas'),
		);
		//log_r($check_unix);
		//$this->load->model('M_personal');
		if ($check_id == $id_unix['unix']->id_unix) {
			$insert = $this->db->update('p_education', $data, ['id_unix' => $check_id]);
		} else {
			$insert = $this->db->insert('p_education',$data);
		}
		
		if ($insert == true ) {
			redirect('uraian_personal');
		}
	}


}
