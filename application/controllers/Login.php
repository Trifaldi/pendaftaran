<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
     * Fixed Menu
     *
     * @access  public
     * @param
     * @return  view
     */
  public function __construct()
  {
   parent::__construct();
   $this->load->model('Form_model');
   $this->load->library('session');
     //$this->load->library(array('form_validation','session'));
                 // $this->load->helper(array('url','html','form'));
                 //$this->id_uniqe = $this->session->userdata('id_uniqe');
 }
 public function index()
 {
  $data['title'] = 'Login';
  $data['message'] = $this->session->flashdata('failed');
  $this->load->view('login');
}

function get_quotes($stt = '')
{
  $data = array();
  $arr = array();
  $quotes = quotes(1);
  shuffle($quotes);
  foreach ($quotes as $key => $value) :
    $data[] = $value;
  endforeach;
  if ($stt == 'log') {
    $arr[] = base_url('apllicants.jpg');
    $arr[] = base_url('assets/img/symphony.png');
  }
  echo '{"plus":' . json_encode($data) . ', "file":' . json_encode($arr) . '}';
}

public function login_attempt(){

  $rules = [
    [
      'field' => 'hp_1',
      'label' => 'Password',
      'rules' => 'required'
    ]
  ];

  $this->form_validation->set_rules($rules);
  if ($this->form_validation->run()) {
    $this->load->model('user_m');
    $attempt = $this->user_m->attempt($this->input->post());
    $hp_1 =  $this->input->post('hp_1');
    if (strlen($hp_1) < 10) {
      header("Content-type:application/json");
      echo json_encode(['hp_1' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Nomor HP minimal 9 digit</div>']);
                //redirect('login');
    } elseif ($attempt === null) {
      header("Content-type:application/json");
      echo json_encode(['hp_1' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Nomor Yang Anda Masukan Tidak Terdaftar</div>']);
            // } elseif ($attempt->hp_1 < 9 ) {
            //     header("Content-type:application/json");
            //     echo json_encode(['hp_1' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Nomor Yang Anda Masukan Tidak Cukup</div>']);
    } else {
      $this->session->set_userdata('active_user', $attempt);
      $session = array(
                 'authenticated'=>true, // Buat session authenticated dengan value true
                 'hp_1'=>$attempt->hp_1,  // Buat session username
                  'fullname'=>$attempt->fullname, //
                 'uniqe'=>$attempt->uniqe // Buat session authenticated
               );
      $this->session->set_userdata($session);
      header("Content-type:application/json");
      echo json_encode(['status' => 'success']);
    }
  } else {
    header("Content-type:application/json");
    echo json_encode($this->form_validation->get_all_errors());
  }
}
public function logout(){
    $this->session->sess_destroy(); // Hapus semua session
    redirect('login'); // Redirect ke halaman login
  }


  public function register_form()
  {
    $this->data['title'] = 'Register';
    $this->data['subview'] = 'template/register';
    $this->load->view('components/layout', $this->data);
  }

  public function post_register()
  {

    $nomor_terpisah = $this->input->post('hp_1');
    $gabung = str_replace('-','',$nomor_terpisah);

    $this->form_validation->set_rules('fullname', 'fullname', 'required');
    $this->form_validation->set_rules('hp_1', 'hp_1', 'required|is_unique[b_personal.hp_1]');
    $this->form_validation->set_error_delimiters('<div class="label label-danger"> test', '</div>');
    $this->form_validation->set_message('required', 'Enter %s');

    if ($this->form_validation->run() === FALSE)
    {
      set_session('fullname',$this->input->post('fullname'));
      set_session('hp_1',$this->input->post('hp_1'));
      set_session('uniqe',$this->input->post('uniqe'));
      $this->session->set_flashdata('failed',"Nomor Ini Sudah Terdaftar");
      redirect('login');

    } else {
      $hp_1 =  $this->input->post('hp_1');
      $namafile= substr($hp_1,0,2);
      if (substr($hp_1,0,2) !== '08') {
        set_session('fullname',$this->input->post('fullname'));
        set_session('hp_1',$this->input->post('hp_1'));
        $this->session->set_flashdata('failed',"Nomor HP Harus di awali 08");
        redirect('login');
      }else if (strlen($hp_1) < 10) {
        $this->session->set_flashdata('failed',"Nomor HP minimal 9 digit");
        redirect('login');
      }
      $data = array(
       'fullname' => $this->input->post('fullname'),
       'hp_1' => $this->input->post('hp_1'),
       'password' => $gabung,
       'uniqe' => $this->input->post('uniqe')
     );
      $this->Form_model->insert_user($data);
      $this->session->set_userdata($data);
    }
    redirect( base_url('profil') ); 
  }
}
