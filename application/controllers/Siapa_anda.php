<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siapa_anda extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('M_personal');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{

		$uniqe = $this->session->userdata('uniqe');
		$this->load->helper('url');
		$data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
		$data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
		$data ['subview'] = 'steep/siapa_anda';
		$this->load->view('step',$data);

	}

	public function create()
	{	
		$check_id = $this->session->userdata('uniqe');
		$id_unix['unix'] = $this->db->query("SELECT * FROM p_uraian WHERE id_unix = '$check_id'")->row();
		$data = array(
			'id_unix'		=> $this->session->userdata('uniqe'),
			'23siapaanda' 		=> $this->input->post('23siapaanda'),
			'24kriminal' 		=> $this->input->post('24kriminal'),
		);
		if ($check_id == $id_unix['unix']->id_unix) {
			$insert = $this->db->update('p_uraian', $data, ['id_unix' => $check_id]);
			echo json_encode($data);
		} else {
			$insert = $this->db->insert('p_uraian',$data);
			echo json_encode($data);
		}
		
		// if ($insert == true ) {
		// 	redirect('gagal');
		// }else{
		// 	redirect('gagal');
		// }
	}

	public function save(){
  	$check_id = $this->session->userdata('uniqe');
		$id_unix['unix'] = $this->db->query("SELECT * FROM p_uraian WHERE id_unix = '$check_id'")->row();
		$data = array(
			'id_unix'		=> $this->session->userdata('uniqe'),
			'26penyataan' 		=> $this->input->post('26penyataan'),
			'dateentry' 		=> date('yy-m-d h:i:s'),
		);
		if ($check_id == $id_unix['unix']->id_unix) {
			$insert = $this->db->update('p_uraian', $data, ['id_unix' => $check_id]);
			echo json_encode($data);
		} else {
			$insert = $this->db->insert('p_uraian',$data);
			echo json_encode($data);
		}
		
		if ($insert == true ) {
			redirect('login/logout');
		}else{
			redirect('siapa_anda');
		}
  	}


}