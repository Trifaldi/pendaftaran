<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav animated bounceInLeft">
        <div class="step active complete" data-form="#form-1"></div>
  			<div class="step active complete" data-form="#form-2"></div>
  			<div class="step active complete" data-form="#form-3"></div>
  			<div class="step active complete" data-form="#form-4"></div>
  			<div class="step active complete" data-form="#form-5"></div>
  			<div class="step active complete" data-form="#form-6"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
  			<div class="step"></div>
			</div>
		</div>

		<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

			<div class="row">
				<div class="col-sm-6 jarak_kanan animated bounceInRight">
          <form class="responsive-height" action="javascript:void(0)" method="POST" id="form-user4">
          <div class="col-md-12">
  <div class="form-group">
    <label >Nama Emergency</label>
    <input type="hidden" id="id_unix" name="id_unix" value="<?php echo $this->session->userdata('uniqe');?>">
    <input type="hidden" name="fam_kategori" value="5">
    <input type="hidden" id="fam_ket" name="fam_ket" value="Emergency">
    <input class="form-control" type="text" id="fam_nama" name="fam_nama" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return harusHuruf('event')">
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Jenis Kelamin</label>
    <select name="fam_jekel" class="form-control" id="fam_jekel">
      <option>Pilih..</option>
      <option value="Pria">Pria</option>
      <option value="Wanita">Wanita</option>
    </select>
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >No Handphone</label>
    <input type="text" name="fam_no_hp" id="fam_no_hp" class="form-control" maxlength="16" onkeypress="return angka(event)">
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Usia</label>
    <input type="text" name="fam_usia" id="fam_usia" class="form-control" maxlength="3">
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Pendidikan</label>
    <select class="form-control" name="fam_pen" id="fam_pen">
      <option>Pilih..</option>
      <?php foreach ($pendidikan as $key => $value) { ?>
       <option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
      <?php } ?>
    </select>
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Job</label>
    <select class="form-control" onchange="myFunctionSaudara(event)" id="fam_job" name="fam_job">
      <option>Pilih..</option>
      <option value="Bekerja">Bekerja</option>
      <option value="Tidak Bekerja">Tidak Bekerja</option>
    </select>
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<p id="suadara"></p>

<button type="button" type="submit" id="btn_save" class="btn btn-primary pull-right">Save</button>

</form>
				</div>
				<div class="col-sm-6 jarak_kiri animated bounceInLeft">
          <style type="text/css">
  tr{
    background: #4dc3ff;
  }
</style>
<table class="table" id="mydata">
  <thead>
    <tr>
      <th>Nama Saudara</th>
      <th>No Handohone</th>
      <th>Jenis Kelamin</th>
    </tr>
  </thead>
  <tbody id="show_data">

  </tbody>
</table>

				</div>
			</div>


		</div>
			<div class="content-box-footer">
        <?php if ($personal->marital==1 && $personal->jumlah_saudara==0) { ?>
        <a href="<?php echo base_url(); ?>orangtua" class="btn btn-warning">Back</a>
      <?php  }elseif ($personal->marital==1 && $personal->jumlah_saudara==1) {?>
       <a href="<?php echo base_url(); ?>saudara" class="btn btn-warning">Back</a>
      <?php }else{ ?>
        <a href="<?php echo base_url(); ?>keluarga" class="btn btn-warning">Back</a>
      <?php } ?>
				<a href="<?php echo base_url(); ?>pendidikan" class="btn btn-success pull-right" id="btn_next">Next</a>
			</div>
	</div>
<script>
$(document).ready(function(){
  <?php if ($emergency->fam_kategori == '5') {?>
    $('#btn_next').show();
  <?php } else { ?>
    $('#btn_next').hide();
  <?php } ?>
    
    $("#btn_save").click(function(){
    $('#btn_next').show();
  });
});
</script>
<script type="text/javascript">

function myFunctionSaudara(e) {
  var x = document.getElementById("fam_job").value;
  if (x == 'Bekerja') {
    document.getElementById("suadara").innerHTML='<div class="col-md-12">'+
    '<div class="form-group">'+
    '<label >Nama Perusahaan</label>'+
    '<input class="form-control" type="text" id="fam_per" name="fam_per" onkeyup="this.value = this.value.toUpperCase()">'+
    '<div class="help-block form-text text-muted form-control-feedback"></div>'+
    '</div>'+
    '</div>';
} else {
    document.getElementById("suadara").innerHTML='';
}

}

</script>
<script type="text/javascript">
      $('#fam_no_hp').click(function(event) {
        var val = $('#fam_no_hp').val();
        if(val==''){
          $('#fam_no_hp').val('08');
        }
      });
      $('#fam_no_hp').focus(function(event) {
        $('#fam_no_hp').val();
      });

      $("#fam_no_hp").focusout(function(){
        $('#fam_no_hp').val();
      });

      $("#fam_no_hp").keyup(function(){
        var val = $('#fam_no_hp').val();
  //var valx = '085834246342';
  var valx = val.replace(/-/gi, "");
  console.log(valx);
  arr = valx.match(/.{1,4}/g);

  //bersihkan array jika ada yg undefined
  var i = 0;
  var string = "";
  if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
    string = arr[0];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
    string = arr[0]+"-"+arr[1];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
    string = arr[0]+"-"+arr[1]+"-"+arr[2];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
    string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  }


  //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  console.log(string);
  $('#fam_no_hp').val('');
  $('#fam_no_hp').val(string);
  if(val=='08'){
    $('#fam_no_hp').val('08');
  }else if(val=='0'){
    $('#fam_no_hp').val('08');
  }else if(val==''){
    $('#fam_no_hp').val('08');
  }
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    show_all();

    $('#mydata').dataTable();

    function show_all(){
      $.ajax({
        type : 'ajax',
        url  : '<?php echo base_url();?>emergency/get',
        async : false,
        dataType : 'json',
        success  : function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<tr>'+
                '<td>'+data[i].fam_nama+'</td>'+
                            '<td>'+data[i].fam_no_hp+'</td>'+
                            '<td style="text-align:center;">'+data[i].fam_jekel+'</td>'+
                            '</tr>';
          }
          $('#show_data').html(html);
        }
      });
    }

    $('#btn_save').click(function(){
      var data = $('#form-user4').serialize();

      $.ajax({
        url: '<?php echo base_url('emergency/save');?>',
        type: 'POST',
        async: false,
        dataType: 'JSON',
        data: data,
        success: function(data){
          $('[name="fam_nama"]').val("");
          $('[name="fam_jekel"]').val("");
          $('[name="fam_no_hp"]').val("");
          $('[name="fam_usia"]').val("");
          $('[name="fam_pen"]').val("");
          $('[name="fam_job"]').val("");
          $('[name="fam_per"]').val("");
          show_all();
        }
      });
      return false;
    });


    $('#show_data').on('click','.item_delete',function(){
            var id_fam = $(this).data('id_fam');

            $('#Modal_Delete').modal('show');
            $('[name="_delete"]').val(id_fam);
        });

     $('#btn_delete').on('click',function(){
        var id_fam = $('#_delete').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('saudara/delete')?>",
            dataType : "JSON",
            data : {id_fam:id_fam},
            success: function(data){
                $('[name="_delete"]').val("");
                $('#Modal_Delete').modal('hide');
                show_product();
            }
        });
        return false;
    });
  });
</script>
