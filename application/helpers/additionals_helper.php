<?php
function max_step()
{
    return 24;
}

function get_tabel()
{
    return array( '', //0
            'b_personal', //step 1
            'b_personal', //step 2
            'p_familiy',
        );
}

//check_in
function check_in($name = '', $unik = '', $user = '')
{
    $nama = '';
    if ($name == '' || $user == '') {
        $stt  = 2; //tidak sesuai ketentuan
    } else {
        $nama = get_name_workers($user);
        $CI = &get_instance();
        if ($unik != '') {
            $CI->db->where('unik', $unik);
        }
        $cek = $CI->db->get_where('check_in', array('name' => $name));
        if ($cek->num_rows() == 0) {  //belum ada langsung di insert
            $CI->db->insert('check_in', array('name' => $name, 'unik' => $unik, 'user' => $user, 'tgl_input' => date('Y-m-d H:i:s')));
            $stt = 0;
        } else {
            if ($cek->row()->user == $user) {
                $stt = 'y'; //hanya user ini yang bisa lihat
            } else {
                $nama = get_name_workers($cek->row()->user);
                $stt = 1; //sudah ada yang pakai
            }
        }
    }
    return array('stt' => $stt, 'nama' => $nama);
}

//delete check in per usernya
function del_check_in($name = '', $unik = '')
{
    $CI = &get_instance();
    $CI->db->delete('check_in', array('user' => '0'));
    if ($unik != '') {
        $CI->db->delete('check_in', array('name' => $name, 'unik' => $unik));
    } else {
        $CI->db->delete('check_in', array('user' => $name));
    }
    return true;
}

//list tag versi di git
function list_version()
{
    $list_versi = exec("git tag --column");
    $data_list = explode(" ", $list_versi);
    $data = array();
    foreach ($data_list as $key => $versi) {
        $data[] = $versi;
    }
    return $data;
}

//cek di combo box apakah selected atau tidak
function selected_cb($stringx, $stringy)
{
    if ($stringy == $stringx) {
        return 'selected';
    }
}

//cek text apakah ada di dalam array
function didalam_array($stringx, $arrayx, $returnx = 'selected')
{

    if (in_array($stringx, $arrayx)) {
        return $returnx;
    }
}


//list detail/message tag sesuai versi di git
function deskripsi_version($versi = '', $length = '')
{
    if ($versi == '') {
        $versi = check_version_app(1);
    }
    if ($length == '') {
        $length = 15;
    }
    $datanya = '';
    $deskripsi = array();
    for ($i = 1; $i <= $length; $i++) {
        $dt = exec("git tag -l -n$i $versi");
        $get_list = explode("- ", $dt);
        if ($datanya != $get_list[1]) {
            $datanya = $get_list[1];
            $deskripsi[] = "- $datanya";
        }
    }
    return $deskripsi;
}

//cek versi di git
function check_version_app($n = null)
{
    $cek = exec("git describe --always");
    $version = explode("-", $cek);
    if ($n == 1) {
        return $version[0];
    } else if ($n == 2) {
        return $version[0] . '-' . $version[1];
    } else {
        return "<b>Version</b> " . substr($version[0], 1);
    }
}

function footernya($stt = '')
{
    $style = '';
    $html = '';
    if ($stt == 'x') {
        $ket = '<style> #a_release{ color:chocolate; } #a_release:hover{ color:#222; } </style><footer class="main-footer">
      <div class="pull-right">
        <a id="a_release" onclick="get_release();" style="cursor:pointer">' . check_version_app() . '</a>
      </div>
      Page rendered in {elapsed_time} seconds.
    </footer>';
    } else {
        if ($stt == 'log_desktop') {
            $style = 'style="bottom:0px;text-align: center;width: 100%;"';
        }
        if ($stt == 'log_mobile') {
            $html = '<br />';
        } else {
            $html = '. ';
        }
        $ket  = '<style> a{ color:chocolate; } a:hover{ color:#222; } </style>';
        $ket .= '<p class="text-center" ' . $style . '>';
        $ket .= ' Copyright &copy;2017-' . date('Y') . $html;
        $ket .= ' <b><a onclick="get_release();" style="cursor:pointer">' . check_version_app() . '</a></b>
             </p>';
    }
    $ket .= modal_versi();
    return $ket;
}

function modal_versi($stt = '')
{
    $CI = &get_instance();
    $CI->load->view('auth/modal_versi');
}

function up_cheat($tbl = '', $id_cabang = '', $id_outlet = '')
{
    $CI = &get_instance();
    $set['dateTIME'] = date('Y-m-d H:i:s');
    if ($tbl != '') {
        $CI->db->where('table', $tbl);
    }
    if ($id_cabang != '') {
        $CI->db->where_in('id_cabang', array($id_cabang, 0));
    }
    if ($id_outlet != '') {
        $CI->db->where_in('id_outlet', array($id_outlet, 0));
    }
    $CI->db->update('e_pos_sync_master', $set);
}

function hari_konsinyasi($hari = '', $query = '')
{
    $CI = &get_instance();
    $gudang_list = array();
    // if (get_hari($hari) == 'Senin') {
    //     $gudang_list = array('32', '38'); //gudang yg di auto_request
    // } elseif (get_hari($hari) == 'Selasa') {
    //     $gudang_list = array('29', '31'); //gudang yg di auto_request
    // } elseif (get_hari($hari) == 'Rabu') {
    //     $gudang_list = array('34', '36'); //gudang yg di auto_request
    // } elseif (get_hari($hari) == 'Kamis') {
    //     $gudang_list = array('32', '74'); //gudang yg di auto_request
    // } elseif (get_hari($hari) == 'Jumat') {
    //     $gudang_list = array('96', '37'); //gudang yg di auto_request
    // }
    if ($query == 'query_builder') {
        if (!empty($gudang_list)) {
            return $CI->db->where_in("A.id_gudang", $gudang_list);
        }
    } else {
        return $gudang_list;
    }
}

function check_syncron($halaman = '', $status = '', $id_outlet = '', $ket = '')
{
    $CI = &get_instance();
    $tbl = 'd_status_syncron';
    $jml = 0;
    if ($ket == 'vs') {
        $where = array('halaman' => $halaman, 'status_vs' => $status);
        $jml = $CI->db->get_where($tbl, $where)->num_rows();
    } else {
        $id_cabang = get_id_cabang_from_outlet($id_outlet);
        $where = array('halaman' => $halaman, 'id_cabang' => $id_cabang, 'id_outlet' => $id_outlet, 'status' => $status);
        $jml = $CI->db->get_where($tbl, $where)->num_rows();
    }
    if ($jml >= 1) {
        $jml = 1;
    }
    return $jml;
}

function up_syncron($halaman = '', $status = '', $id_outlet = '', $ket = '')
{
    $CI = &get_instance();
    $tbl = 'd_status_syncron';
    if ($id_outlet == '') {
        $where = array('halaman' => $halaman);
        if ($CI->db->get_where($tbl, $where)->num_rows() != 0) {
            if ($ket == 'vs') {
                $CI->db->update($tbl, array('status_vs' => $status), $where);
            } else {
                $CI->db->update($tbl, array('status' => $status), $where);
            }
        }
    } else {
        $id_cabang = get_id_cabang_from_outlet($id_outlet);
        $where = array('halaman' => $halaman, 'id_cabang' => $id_cabang, 'id_outlet' => $id_outlet);
        if ($CI->db->get_where($tbl, $where)->num_rows() != 0) {
            if ($ket == 'vs') {
                $CI->db->update($tbl, array('status_vs' => $status), $where);
            } else {
                $CI->db->update($tbl, array('status' => $status), $where);
            }
        }
    }
    return true;
}

function log_r($string = null, $var_dump = false)
{
    if ($var_dump) {
        var_dump($string);
    } else {
        echo "<pre>";
        print_r($string);
    }
    exit;
}

function log_data($string = null, $var_dump = false)
{
    if ($var_dump) {
        var_dump($string);
    } else {
        echo "<pre>";
        print_r($string);
    }
    // exit;
}

function get_bulan($bulan = '')
{
    $tanggal = date("Y-$bulan-d");
    $arr = array(
        '01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni',
        '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember',
    );
    $split = explode('-', $tanggal);
    return $arr[$split[1]];
}

function get_tgl_indo($tanggal = '')
{
    if ($tanggal == '') {
        $tanggal = date('Y-m-d');
    }
    $split = explode('-', $tanggal);
    return $split[2] . ' ' . get_bulan($split[1]) . ' ' . $split[0];
}

function get_hari($hari = '')
{
    if ($hari == '') {
        $hari = date("D");
    }
    switch ($hari) {
        case 'Sun':
            $hari = "Minggu";
            break;
        case 'Mon':
            $hari = "Senin";
            break;
        case 'Tue':
            $hari = "Selasa";
            break;
        case 'Wed':
            $hari = "Rabu";
            break;
        case 'Thu':
            $hari = "Kamis";
            break;
        case 'Fri':
            $hari = "Jumat";
            break;
        case 'Sat':
            $hari = "Sabtu";
            break;
        default:
            $hari = "Tidak di ketahui";
            break;
    }
    return $hari;
}

function save_data($table, $data = array(), $id = null, $primary_key = null)
{
    $CI = &get_instance();
    if ($id === null) {                    // Insert
        $CI->db->set($data);
        $CI->db->insert($table);
        $id = $CI->db->insert_id();
    } else {                                // Update
        $CI->db->set($data);
        $CI->db->where($primary_key, $id);
        $CI->db->update($table);
    }
    return $id;
}

function dF($date, $format)
{
    return date($format, strtotime($date));
}

function encode_id($string)
{
    $CI = &get_instance();
    $id = $CI->encrypt->encode($string);
    $id = str_replace("/", "==11==", $id);
    $id = str_replace("+", "==22==", $id);
    return $id;
}

function decode_id($string)
{
    $CI = &get_instance();
    $id = str_replace("==11==", "/", $string);
    $id = str_replace("==22==", "+", $id);
    $id = $CI->encrypt->decode($id);
    return $id;
}

function hash_password($string)
{
    return hash('sha512', $string . config_item('encryption_key'));
}

function check_status_po_completed()
{
    $CI = get_instance();
    $where = array('status' => 'COMPLETED');
    $data = $CI->db->get_where('d_po_master', $where)->num_rows();
    $dat = '';
    if ($data > 0) $dat = '<span class="pull-right-container"><small class="label pull-right bg-red">' . $data . '</small></span>';
    return $dat;
}

function get_list_cabang($id = null, $kosong = null)
{
    $CI = get_instance();
    $where = array();
    if ($id != null) $where = array('id' => $id);
    $CI->db->where('id !=', 1);
    $data = $CI->db->get_where('a_cabang', $where)->result();
    $option = array('' => 'Pilih Kota');
    foreach ($data as $key => $value) {
        $option[$value->id] = $value->name;
    }
    return $option;
}

function get_list_brand($id = null, $kosong = null)
{
    $CI = get_instance();
    $where = array();
    if ($id != null) $where = array('id' => $id);
    $data = $CI->db->get_where('a_brand', $where)->result();
    $option = array('' => 'Pilih Brand');
    foreach ($data as $key => $value) {
        $option[$value->id] = $value->brand_name;
    }
    return $option;
}

function get_list_brand_by_kota($id = null, $kosong = null)
{
    $CI = get_instance();
    $where = array();
    if ($id != null) $where = array('id_cabang' => $id);
    $data = $CI->db->get_where('a_brand', $where)->result();
    $option = array('' => 'Pilih Brand');
    foreach ($data as $key => $value) {
        $option[$value->id_brand] = $value->brand_name;
    }
    return $option;
}

function get_kota_by_brand($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('id_cabang')->get_where('a_brand', $where)->row();
    if (empty($data)) return '-';
    else return $data->id_cabang;
}

function get_region_by_id($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('name')->get_where('a_region', $where)->row();
    if (empty($data)) return '-';
    else return $data->name;
}

function get_name_vendor_by_id($id = null)
{
    $CI = get_instance();
    $where = array('id_vendor' => $id);
    $data = $CI->db->select('nm_vendor')->get_where('d_vendor', $where)->row();
    if (empty($data)) return '-';
    else return $data->nm_vendor;
}

function get_name_perusahaan_by_id($id = null)
{
    $CI = get_instance();
    $CI->db->select('a.nama_perusahaan, b.nm_badan_usaha');
    $CI->db->join('d_badan_usaha b', 'a.id_badan_usaha = b.id_badan_usaha');
    $CI->db->where('a.id', $id);
    $data = $CI->db->get('a_perusahaan a')->row();
    if (empty($data)) return '-';
    else return $data->nama_perusahaan . ', ' . $data->nm_badan_usaha;
}

function get_logo_perusahaan_by_id($id = null)
{
    $CI = get_instance();
    $CI->db->select('a.path, a.logo_perusahaan');
    $CI->db->where('a.id', $id);
    $data = $CI->db->get('a_perusahaan a')->row();
    if (empty($data)) return '-';
    else return $data->path . '/' . $data->logo_perusahaan;
}

function get_name_item_from_plu($plu = null, $row = false)
{
    $CI = get_instance();
    $where = array('plu' => $plu);
    $datarow = $CI->db->get_where('d_item', $where)->row();
    if ($row) {
        return $datarow;
    } else {
        $nama = '-';
        if (!empty($datarow)) $nama = $datarow->nama_item;
        return $nama;
    }
}

function get_name_users($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('name')->get_where('a_users', $where)->row();
    if (empty($data)) return '-';
    else return $data->name;
}

function get_no_invoice($id = null)
{
    $CI = get_instance();
    $where = array('no_urut_po' => $id);
    $data = $CI->db->select('no_invoice')->get_where('d_pembelian', $where)->row();
    // log_r($data->no_invoice);
    if (empty($data)) return '-';
    else return $data->no_invoice;
}

function get_qty_pembelian($plu = null, $no_urut = null, $nama_produk = null)
{
    $CI = get_instance();
    $where = array('plu' => $plu, 'no_urut_po' => $no_urut, 'nama_produk' => $nama_produk);
    $CI->db->group_by('nama_produk');
    $data = $CI->db->select('SUM(jumlah) as jumlah')->get_where('d_pembelian_list', $where)->row();
    if (empty($data)) return '0';
    else return $data->jumlah;
}

function get_jumlah_qty_po($plu = null, $no_urut_po = null, $merk = null)
{
    $CI = get_instance();
    $where = array('plu' => $plu, 'no_urut' => $no_urut_po, 'nama_produk' => $merk);
    $CI->db->select('jumlah');
    $CI->db->from('d_po_list');
    $CI->db->where($where);
    $data = $CI->db->get()->row();
    if (empty($data)) return '0';
    else return $data->jumlah;
}


function getKota($id)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $CI->db->select('name');
    $CI->db->from('a_cabang');
    $CI->db->where($where);
    $data = $CI->db->get()->row();
    if (empty($data)) return '-';
    else return $data->name;
}
function cek_satuan_pembelian($satuan = null, $id_satuan = null)
{
    $CI = get_instance();
    $where = array('unit_name' => trim($satuan, " "), 'id_unit' => $id_satuan);
    $CI->db->select('unit_name');
    $CI->db->from('d_item_unit');
    $CI->db->where($where);
    $data = $CI->db->get()->row();
    if (empty($data)) return false;
    else return true;
}

function get_name_workers($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('nama')->get_where('b_worker', $where)->row();
    if (empty($data)) return '-';
    else return $data->nama;
}

function get_worker_id($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('id_worker')->get_where('a_users', $where)->row();
    if (empty($data)) return '-';
    else return get_posisi_by_worker_id($data->id_worker);
}

function get_posisi_by_worker_id($worker_id = null)
{
    $CI = get_instance();
    $where = array('id' => $worker_id);
    $data = $CI->db->query(" SELECT P.posisi AS posisi FROM `b_worker` AS B JOIN b_posisi AS P ON B.posisi_id = P.id WHERE B.id = $worker_id ")->row();
    if (empty($data)) return '-';
    else return $data->posisi;
}

function get_name_users_pos($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('nama_lengkap')->get_where('e_pos_login', $where)->row();
    if (empty($data)) return '-';
    else return $data->nama_lengkap;
}

function get_name_gudang($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('nama_gudang')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '-';
    else return $data->nama_gudang;
}

function get_outlet_partnerstore($no, $pelanggan)
{
    $CI = get_instance();
    if ($pelanggan == '34') {
        $where = array('no_penjualan' => $no);
        $CI->db->join('d_gudang B', 'A.gudang_tujuan = B.id');
        $data = $CI->db->select('SUBSTR(B.nama_gudang, 21) as nama_gudang_tujuan')->get_where('d_penjualan_pelanggan_master A', $where)->row();
    } else if ($pelanggan == '0' || $pelanggan == NULL) {
        $data = '';
    } else {
        $where = array('no_penjualan' => $no);
        $CI->db->join('d_pelanggan_store B', 'A.gudang_tujuan = B.id');
        $data = $CI->db->select('nama_store as nama_gudang_tujuan')->get_where('d_penjualan_pelanggan_master A', $where)->row();
    }
    if (empty($data)) return '-';
    else return $data->nama_gudang_tujuan;
}

function get_group_name_gudang($id = null)
{
    $CI = get_instance();
    $where = array('id_outlet' => $id);
    $data = $CI->db->select('group_name')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '-';
    else return $data->group_name;
}

function get_username($id = null)
{
    $CI = get_instance();
    $where = array('id_gudang' => $id, 'active' => 1);
    $data = $CI->db->select('username')->get_where('a_users', $where)->row();
    if (empty($data)) return '-';
    else return $data->username;
}

function get_username_DC($id = null)
{
    $CI = get_instance();
    $where = array('id_gudang' => $id, 'active' => 1);
    $CI->db->where('name LIKE "DC%"');
    $data = $CI->db->select('username')->get_where('a_users', $where)->row();
    if (empty($data)) return '-';
    else return $data->username;
}

function get_name_outlet($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('name')->get_where('a_outlet', $where)->row();
    if (empty($data)) return '-';
    else return $data->name;
}

function get_name_vendor($id = null)
{
    $CI = get_instance();
    $where = array('id_vendor' => $id);
    $data = $CI->db->select('nm_vendor')->get_where('d_vendor', $where)->row();
    if (empty($data)) return '-';
    else return $data->nm_vendor;
}

function get_name_pelanggan($id = null)
{
    $CI = get_instance();
    if (empty($id)) {
        return '-';
    } else {
        $where = array('id' => $id);
        $data = $CI->db->select('nama_badan_usaha,nama_lengkap')->get_where('d_pelanggan', $where)->row();
        if ($data->nama_badan_usaha == "NONE") {
            return $data->nama_lengkap;
        } else return $data->nama_badan_usaha;
    }
}

function get_name_cabang($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('name')->get_where('a_cabang', $where)->row();
    if (empty($data)) return '-';
    else return $data->name;
}

function get_name_depart($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('name')->get_where('c_department', $where)->row();
    if (empty($data)) return '-';
    else return $data->name;
}

function get_name_level($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('jabatan')->get_where('b_jabatan', $where)->row();
    if (empty($data)) return '-';
    else return $data->jabatan;
}

function get_existing($id = null, $cabang_id = null)
{
    $CI = get_instance();

    $data = $CI->db->query("SELECT
                                COUNT(department_id) AS total
                            FROM
                                b_worker
                            WHERE
                                department_id = $id
                                AND cabang_id = $cabang_id
                                AND status = 1
                            ")->result_array();

    return $data[0]['total'];
}


function get_brand($select, $where = '')
{
    $CI = get_instance();
    if ($where != '') {
        $data = $CI->db->select($select)->order_by('id_brand', 'ASC')->get_where('a_brand', $where);
    } else {
        $data = $CI->db->select($select)->order_by('id_brand', 'ASC')->get('a_brand');
    }
    return $data;
}

function get_name_brand($id = null)
{
    $CI = get_instance();
    $where = array('id_brand' => $id);
    $data = $CI->db->select('brand_name')->get_where('a_brand', $where)->row();
    if (empty($data)) return '-';
    else return $data->brand_name;
}

function get_id_brand($id = ID_WORKER)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('brand_id')->get_where('b_worker', $where)->row();
    if (empty($data)) return '-';
    else return $data->brand_id;
}

function get_name_idbrand($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('brand_name')->get_where('a_brand', $where)->row();
    if (empty($data)) return '-';
    else return $data->brand_name;
}

function get_payrol_id($user_id)
{
    $CI = get_instance();
    $where = array('id' => $user_id);
    $data = $CI->db->query("SELECT MAX(B.id), B.payrol_id FROM a_users A JOIN b_worker B ON B.id = A.id_worker WHERE A.id = '$user_id' ")->row();
    if (empty($data)) return '-';
    else return $data->payrol_id;
}

function get_id_outlet_from_gudang($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('id_outlet')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '';
    else return $data->id_outlet;
}

function get_id_gudang_from_outlet($id = null)
{
    $CI = get_instance();
    $where = array('id_outlet' => $id);
    $data = $CI->db->select('id')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '';
    else return $data->id;
}

function get_id_cabang_from_outlet($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('location')->get_where('a_outlet', $where)->row();
    if (empty($data)) return '';
    else return $data->location;
}

function get_id_cabang_from_gudang($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('id_cabang')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '';
    else return $data->id_cabang;
}

function get_id_brand_from_users($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $CI->db->select('id_brand');
    $data = $CI->db->get_where('a_users', $where)->row();
    if (empty($data)) return '';
    else return $data->id_brand;
}

function get_id_brand_from_outlet($id = null)
{
    $CI = get_instance();
    $where = array('c.id' => $id);
    $CI->db->select('b.id_brand');
    $CI->db->join('a_brand as b', 'a.id_brand=b.id_brand');
    $CI->db->join('d_gudang as c', 'a.id=c.id_outlet');
    $data = $CI->db->get_where('a_outlet as a', $where)->row();
    if (empty($data)) return '';
    else return $data->id_brand;
}

//
// function get_stock_item($date=NULL,$gudang=NULL,$plu=NULL){
//  $CI = get_instance();
//      //UPDATE Stock
//  $where = array('b.plu'=>$plu,'a.gudang'=>$gudang);
//  $CI->db->select("a.no_so as no_so, b.sisa_stock as total");
//  $CI->db->join("d_item_stockopname_detail b","a.no_so = b.no_so");
//  $CI->db->where("a.tanggal < '$date'");
//  $CI->db->order_by("a.tanggal","desc");
//  $CI->db->limit(1);
//  $get_last_so = $CI->db->get_where("d_item_stockopname_master a",$where)->row();
//  // log_r($CI->db->last_query());
//  $stock_update = 0;
//  $where      = array('gudang'=>$gudang, 'plu'=>$plu);
//  if(!empty($get_last_so)){
//      // $no_so           = preg_replace("/[^0-9]/", '', $lastSO->keterangan);
//      $lastSO         = $CI->db->query("SELECT tanggal FROM   d_item_stock_transfer WHERE gudang = '$gudang' AND keterangan LIKE CONCAT ('%',$get_last_so->no_so,'%') AND plu = '$plu' ORDER BY tanggal DESC LIMIT 1")->row();
//      $CI->db->where("tanggal >= '$lastSO->tanggal'");
//      $CI->db->where("tanggal <= '$date'");
//      $saldo_awal   = $CI->db->select('SUM(in_qty) - SUM(out_qty) as total')->get_where('d_item_stock_transfer',$where)->row();
//      $stock_update = $saldo_awal->total + $get_last_so->total;
//  } else {
//      $CI->db->where("tanggal <= '$date'");
//      $saldo_awal   = $CI->db->select('SUM(in_qty) - SUM(out_qty) as total')->get_where('d_item_stock_transfer',$where)->row();
//      $stock_update = $saldo_awal->total;
//  }
//
//  return $stock_update;
// }

function native_url($link = null)
{
    $CI = &get_instance();
    $path = "http://192.168.9.2/erp/villadmin";
    $salt = date('dMyH');
    $url = $path . '/' . $link . '/' . $CI->session->userdata('user_id') . '/' . md5($salt . $CI->session->userdata('user_id'));
    return $url;
}

function folENC($id, $type = 0)
{
    return base64_encode($id);
}

function view_files($link = null, $no_img = null, $type = null)
{
    $dir = $link;

    $map = directory_map($dir, false, true);
    if (!empty($map[0])) {

        if ($type != null) {
            foreach ($map as $key => $value) {
                $img[$key]['source'] = base_url() . $dir . '/' . $value;
                $img[$key]['name'] = $value;
            }
        } else {
            $img['source'] = base_url() . $dir . '/' . $map[0];
            $img['name'] = $map[0];
        }
    } else {
        if ($no_img != null) {
            $img['source'] = 'assets/img/no_img/' . $no_img;
            $img['name'] = "";
        } else {
            $img['source'] = "";
            $img['name'] = "";
        }
    }
    return $img;
}

function upload_files($field = null, $type = null)
{
    $CI = &get_instance();
    if ($type == 'multi_upload') { } else {
        $idSAVE = $field['idSAVE'];
        $path = $field['path'] . '/' . folENC($idSAVE);
        $field_upload = $field['name_field'];
        if (!file_exists($path)) mkdir($path, 0777, true);
        $map = directory_map($path, false, true);
        if (!empty($_FILES[$field_upload]['tmp_name'])) {
            foreach ($map as $value) {
                unlink($path . '/' . $value);
            }
        }
        $config['upload_path'] = $path;
        $config['allowed_types'] = $field['allowed_types'];
        $config['max_size'] = $field['max_size'];
        $config['overwrite'] = true;
        $config['file_name'] = date('Ymdhis');

        $CI->upload->initialize($config);
        $uploaded = $CI->upload->do_upload($field_upload);
        if (!$uploaded) {
            $datamsg = array("msg" => $CI->upload->display_errors());
            return $datamsg;
        }
    }
}

function check_details_users()
{
    $CI = get_instance();
    $where = array('id' => $CI->session->userdata('user_id'));
    $data = $CI->db->get_where('a_users', $where)->row();
    return $data;
}

function check_status_tiket()
{
    $CI = get_instance();
    $dt = check_details_users();
    $data = 0;
    if ($CI->ion_auth->in_group(array('manager'))) {
        $idtype = array(1, 4);
        $CI->db->from('e_pos_helpdesk');
        $CI->db->where_in('idTHELPDESK', $idtype);
        $CI->db->where('approvebyHELPDESK', '');
        $CI->db->where('id_cabang', $dt->cabang_id);
        $data = $CI->db->get()->num_rows();
    }

    if ($CI->ion_auth->in_group(array('admin'))) {
        $where = array('donebyHELPDESK' => '');
        $data = $CI->db->get_where('e_pos_helpdesk', $where)->num_rows();
    }
    $dat = '';
    if ($data > 0) $dat = '<span class="pull-right-container"><small class="label pull-right bg-red">' . $data . '</small></span>';

    return $dat;
}

function groupurl()
{
    $url = new stdClass;
    $url->url_server = 'http://192.168.9.2/villasistem/';
    return $url;
}

// function get_mac_address()
// {
//     $client = @$_SERVER['HTTP_CLIENT_IP'];
//     $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
//     $remote = $_SERVER['REMOTE_ADDR'];
//     $SERVER_ADDR = $_SERVER['SERVER_ADDR'];
//     if (filter_var($client, FILTER_VALIDATE_IP)) {
//         $ip = $client;
//     } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
//         $ip = $forward;
//     } else {
//         $ip = $remote;
//     }
//     if ($ip == $SERVER_ADDR) {
//         if (PHP_OS == 'Linux') {
//             ob_start();
//             system('ifconfig -a');
//             $mycom = ob_get_contents();
//             ob_clean();
//             $findme = "Physical";
//             $pmac = strpos($mycom, $findme);
//             $mac = substr($mycom, ($pmac + 37), 18);
//             $mac_address = strtoupper(str_replace(':', '-', $mac));
//
//         } else {
//             ob_start();
//             system('getmac');
//             $Content = ob_get_contents();
//             ob_clean();
//             $mac_address = substr($Content, strpos($Content, '\\') - 20, 17);
//         }
//     } else {
//         if (PHP_OS == 'Linux') {
//             ob_start();
//             system(" arp -a " . $ip . " | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'");
//             $Content = ob_get_contents();
//             ob_clean();
//             $mac_address = strtoupper(str_replace(':', '-', $Content));
//         } else {
//             ob_start();
//             system('arp -a ' . $ip);
//             $Content = ob_get_contents();
//             ob_clean();
//             $mac_address = substr($Content, strpos($Content, '\\') - 40, 23);
//         }
//
//     }
//     $mac_address = str_replace(" ", "", $mac_address);
//     $mac_address = str_replace("\n", "", $mac_address);
//     return $mac_address;
// }

function update_sync_master($table = null, $id_cabang = 0, $id_outlet = 0)
{
    $CI = get_instance();
    if ($table != null) {
        $data['dateTIME'] = date('Y-m-d H:i:s');
        $CI->db->where('table', $table);

        if ($id_cabang != 0) $CI->db->where('id_cabang', $id_cabang);
        else $CI->db->where('id_outlet', $id_outlet);

        $CI->db->update('e_pos_sync_master', $data);
    }
}

function get_kota_from_plu($plu = null)
{
    $CI = get_instance();
    $name = '-';
    if ($plu != null) {
        $CI->db->select('a.brand_name');
        $CI->db->join('d_item_cabang AS b', 'a.id_brand=b.id_brand');
        $CI->db->where('plu', $plu);
        $result = $CI->db->get('a_brand AS a')->result();
        $name = array();
        foreach ($result as $key => $value) {
            $name[] = $value->brand_name;
        }
        $name = implode('<br>', $name);
    }
    return $name;
}

function get_brand_name_from_plu($plu = null, $kota = null)
{
    $CI = get_instance();
    $name = '-';
    if ($plu != null) {
        $CI->db->select('a.brand_name');
        $CI->db->join('d_item_cabang AS b', 'a.id_brand=b.id_brand');
        $CI->db->where('plu', $plu);
        $CI->db->where('id_cabang', $kota);
        $result = $CI->db->get('a_brand AS a')->result();
        $name = array();
        foreach ($result as $key => $value) {
            $name[] = $value->brand_name;
        }
        $name = implode('<br>', $name);
    }
    return $name;
}

function get_data_konversi_for_itemkota_detail($idvendor, $merk, $id_brand, $namaitem)
{
    $output = array();
    $CI = get_instance();
    // $query = $CI->db->query("SELECT tablesatuanawal.unit_name AS namasatuanawal, tablesatuankonversi.unit_name AS namasatuankonversi, d_item_vendor.nilai_konversi FROM d_item_unit AS tablesatuanawal INNER JOIN d_item_vendor ON tablesatuanawal.id_unit=d_item_vendor.satuan_awal INNER JOIN d_item_unit AS tablesatuankonversi ON tablesatuankonversi.id_unit=d_item_vendor.satuan_konversi WHERE d_item_vendor.id_vendor='$idvendor' AND d_item_vendor.merk='$merk' AND d_item_vendor.id_cabang='$idcabang' AND d_item_vendor.nama_item='$namaitem'")->result();

    $CI->db->select('tablesatuanawal.unit_name AS namasatuanawal, tablesatuankonversi.unit_name AS namasatuankonversi, d_item_vendor.nilai_konversi');
    $CI->db->from('d_item_unit AS tablesatuanawal');
    $CI->db->join('d_item_vendor', 'tablesatuanawal.id_unit=d_item_vendor.satuan_awal');
    $CI->db->join('d_item_unit AS tablesatuankonversi', 'tablesatuankonversi.id_unit=d_item_vendor.satuan_konversi');
    $CI->db->where('d_item_vendor.id_vendor', $idvendor);
    $CI->db->where('d_item_vendor.merk', $merk);
    $CI->db->where('d_item_vendor.id_brand', $id_brand);
    $CI->db->where('d_item_vendor.nama_item', $namaitem);
    $query = $CI->db->get()->result();

    foreach ($query as $key => $value) {
        $output[] = $value->namasatuanawal . " &ensp;<i class='fa fa-arrow-right'></i>&ensp; " . $value->namasatuankonversi . " &emsp;=&emsp; " . $value->nilai_konversi;
    }
    $output = implode('<br>', $output);
    return $output;
}

function get_user_group_access($iduser = null)
{
    $CI = get_instance();
    $query = $CI->db->query("SELECT a_groups.name, a_groups.id FROM a_groups INNER JOIN a_users_groups ON a_groups.id=a_users_groups.group_id WHERE a_users_groups.user_id='$iduser'")->row();
    return $query;
}

function get_satuan_from_plu($plu = null)
{
    $CI = get_instance();
    // $where = array('plu'=>$plu);
    $datarow = $CI->db->query("SELECT unit_name FROM d_item LEFT JOIN d_item_unit ON d_item.satuan_berat = d_item_unit.id_unit WHERE plu ='$plu' GROUP BY d_item.plu")->row();
    return $datarow->unit_name;
}

function get_jenis_from_plu($plu = null)
{
    $CI = get_instance();
    // $where = array('plu'=>$plu);
    $datarow = $CI->db->query("SELECT department_name FROM d_item_department WHERE department_code = MID('$plu', 1, 2 )")->row();
    // log_r($CI->db->last_query());
    return $datarow->department_name != NULL || $datarow->department_name != '' ? $datarow->department_name : '';
}

function get_jenis_harga_pelanggan($type = null, $id = null)
{
    $CI = get_instance();
    if ($type == 0) {
        $harga = 'harga_beli';
    } else {
        $harga = 'hargajual' . $type;
    }
    // $where = array('plu'=>$plu);
    $datarow = $CI->db->query("SELECT " . $harga . " as harga FROM d_item_cabang WHERE id_item_cabang = '" . $id . "'")->row();
    // log_r($CI->db->last_query());
    return $datarow->harga;
}

function get_idbrand_from_idoutlet($id = null)
{
    $CI = get_instance();
    // $where = array('plu'=>$plu);
    $datarow = $CI->db->query("SELECT id_brand FROM a_outlet WHERE id='$id'")->row();
    // log_r($id);
    return $datarow->id_brand;
}

function get_data_konversi_for_itemkota_pelanggan($idpelanggan, $id_brand, $namaitem)
{
    $output = array();
    $CI = get_instance();

    $CI->db->select('tablesatuanawal.unit_name AS namasatuanawal, tablesatuankonversi.unit_name AS namasatuankonversi, d_item_pelanggan.nilai_konversi');
    $CI->db->from('d_item_unit AS tablesatuanawal');
    $CI->db->join('d_item_pelanggan', 'tablesatuanawal.id_unit=d_item_pelanggan.satuan_awal');
    $CI->db->join('d_item_unit AS tablesatuankonversi', 'tablesatuankonversi.id_unit=d_item_pelanggan.satuan_konversi');
    $CI->db->where('d_item_pelanggan.id_pelanggan', $idpelanggan);
    $CI->db->where('d_item_pelanggan.id_brand', $id_brand);
    $CI->db->where('d_item_pelanggan.nama_item', $namaitem);
    $query = $CI->db->get()->result();
    // log_r($CI->db->last_query());
    foreach ($query as $key => $value) {
        $output[] = $value->namasatuanawal . " &ensp;<i class='fa fa-arrow-right'></i>&ensp; " . $value->namasatuankonversi . " &emsp;=&emsp; " . $value->nilai_konversi;
    }
    $output = implode('<br>', $output);
    return $output;
}

function get_id_cabang_from_brand($id = null)
{
    $CI = get_instance();
    $where = array('id_brand' => $id);
    $data = $CI->db->select('id_cabang')->get_where('a_brand', $where)->row();
    if (empty($data)) return '';
    else return $data->id_cabang;
}

function get_persentase($id = null)
{
    $CI = get_instance();
    $where = array('a.no_struk' => $id);
    $CI->db->select('c.persentase');
    $CI->db->join('d_set_user_fee as b', 'a.kode_user=b.kode');
    $CI->db->join('d_setup_fee as c', 'c.id=b.type_fee');
    $data = $CI->db->get_where('y_list_fee as a', $where)->row();
    // log_r($CI->db->last_query());
    if (empty($data)) return '0';
    else return $data->persentase;
}

function get_gudang_tujuan($plu = null, $keterangan = null)
{
    $CI = get_instance();
    $CI->db->select('C.name');
    $CI->db->from('d_item_stock_transfer AS A');
    $CI->db->join('d_gudang AS B', 'A.gudang=B.id');
    $CI->db->join('a_outlet AS C', 'B.id_outlet=C.id');
    $CI->db->where('A.keterangan LIKE', $keterangan);
    $CI->db->where('plu', $plu);
    $CI->db->where('out_qty', 0);
    $data = $CI->db->get()->row();
    // log_r($CI->db->last_query());
    if (empty($data)) return '';
    else return $data->name;
}

function get_value_done($no_dept = null, $plu = null, $dari = null, $sampai = null)
{
    $CI = get_instance();
    $where = array('keterangan LIKE' => '%' . $no_dept . '%', 'plu' => substr($plu, 0, -1), 'in_qty' => 0);
    $data = $CI->db->select('value')->get_where('d_item_stock_transfer', $where)->row();
    // log_r($CI->db->last_query());
    if (empty($data)) return '';
    else return $data->value;
}

function get_hpp_ppob($transaksi_group, $plu, $harga_jual)
{
    $CI = get_instance();

    $CI->db->select('nominal');
    $CI->db->where('transaksi_group', $transaksi_group);
    $get_hpp = $CI->db->get('e_pos_receipt')->row()->nominal;
    $hpp_fix = 0;

    if ($get_hpp == NULL) {
        $CI->db->where('plu', $plu);
        $get_admin = $CI->db->get('e_pos_fee_ppob')->row()->administration_fee;

        $hpp_fix = $harga_jual - $get_admin;
    } else {
        $hpp_fix = $get_hpp;
    }

    return $hpp_fix;
}

function get_cabang_from_brand($id = null)
{
    $CI = get_instance();
    $where = array('id_brand' => $id);
    $data = $CI->db->select('id_cabang')->get_where('a_brand', $where)->row();
    if (empty($data)) return '-';
    else return $data->id_cabang;
}


function get_cabang_from_gudang($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('id_cabang')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '-';
    else return $data->id_cabang;
}

function cek_finger_outlet()
{
    $CI = get_instance();

    $ip_finger = $CI->db->select("ip_finger")->where("id", $CI->session->userdata("outlet_id"))->get("a_outlet")->row();

    return $ip_finger ? $ip_finger->ip_finger : null;
}

function get_nama_pelanggan($id = NULL)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('nama_lengkap')->get_where('d_pelanggan', $where)->row();
    if (empty($data)) return '-';
    else return $data->nama_lengkap;
}

function terbilang($x)
{
    $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
    if ($x < 12)
        return " " . $angka[$x];
    elseif ($x < 20)
        return terbilang($x - 10) . " belas";
    elseif ($x < 100)
        return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
        return "seratus" . terbilang($x - 100);
    elseif ($x < 1000)
        return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
        return "seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
        return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
        return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
}

/**
 *
 * @param is NULL or empty string
 * @return bool
 */
function blank($params)
{
    if (empty($params)) {
        return true;
    } elseif (is_null($params)) {
        return true;
    } else {
        return false;
    }
}

function get_dept_by_plu($plu = null)
{
    $CI = get_instance();
    $dept = substr($plu, 0, 2);
    $where = array('department_code' => $dept);
    $data = $CI->db->select('department_name')->get_where('d_item_department', $where)->row();
    if (empty($data)) return '-';
    else return $dept . ' ' . $data->department_name;
}

function get_harga_from_id_gudang($id = null)
{
    $CI = get_instance();
    $where = array('id' => $id);
    $data = $CI->db->select('harga')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '-';
    else return $data->harga;
}

function get_harga_from_id_outlet($id = null)
{
    $CI = get_instance();
    $where = array('a_outlet.id' => $id);
    $CI->db->join('a_outlet', 'd_gudang.id_outlet=a_outlet.id');
    $data = $CI->db->select('d_gudang.harga')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '-';
    else return $data->harga;
}
function get_harga_from_name_gudang($id = null)
{
    $CI = get_instance();
    $where = array('nama_gudang' => $id);
    $data = $CI->db->select('harga')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '-';
    else return $data->harga;
}

function get_harga_from_name_outlet($id = null)
{
    $CI = get_instance();
    $where = array('a_outlet.name' => $id);
    $CI->db->join('a_outlet', 'd_gudang.id_outlet=a_outlet.id');
    $data = $CI->db->select('d_gudang.harga')->get_where('d_gudang', $where)->row();
    if (empty($data)) return '-';
    else return $data->harga;
}

function set_sisa_stock_from_stock_transfer($id_gudang, $plu)
{
    $CI = get_instance();
    $lastSO = $CI->db->query("SELECT tanggal,keterangan FROM d_item_stock_transfer WHERE gudang = '$id_gudang' AND keterangan LIKE 'Stockopname%' AND plu = '$plu' ORDER BY tanggal DESC LIMIT 1")->row();
    $today = date("Y-m-d H:i:s");

    //UPDATE Stock
    $stock_update = 0;
    $stock_so     = 0;
    $where = array('gudang' => $id_gudang, 'plu' => $plu);
    if ($lastSO != null) {
        $no_so = preg_replace("/[^0-9]/", '', $lastSO->keterangan);
        $sisa_sistem_so = $CI->db->query("SELECT sisa_stock as total FROM d_item_stockopname_detail WHERE   no_so = '$no_so' AND plu = '$plu'")->row();
        $CI->db->where("tanggal >= '$lastSO->tanggal'");
        $CI->db->where("tanggal <= '$today'");
        $saldo_awal = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();
        @$stock_update = $saldo_awal->total + $sisa_sistem_so->total;
        $stock_so     = $sisa_sistem_so->total;

        // log_r($stock_update);
    } else {
        $CI->db->where("tanggal <= '$today'");
        $saldo_awal = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();
        @$stock_update = $saldo_awal->total;
    }
    $query = $CI->db->query("UPDATE d_item_stock SET qty='$stock_update' WHERE plu='$plu' and id_gudang =" . $id_gudang);

    if (!$query) {
        $CI->db->query("INSERT INTO d_item_stock (plu, id_cabang, id_item_cabang, id_gudang, qty, qty_sistem_so, last_so) VALUES('$plu','get_cabang_from_gudang($id_gudang)','0','$id_gudang','$stock_update','$sisa_sistem_so->total','$lastSO->tanggal')");
    }

    // $CI->db->query("UPDATE d_item_stock, (SELECT plu as no_pro, ROUND(sum(in_qty) - sum(out_qty),2) as sisa from d_item_stock_transfer where gudang =".$id_gudang." GROUP BY plu) as sisa_stok SET qty=sisa WHERE plu=no_pro and id_gudang =".$id_gudang);
}

##################### SYCRON STOCK  ##########################

function get_sisa_stock_from_stock_transfer($id_gudang, $plu, $today)
{
    $CI = get_instance();
    $month     = date("m", strtotime(date("Y-m-d", strtotime($today)) . "-1 month"));
    $month2    = date("m", strtotime(date("Y-m-d", strtotime($today)) . "-2 month"));
    $date      = date("Y-m-d", strtotime($today));
    $tgldari   = $date . " 00:00:00";
    $tglsmp    = $date . " 23:59:59";

    $lastSO = $CI->db->query("SELECT a.tanggal, a.no_so, b.sisa_stock FROM d_item_stockopname_master a JOIN d_item_stockopname_detail b ON a.no_so = b.no_so WHERE a.tanggal <= '$tgldari' AND a.gudang = '$id_gudang' AND b.plu = '$plu' ORDER BY tanggal DESC LIMIT 1")->row();

    //UPDATE Stock
    $saldo_awal_fix = 0;
    $where = array('gudang' => $id_gudang, 'plu' => $plu);

    if ($lastSO == null) {
        $saldo_awal = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->where("tanggal < '$tglsmp'")->get_where('d_item_stock_transfer', $where)->row();
        $saldo_awal_fix = ($saldo_awal->total == null ? 0 : ROUND($saldo_awal->total, 2));
    } else {
        $CI->db->where("tanggal >= '$lastSO->tanggal'");
        $saldo_awal = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->where("tanggal <= '$tglsmp'")->get_where('d_item_stock_transfer', $where)->row();
        @$saldo_awal_fix = ROUND($saldo_awal->total, 2) + $lastSO->sisa_stock;
    }

    $saldo_fix = $saldo_awal_fix;
    $saldo_up = 0;
    if ($saldo_fix > 0) {
        $saldo_up = $saldo_fix;
    }

    return $saldo_up;
}

function get_stock_so($id_gudang, $plu)
{
    $CI = get_instance();

    $tgldari = date("Y-m-d H:i:s");
    $CI->db->where('id_gudang', $id_gudang);
    $CI->db->where('plu', $plu);
    $get_last_so = $CI->db->get('d_item_stock')->row();

    if (!empty($get_last_so)) {
        $CI->db->select('d_item_stockopname_master.no_so,d_item_stockopname_detail.sisa_stock');
        $CI->db->join('d_item_stockopname_detail', 'd_item_stockopname_detail.no_so = d_item_stockopname_master.no_so');
        $CI->db->where('d_item_stockopname_master.tanggal', $get_last_so->last_so);
        $CI->db->where('d_item_stockopname_master.gudang', $id_gudang);
        $CI->db->where('d_item_stockopname_detail.plu', $plu);
        $cekSO = $CI->db->get('d_item_stockopname_master')->row();
        if (empty($cekSO)) {
            $qty_so = $get_last_so->qty_sistem_so;
        } else {
            $CI->db->update('d_item_stock', array('qty_sistem_so' => $cekSO->sisa_stock), array('id_gudang' => $id_gudang, 'plu' => $plu));
            $qty_so = $cekSO->sisa_stock;
        }

        $where = array('gudang' => $id_gudang, 'plu' => $plu);
        $CI->db->where("tanggal >= '$get_last_so->last_so'");
        $saldo_awal = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->where("tanggal <= '$tgldari'")->get_where('d_item_stock_transfer', $where)->row();
        @$saldo_awal_fix = ROUND($saldo_awal->total, 2) + $qty_so;
    } else {
        @$saldo_awal_fix = get_stock_today($id_gudang, $plu);
    }

    $saldo_up = 0;
    if ($saldo_awal_fix > 0) {
        $saldo_up = ROUND($saldo_awal_fix, 2);
    }

    $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$plu' and id_gudang =" . $id_gudang);

    return $saldo_up;
}

function get_stock_today($id_gudang, $plu, $tgl_new = '')
{
    $CI = get_instance();
    if ($tgl_new == '' || $tgl_new == '1') {
        $tgldari = date("Y-m-d");
    } else {
        $tgldari = date("Y-m-d", strtotime('1 day', strtotime($tgl_new)));
    }
    $CI->db->where('id_gudang', $id_gudang);
    $CI->db->where('plu', $plu);
    $CI->db->where('tanggal = "' . $tgldari . '" - INTERVAL 1 DAY');
    $get_last_so = $CI->db->get('d_potret_stock')->row();
    if (!empty($get_last_so)) {
        if ($tgl_new == '') {
            $saldo_up = $CI->db->get_where('d_item_stock', array('plu' => $plu, 'id_gudang' => $id_gudang))->row();
            if (empty($saldo_up)) {
                $saldo_up = 0;
            } else {
                $saldo_up = $saldo_up->qty;
            }
        } else {
            $where = array('gudang' => $id_gudang, 'plu' => $plu);
            $CI->db->where("DATE(tanggal)", $tgldari);
            $saldo_awal = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();
            @$saldo_awal_fix = ROUND($saldo_awal->total, 2) + $get_last_so->stock;

            $saldo_up = 0;
            if ($saldo_awal_fix > 0) {
                $saldo_up = $saldo_awal_fix;
            }
            $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$plu' and id_gudang =" . $id_gudang);
        }
    } else {
        $where = array('gudang' => $id_gudang, 'plu' => $plu);
        $awal_tahun = date('Y') . '-01-01';
        $date_b     = date('Y-m-d', strtotime('-1 day', strtotime($tgldari)));
        $CI->db->where("DATE(tanggal) >=", $awal_tahun);
        $CI->db->where("DATE(tanggal) <=", $date_b);
        $saldo_awal1 = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();

        $brand = get_id_brand_from_outlet($id_gudang);
        $get_harga = get_harga_item_cabang($brand, $plu);
        $data_insert = array(
            'tanggal' => $date_b,
            'plu' => $plu,
            'id_cabang' => get_id_cabang_from_gudang($id_gudang),
            'id_brand' => $brand,
            'id_gudang' => $id_gudang,
            'stock' => ROUND($saldo_awal1->total, 2),
            'hpp' => $get_harga->harga_beli,
            'ma' => $get_harga->hargabeli_ma,
            'hj' => $get_harga->hargajual1,
            'v_hpp' => $get_harga->harga_beli * ROUND($saldo_awal1->total, 2),
            'v_ma' => $get_harga->hargabeli_ma * ROUND($saldo_awal1->total, 2),
            'v_hj' =>  $get_harga->hargajual1 * ROUND($saldo_awal1->total, 2),
            'satuan' => get_satuan_from_plu($plu)
        );

        $CI->db->insert('d_potret_stock', $data_insert);

        $where = array('gudang' => $id_gudang, 'plu' => $plu);
        $CI->db->where("DATE(tanggal)", $tgldari);
        $saldo_awal2 = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();
        @$saldo_awal_fix = ROUND($saldo_awal2->total, 2) + $saldo_awal1->total;

        $saldo_up = 0;
        if ($saldo_awal_fix > 0) {
            $saldo_up = $saldo_awal_fix;
        }
        $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$plu' and id_gudang =" . $id_gudang);
    }

    return $saldo_up;
}

function get_stock_before($id_gudang, $plu, $tgl_b)
{
    $CI = get_instance();

    $tgldari = $tgl_b;
    $CI->db->where('id_gudang', $id_gudang);
    $CI->db->where('plu', $plu);
    $CI->db->where('tanggal = "' . $tgldari . '" - INTERVAL 1 DAY');
    $get_last_so = $CI->db->get('d_potret_stock')->row();
    if (!empty($get_last_so)) {
        $saldo_awal_fix = $get_last_so->stock;
    } else {
        $where = array('gudang' => $id_gudang, 'plu' => $plu);
        $awal_tahun = date('Y') . '-01-01';
        $date_b     = date('Y-m-d', strtotime('-1 day', strtotime($tgldari)));
        $CI->db->where("DATE(tanggal) >=", $awal_tahun);
        $CI->db->where("DATE(tanggal) <=", $date_b);
        $saldo_awal1 = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();

        $brand = get_id_brand_from_outlet($id_gudang);
        $get_harga = get_harga_item_cabang($brand, $plu);
        $data_insert = array(
            'tanggal' => $date_b,
            'plu' => $plu,
            'id_cabang' => get_id_cabang_from_gudang($id_gudang),
            'id_brand' => $brand,
            'id_gudang' => $id_gudang,
            'stock' => ROUND($saldo_awal1->total, 2),
            'hpp' => $get_harga->harga_beli,
            'ma' => $get_harga->hargabeli_ma,
            'hj' => $get_harga->hargajual1,
            'v_hpp' => $get_harga->harga_beli * ROUND($saldo_awal1->total, 2),
            'v_ma' => $get_harga->hargabeli_ma * ROUND($saldo_awal1->total, 2),
            'v_hj' =>  $get_harga->hargajual1 * ROUND($saldo_awal1->total, 2),
            'satuan' => get_satuan_from_plu($plu)
        );

        $CI->db->insert('d_potret_stock', $data_insert);

        $where = array('gudang' => $id_gudang, 'plu' => $plu);
        $CI->db->where("DATE(tanggal)", $tgldari);
        $saldo_awal2 = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();
        @$saldo_awal_fix = ROUND($saldo_awal2->total, 2) + $saldo_awal1->total;

        $saldo_up = 0;
        if ($saldo_awal_fix > 0) {
            $saldo_up = $saldo_awal_fix;
        }

        $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$plu' and id_gudang =" . $id_gudang);
    }

    $saldo_up = 0;
    if ($saldo_awal_fix > 0) {
        $saldo_up = $saldo_awal_fix;
    }

    // $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$plu' and id_gudang =" . $id_gudang);

    return $saldo_up;
}

function set_stock_before($id_gudang)
{
    $CI = get_instance();

    $tgldari = date('Y-m-d');
    $get_last_so = $CI->db->query('SELECT   plu FROM    d_item_stock WHERE  id_gudang = "' . $id_gudang . '"    AND plu NOT IN ( SELECT plu FROM d_potret_stock WHERE   id_gudang = "' . $id_gudang . '"    AND tanggal = DATE_SUB( CURRENT_DATE, INTERVAL 1 DAY )  )')->result();
    foreach ($get_last_so as $key => $value) {

        $where = array('gudang' => $id_gudang, 'plu' => $value->plu);
        $awal_tahun = date('Y') . '-01-01';
        $date_b     = date('Y-m-d', strtotime('-1 day', strtotime($tgldari)));
        $CI->db->where("DATE(tanggal) >=", $awal_tahun);
        $CI->db->where("DATE(tanggal) <=", $date_b);
        $saldo_awal1 = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();

        $brand = get_id_brand_from_outlet($id_gudang);
        $get_harga = get_harga_item_cabang($brand, $value->plu);
        $data_insert = array(
            'tanggal' => $date_b,
            'plu' => $plu,
            'id_cabang' => get_id_cabang_from_gudang($id_gudang),
            'id_brand' => $brand,
            'id_gudang' => $id_gudang,
            'stock' => ROUND($saldo_awal1->total, 2),
            'hpp' => $get_harga->harga_beli,
            'ma' => $get_harga->hargabeli_ma,
            'hj' => $get_harga->hargajual1,
            'v_hpp' => $get_harga->harga_beli * ROUND($saldo_awal1->total, 2),
            'v_ma' => $get_harga->hargabeli_ma * ROUND($saldo_awal1->total, 2),
            'v_hj' =>  $get_harga->hargajual1 * ROUND($saldo_awal1->total, 2),
            'satuan' => get_satuan_from_plu($plu)
        );

        $CI->db->insert('d_potret_stock', $data_insert);

        $where = array('gudang' => $id_gudang, 'plu' => $plu);
        $CI->db->where("DATE(tanggal)", $tgldari);
        $saldo_awal2 = $CI->db->select('COALESCE(SUM(in_qty),0) - COALESCE(SUM(out_qty),0) as total')->get_where('d_item_stock_transfer', $where)->row();
        @$saldo_awal_fix = ROUND($saldo_awal2->total, 2) + $saldo_awal1->total;

        $saldo_up = 0;
        if ($saldo_awal_fix > 0) {
            $saldo_up = $saldo_awal_fix;
        }

        $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$value->plu' and id_gudang =" . $id_gudang);
    }
}

##################### SYCRON STOCK  ##########################

function get_percentage($total, $number)
{
    if ($total > 0) {
        return round($number / ($total / 100));
    } else {
        return 0;
    }
}

function get_harga_item_cabang($brand, $plu)
{
    $CI = get_instance();
    $data = $CI->db->query("SELECT harga_beli, hargabeli_ma, hargajual1 FROM d_item_cabang WHERE plu='$plu' and id_brand =" . $brand)->row();

    return $data;
}

function get_sisa_stock($id_gudang, $plu)
{
    $CI = get_instance();
    $data = $CI->db->query("SELECT qty FROM d_item_stock WHERE plu='$plu' and id_gudang =" . $id_gudang)->row();

    return $data->qty;
}

function set_sisa_stock($saldo_up, $id_gudang, $plu)
{
    $CI = get_instance();
    $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$plu' and id_gudang =" . $id_gudang);
}

// function

function timeAgo($timestamp)
{
    $time = time() - $timestamp;

    if ($time < 60)
        return ($time > 1) ? $time . ' detik yang lalu' : 'satu detik';
    elseif ($time < 3600) {
        $tmp = floor($time / 60);
        return ($tmp > 1) ? $tmp . ' menit yang lalu' : ' satu menit yang lalu';
    } elseif ($time < 86400) {
        $tmp = floor($time / 3600);
        return ($tmp > 1) ? $tmp . ' jam yang lalu' : ' satu jam yang lalu';
    } elseif ($time < 2592000) {
        $tmp = floor($time / 86400);
        return ($tmp > 1) ? $tmp . ' hari lalu' : ' satu hari lalu';
    } elseif ($time < 946080000) {
        $tmp = floor($time / 2592000);
        return ($tmp > 1) ? $tmp . ' bulan lalu' : ' satu bulan lalu';
    } else {
        $tmp = floor($time / 946080000);
        return ($tmp > 1) ? $tmp . ' years' : ' a year';
    }
}

function getNomorInOut($idWorker, $tanggal)
{
    $CI = get_instance();
    $where = array('worker_id' => $idWorker, 'date_inout' => $tanggal, 'status_inout' => 1);
    $data = $CI->db->select('nomor')->get_where('c_inout_finger', $where)->row();
    if (empty($data)) return '';
    else return $data->nomor;
}

function getNomorDopMasuk($idWorker, $tanggal)
{
    $CI = get_instance();
    $where = array('worker_id' => $idWorker, 'date_dop' => $tanggal, 'status_dop' => 1, 'type_dop' => 1);
    $data = $CI->db->select('nomor, reason')->get_where('c_dop_finger', $where)->row();
    if (empty($data)) return '';
    else return $data;
}

function pesanpengajuan($nama, $jenispengajuan, $kode, $alasan, $statuspengaju, $oleh, $link)
{
    $pesan = 'Nama : ' . $nama;
    $pesan .= "\n";
    $pesan .= 'Pengajuan : ' . $jenispengajuan;
    $pesan .= "\n";
    $pesan .= 'Kode : ' . $kode;
    $pesan .= "\n";
    $pesan .= 'Alasan / Keterangan : ' . $alasan;
    $pesan .= "\n";
    ($statuspengaju == 1) ? $kata = 'Dibuat oleh : ' : $kata = 'Diperbaruhi oleh : ';
    $pesan .= $kata . $oleh;
    if ($link == true) {
        $pesan .= "\n";
        $pesan .= 'Silahkan di proses lebih lanjut.<a href="http://www.villacorp.systems/"> Menuju website</a>.';
    }

    return $pesan;
}

function pesankekaryawan($nama, $jenispengajuan, $kode, $status, $oleh, $link)
{
    $pesan = 'Nama : ' . $nama;
    $pesan .= "\n";
    $pesan .= 'Pengajuan : ' . $jenispengajuan;
    $pesan .= "\n";
    $pesan .= 'Kode : ' . $kode;
    $pesan .= "\n";
    $pesan .= 'Status : ' . $status;
    $pesan .= "\n";
    $pesan .= 'Diselesaikan oleh : ' . $oleh;
    if ($link == true) {
        $pesan .= "\n";
        $pesan .= 'Silahkan kunjungi website untuk informasi lebih detail.<a href="http://www.villacorp.systems/"> Menuju website</a>.';
    }

    return $pesan;
}

function pesanKeHR($nama, $jenispengajuan, $kode, $status, $oleh, $pesanTambahan)
{
    $pesan = 'Nama : ' . $nama;
    $pesan .= "\n";
    $pesan .= 'Pengajuan : ' . $jenispengajuan;
    $pesan .= "\n";
    $pesan .= 'Kode : ' . $kode;
    $pesan .= "\n";
    $pesan .= 'Status : ' . $status;
    $pesan .= "\n";
    $pesan .= 'Diselesaikan oleh : ' . $oleh;
    $pesan .= "\n\n" . $pesanTambahan;

    return $pesan;
}

function pesankesekurity($nama, $jenispengajuan, $kode, $alasan)
{
    $pesan = 'Nama : ' . $nama;
    $pesan .= "\n";
    $pesan .= 'Pengajuan : ' . $jenispengajuan;
    $pesan .= "\n";
    $pesan .= 'Kode : ' . $kode;
    $pesan .= "\n";
    $pesan .= 'Alasan / Keterangan : ' . $alasan;
    return $pesan;
}

function pesanlemburkaryawan($nama, $jenispengajuan, $kode, $status, $alasan, $dibuat, $statuspengaju)
{
    $pesan = 'Nama : ' . $nama;
    $pesan .= "\n";
    $pesan .= 'Pengajuan : ' . $jenispengajuan;
    $pesan .= "\n";
    $pesan .= 'Kode : ' . $kode;
    $pesan .= "\n";
    $pesan .= 'Alasan / Keterangan : ' . $alasan;
    $pesan .= "\n";
    ($statuspengaju == 1) ? $kata = 'Dibuat oleh : ' : $kata = 'Diperbaruhi oleh : ';
    $pesan .= $kata . $dibuat;
    $pesan .= "\n";
    $pesan .= 'Status : ' . $status;
    $pesan .= "\n";
    $pesan .= 'Silahkan kunjungi website untuk proses persetujuan.<a href="http://www.villacorp.systems/"> Menuju website</a>';
    return $pesan;
}

function getAllApproval($employeeId, $url)
{
    $CI = get_instance();
    $ceklistapproval = $CI->db->get_where('y_list_approval', array('worker_id' => $employeeId))->row();
    if (empty($ceklistapproval)) {
        $CI->session->set_flashdata('failed', 'Data approval anda kosong. Silahkan hubungi HRD untuk info lebih lanjut.');
        redirect($url);
    }

    $roleapprove[] = $ceklistapproval->approval_1;
    $roleapprove[] = $ceklistapproval->approval_2;
    $roleapprove[] = $ceklistapproval->approval_3;
    $roleapprove[] = $ceklistapproval->approval_4;

    return $roleapprove;
}

function getAllApprovalApi($employeeId)
{
    $CI = get_instance();
    $ceklistapproval = $CI->db->get_where('y_list_approval', array('worker_id' => $employeeId))->row();
    if (empty($ceklistapproval)) {
        return false;
    }

    $roleapprove[] = $ceklistapproval->approval_1;
    $roleapprove[] = $ceklistapproval->approval_2;
    $roleapprove[] = $ceklistapproval->approval_3;
    $roleapprove[] = $ceklistapproval->approval_4;

    return $roleapprove;
}

function getApprovalBertingkat($employeeId, $statusapprove, $roleapprovesekarang, $url)
{
    $CI = get_instance();
    $CI->load->model(array('m_crud', 'm_home'));
    $cekApproval = $CI->m_crud->getById('y_list_approval', array('worker_id' => $employeeId));
    if (!empty($cekApproval)) {
        if ($statusapprove == 1) {
            if (!empty($cekApproval->approval_1) & !empty($cekApproval->approval_2) & ($cekApproval->approval_1 == $roleapprovesekarang)) {
                $status = 0;
                $roleselanjutnya = $cekApproval->approval_2;
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_2);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_3);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_4);
            } elseif (!empty($cekApproval->approval_2) & !empty($cekApproval->approval_3) & ($cekApproval->approval_2 == $roleapprovesekarang)) {
                $status = 0;
                $roleselanjutnya = $cekApproval->approval_3;
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_3);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_4);
            } elseif (!empty($cekApproval->approval_3) & !empty($cekApproval->approval_4) & ($cekApproval->approval_3 == $roleapprovesekarang)) {
                $status = 0;
                $roleselanjutnya = $cekApproval->approval_4;
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_4);
            } else {
                $status = 1;
                $roleselanjutnya = $roleapprovesekarang;
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_1);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_2);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_3);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_4);
            }
        } elseif ($statusapprove == 2) {
            $status = 2;
            $roleselanjutnya = $roleapprovesekarang;
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_1);
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_2);
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_3);
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->approval_4);
        }

        return array('status' => $status, 'roleselanjutnya' => $roleselanjutnya, 'notif' => $idTelegram);
    } else {
        $CI->session->set_flashdata('failed', 'Silahkan isi data approval terlebih dahulu');
        redirect($url);
    }
}

function getApprovalBertingkatDinas($table, $idDinas, $statusapprove, $roleapprovesekarang, $url)
{
    $CI = get_instance();
    $CI->load->model(array('m_crud', 'm_home'));
    $cekApproval = $CI->m_crud->getById($table, array('id' => $idDinas));
    if (!empty($cekApproval)) {
        if ($statusapprove == 1) {
            if (!empty($cekApproval->ap1) & ($cekApproval->ap1 == $roleapprovesekarang)) {
                if (!empty($cekApproval->ap2)) {
                    $status = 0;
                    $roleselanjutnya = $cekApproval->ap2;
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap2);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
                } elseif (empty($cekApproval->ap2) & empty($cekApproval->ap3) & empty($cekApproval->ap4)) {
                    $status = 1;
                    $roleselanjutnya = $roleapprovesekarang;
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap1);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap2);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
                } else {
                    $status = 0;
                    $roleselanjutnya = $cekApproval->ap3;
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap2);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
                }
            } elseif (!empty($cekApproval->ap2) & ($cekApproval->ap2 == $roleapprovesekarang)) {
                if (!empty($cekApproval->ap3)) {
                    $status = 0;
                    $roleselanjutnya = $cekApproval->ap3;
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
                } elseif (empty($cekApproval->ap3) & empty($cekApproval->ap4)) {
                    $status = 1;
                    $roleselanjutnya = $roleapprovesekarang;
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap1);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap2);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
                } else {
                    $status = 0;
                    $roleselanjutnya = $cekApproval->ap4;
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
                    $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
                }
            } elseif (!empty($cekApproval->ap3) & !empty($cekApproval->ap4) & ($cekApproval->ap3 == $roleapprovesekarang)) {
                $status = 0;
                $roleselanjutnya = $cekApproval->ap4;
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
            } else {
                $status = 1;
                $roleselanjutnya = $roleapprovesekarang;
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap1);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap2);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
                $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
            }
        } elseif ($statusapprove == 2) {
            $status = 2;
            $roleselanjutnya = $roleapprovesekarang;
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap1);
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap2);
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap3);
            $idTelegram[] = $CI->m_home->findIdTelegram($cekApproval->ap4);
        }

        return array('status' => $status, 'roleselanjutnya' => $roleselanjutnya, 'notif' => $idTelegram);
    } else {
        $CI->session->set_flashdata('failed', 'Silahkan isi data approval terlebih dahulu');
        redirect($url);
    }
}

function historyDinas($ap1, $ap2, $ap3, $ap4, $ra, $ds)
{
    $CI = get_instance();

    $data['ap1'] = (!empty($ap1)) ? $CI->m_dinas->getKaryawanbyUserId($ap1) : '';
    $data['ap2'] = (!empty($ap2)) ? $CI->m_dinas->getKaryawanbyUserId($ap2) : '';
    $data['ap3'] = (!empty($ap3)) ? $CI->m_dinas->getKaryawanbyUserId($ap3) : '';
    $data['ap4'] = (!empty($ap4)) ? $CI->m_dinas->getKaryawanbyUserId($ap4) : '';

    $dataApproval = array(
        'ap1' => $ap1,
        'ap2' => $ap2,
        'ap3' => $ap3,
        'ap4' => $ap4
    );

    $statusnya = true;
    $reject = false;
    foreach ($dataApproval as $key => $value) {
        if (!empty($value)) {
            if ($value == $ra) {
                if ($ds == 0) {
                    $data[$key]->status = 'Need Approve';
                    $statusnya = false;
                } elseif ($ds == 2) {
                    $data[$key]->status = 'Rejected';
                    $statusnya = true;
                    $reject = true;
                } else {
                    $data[$key]->status = 'Approved';
                    $statusnya = true;
                }
            } elseif ($statusnya == false) {
                $data[$key]->status = 'Need Approve';
            } elseif ($reject == true) {
                $data[$key]->status = 'Rejected';
            } else {
                $data[$key]->status = 'Approved';
            }
        }
    }

    return $data;
}

function get_kemasan($id = null)
{
    $CI = get_instance();
    $where = array('id_kemasan' => $id);
    $data = $CI->db->select('nama_kemasan')->get_where('d_kemasan', $where)->row();
    if (empty($data)) return '-';
    else return $data->nama_kemasan;
}

function getAksesMenuPengobatan()
{
    $CI = get_instance();
    return $CI->db->get_where('c_pengobatan_akses_menu', ['worker_id' => ID_WORKER, 'status' => 1])->row();
}

function checkAgendaActive()
{
    $CI = get_instance();
    $CI->load->model(array('m_agenda'));
    // $CI->m_agenda->getActiveAgenda()->result();
    $toAgenda = $CI->m_agenda->getActiveAgenda()->result();
    $today = date("Y-m-d");
    $now = date("H:i:s");
    foreach ($toAgenda as $key => $value) {
        // log_r(strtotime($today));
        if (strtotime($value->tanggal) <= strtotime($today) && strtotime($value->jam_selesai) <= strtotime($now)) {
            $CI->m_agenda->setNonActive($value->id);
        }
    }
}

function checkInfoActive()
{
    $CI = get_instance();
    $CI->load->model(array('m_info'));
    // $CI->m_agenda->getActiveAgenda()->result();
    $toInfo = $CI->m_info->getActiveInfo()->result();
    $today = date("Y-m-d");
    foreach ($toInfo as $key => $value) {
        // log_r($value->tanggal_selesai);
        if (strtotime($value->tanggal_selesai) < strtotime($today)) {
            $CI->m_info->setNonActive($value->id);
        }
    }
}


function rupiah($angka)
{
    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;
}

function hari($date)
{
    $hari = date("D", strtotime($date));

    switch ($hari) {
        case 'Sun':
            $hari_ini = "Minggu";
            break;

        case 'Mon':
            $hari_ini = "Senin";
            break;

        case 'Tue':
            $hari_ini = "Selasa";
            break;

        case 'Wed':
            $hari_ini = "Rabu";
            break;

        case 'Thu':
            $hari_ini = "Kamis";
            break;

        case 'Fri':
            $hari_ini = "Jumat";
            break;

        case 'Sat':
            $hari_ini = "Sabtu";
            break;

        default:
            $hari_ini = "Tidak di ketahui";
            break;
    }

    return "<b>" . $hari_ini . "</b>";
}

function generate_cuti()
{
    $CI = get_instance();
    $CI->load->model(array('m_karyawan'));
    $CI->m_karyawan->generateCuti();
}

function company_check($brand = null)
{
    $CI = get_instance();
    $cek = $CI->db->select('id_brand')->get_where('a_brand', array('company_id' => 4, 'id_cabang' => 2))->result();
    $listbrand = array();

    foreach ($cek as $val) {
        $listbrand[] = $val->id_brand;
    }

    if (in_array($brand, $listbrand)) {
        return true;
    } else {
        return false;
    }
}

function status_adjustment($no_modul)
{
    $CI = get_instance();
    $CI->db->select("status, no_adjustment");
    $CI->db->from("e_pos_adjustment_master");
    $CI->db->where("no_referensi = ", $no_modul);
    return $CI->db->get()->row();
}

function cek_last_so_item($plu, $id_gudang)
{
    $CI = get_instance();
    $CI->db->select('tanggal');
    $CI->db->from('d_item_stock_transfer');
    $CI->db->where('plu =', $plu);
    $CI->db->where('gudang =', $id_gudang);
    $CI->db->where('SUBSTR(keterangan,1,11) = "Stockopname"');
    $CI->db->order_by('id', 'desc');
    $CI->db->limit('1');
    return $CI->db->get()->row();
}

function get_satuan_terkecil($plu)
{
    $CI = get_instance();
    $CI->db->select("d_item.satuan_berat, d_item_unit.unit_name, d_item.type_item");
    $CI->db->from("d_item");
    $CI->db->join("d_item_unit", "d_item.satuan_berat=d_item_unit.id_unit");
    $CI->db->where("plu = ", $plu);
    return $CI->db->get()->row();
}

function get_nilai_konversi($id_satuan, $satuanitem, $plu, $id_vendor, $id_brand)
{
    $CI = get_instance();
    $CI->db->select("nilai_konversi, harga_beli");
    $CI->db->from("d_item_vendor");
    $CI->db->where("satuan_awal = ", $id_satuan);
    $CI->db->where("satuan_konversi = ", $satuanitem);
    $CI->db->where("plu = ", $plu);
    $CI->db->where("id_vendor = ", $id_vendor);
    // $CI->db->where("merk = ",$merk);
    $CI->db->where("id_brand = ", $id_brand);
    return $CI->db->get()->row();
}

function get_item_last_stock($plu, $id_gudang)
{
    $CI = get_instance();
    $CI->db->select("qty");
    $CI->db->from("d_item_stock");
    $CI->db->where("plu = ", $plu);
    $CI->db->where("id_gudang = ", $id_gudang);
    return $CI->db->get()->row()->qty;
}

function insert_master_adj($data)
{
    $CI = get_instance();
    return $CI->db->insert("e_pos_adjustment_master", $data);
}

function insert_adj_helper($data, $table)
{
    $CI = get_instance();
    return $CI->db->insert($table, $data);
}

function insert_adj_list_helper($data, $table)
{
    $CI = get_instance();
    return $CI->db->insert_batch($table, $data);
}

function cek_status_adjustment($no_urut_pembelian, $no_adjustment)
{
    $CI = get_instance();
    $CI->db->select("no_adjustment");
    $CI->db->from("e_pos_adjustment_master");
    $CI->db->where("no_referensi = ", $no_urut_pembelian);
    $CI->db->where("no_adjustment = ", $no_adjustment);
    $CI->db->where("status = ", null);
    return $CI->db->get()->row();
}

function get_gudang_from_group($group_name = '')
{
    $CI = get_instance();
    $id_gudang = get_id_gudang_from_outlet(GUDANG);
    $getOutlet = $CI->db->query("SELECT linkOutlet From a_outlet WHERE id='" . GUDANG . "'")->row();
    $outlet = explode(",", $getOutlet->linkOutlet);
    $CI->db->select('id');
    $CI->db->from('d_gudang');
    $CI->db->where('id_cabang', CABANG);
    $CI->db->where_in('id_outlet', $outlet);
    $CI->db->where_in('group_name', $group_name);
    $CI->db->or_where('id', $id_gudang);
    return $CI->db->get()->row()->id;
}

function insert_sisa_stock_from_stock_transfer($id_gudang, $plu, $today)
{
    $CI = get_instance();
    $date = date("Y-m-d", strtotime($today));
    $tgldari = $date . " 00:00:00";
    $tglsmp = $date . " 23:59:59";

    //array gudang musnah & reject
    $lastSO = $CI->db->query("SELECT a.tanggal, a.no_so, b.sisa_stock FROM d_item_stockopname_master a JOIN d_item_stockopname_detail b ON a.no_so = b.no_so WHERE a.tanggal <= '$tgldari' AND a.gudang = '$id_gudang' AND b.plu = '$plu' ORDER BY tanggal DESC LIMIT 1")->row();

    //UPDATE Stock
    $stock_update = 0;
    $saldo_awal_fix = 0;
    $where = array('gudang' => $id_gudang, 'plu' => $plu);
    if ($lastSO == null) {
        // $saldo_awal = $CI->db->select('SUM(in_qty) - SUM(out_qty) as total')->where("tanggal < '$tgldari'")->get_where('d_item_stock_transfer', $where)->row();
        // $saldo_awal_fix = ($saldo_awal->total == null ? 0 : $saldo_awal->total);
        // exit("error6");
    } else {
        $CI->db->where("tanggal >= '$lastSO->tanggal'");
        $saldo_awal = $CI->db->select('SUM(in_qty) - SUM(out_qty) as total')->where("tanggal <= '$tglsmp'")->get_where('d_item_stock_transfer', $where)->row();
        @$saldo_awal_fix = $saldo_awal->total + $lastSO->sisa_stock;
    }

    $saldo_fix = $saldo_awal_fix;
    $saldo_up = 0;
    if ($saldo_fix >= 0) {
        $saldo_up = $saldo_fix;
    }
    $CI->db->query("UPDATE d_item_stock SET qty='$saldo_up' WHERE plu='$plu' and id_gudang =" . $id_gudang);
}

function save_adj_master($no_adj, $data)
{
    $CI = get_instance();
    $CI->db->where("no_adjustment = ", $no_adj);
    return $CI->db->update("e_pos_adjustment_master", $data);
}


// CHECK PERMISSION VS


function checked_menu_btn($menu, $crud)
{
    if ($menu != "N;") {
        if (in_array($crud, unserialize($menu))) {
            echo "checked";
        }
    }
}

function check_permission_menu($id_group, $menu, $status = false)
{
    $CI = get_instance();
    // $ceklebihrole = $CI->db->get_where('a_users_role', array('id_user_group' => $id_group, 'id_menu' => $menu))->num_rows();
    $data = $CI->db->get_where('a_users_role', array('id_user_group' => $id_group, 'id_menu' => $menu))->row();
    if (!$data) {
        return $status;
    } else {
        if (!empty($data->user_permission)) {
            if ($data->user_permission != "N;") {
                if (in_array("read", unserialize($data->user_permission))) {
                    return !$status;
                } else {
                    return $status;
                }
            } else {
                return $status;
            }
        } else {
            return $status;
        }
    }
}

function check_permission_view($id_group, $crud, $url, $status = true)
{
    $CI = get_instance();

    $where = array(
        'b.id_user_group' => $id_group,
        'a.menu_url' => $url,
    );

    $CI->db->select('b.id_menu,b.id_user_group,b.user_permission,a.menu_url');
    $CI->db->from('a_menu a');
    $CI->db->join('a_users_role b', 'a.id_menu=b.id_menu');
    $CI->db->join('a_groups c', 'b.id_user_group=c.id');
    $CI->db->where($where);

    $dt = $CI->db->get()->row();

    if (!empty($dt->user_permission)) {
        if ($dt->user_permission != "N;") {
            if (in_array($crud, unserialize($dt->user_permission))) {
                return $status;
            }
        } else {
            return !$status;
        }
    } else {
        return !$status;
    }
}

function check_permission_page($id_group, $crud, $url, $statuslog = true)
{
    $CI = get_instance();

    $where = array(
        'b.id_user_group' => $id_group,
        'a.menu_url' => $url,
    );

    $CI->db->select('b.id_menu,b.id_user_group,b.user_permission,a.menu_url');
    $CI->db->from('a_menu a');
    $CI->db->join('a_users_role b', 'a.id_menu=b.id_menu');
    $CI->db->join('a_groups c', 'b.id_user_group=c.id');
    $CI->db->where($where);

    $dt = $CI->db->get()->row();

    if (!empty($dt->user_permission)) {
        if ($dt->user_permission != "N;") {

            if (!in_array($crud, unserialize($dt->user_permission))) {
                redirect(base_url());
            }

            if ($statuslog == true) {
                saveLog($crud, $url);
            }
        } else {
            redirect(base_url());
        }
    } else {
        redirect(base_url());
    }
}

function saveLog($tipe, $url)
{
    $CI = get_instance();
    $ip = getRealIpAddr();
    $CI->db->insert('a_log_user', ['id_user' => USER_ID, 'tipe' => $tipe, 'url' => base_url($url), 'ip_client' => $ip]);
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function check_ajax_request()
{
    $CI = get_instance();
    if (!$CI->input->is_ajax_request()) {
        redirect(base_url());
    }
}

function check_post_request()
{
    $CI = get_instance();
    if (!$CI->input->post()) {
        redirect(base_url());
    }
}

function check_get_request()
{
    $CI = get_instance();
    if (!$CI->input->get()) {
        redirect(base_url());
    }
}

function view_menu()
{
    $CI = get_instance();
    // Data Header
    $CI->db->select('*');
    $CI->db->from('a_menu');
    $CI->db->where('menu_level', '0');
    $CI->db->order_by('menu_sortable', 'asc');
    $data = $CI->db->get()->result();
    // Data Level 1
    $CI->db->select('*');
    $CI->db->from('a_menu');
    $CI->db->where('menu_level', '1');
    $CI->db->order_by('menu_sortable', 'asc');
    $data1 = $CI->db->get()->result();
    // Data Level 2
    $CI->db->select('*');
    $CI->db->from('a_menu');
    $CI->db->where('menu_level', '2');
    $CI->db->order_by('menu_sortable', 'asc');
    $data2 = $CI->db->get()->result();
    // Data Level 3
    $CI->db->select('*');
    $CI->db->from('a_menu');
    $CI->db->where('menu_level', '3');
    $CI->db->order_by('menu_sortable', 'asc');
    $data3 = $CI->db->get()->result();

    return array(
        'data' => $data,
        'data1' => $data1,
        'data2' => $data2,
        'data3' => $data3,
    );
}

function get_outlet_name($id)
{
    $CI = get_instance();
    $where = array("id" => $id);
    $data = $CI->db->select("name")->get_where("a_outlet", $where)->row();
    return $data->name;
}

function check_parameter_menu_lain($id_menu)
{
    $CI = get_instance();
    $where = array("menu_id" => $id_menu);
    $data = $CI->db->select("menu_id,parameter,deskripsi")->get_where("a_akses_data", $where)->result();
    return $data;
}

function get_name_posisi($id)
{
    $CI = get_instance();
    $where = array("id" => $id);
    $data = $CI->db->select("posisi")->get_where("b_posisi", $where)->row();
    return $data->posisi;
}

function get_data_request($plu = null, $tujuan = null)
{
    $CI = get_instance();
    $CI->db->select('   a.no_surat, a.id_item,  CONCAT(b.date_request," ",b.time_request) AS date_request,  a.jumlah_request,   c.no_dept,  c.tanggal AS date_done, c.in_qty ');

    $CI->db->join('d_request b', 'a.no_surat = b.no_surat');
    $CI->db->join('d_item_stock_transfer c', 'c.keterangan LIKE CONCAT("%",b.no_surat,"%")');
    $CI->db->join('d_gudang d', 'b.tujuan = d.id_outlet');

    $CI->db->where('a.id_item', $plu);
    $CI->db->where('d.id', $tujuan);
    $CI->db->where('b.type', 1);
    $CI->db->where('c.in_qty !=', 0);
    $CI->db->where('c.plu = a.id_item');

    $CI->db->group_by('a.no_surat');
    $CI->db->group_by('a.id_item');
    $CI->db->order_by('b.date_request', 'desc');
    $CI->db->order_by('b.time_request', 'desc');
    $CI->db->limit(1);

    $data = $CI->db->get('d_request_item a')->row();

    return $data;
}

function get_name_dep_explode($MasterAbsenId)
{
    $CI = get_instance();
    $dep_master = $CI->db->select('dep_id')->get_where('c_master_absen_dep', array('master_absen_id' => $MasterAbsenId))->result_array();
    if ($dep_master) {
        $depMasAb = array();
        foreach ($dep_master as $value) {
            $depMas[] = $value['dep_id'];
        }
        $data_dep = $CI->db->select('name')->where_in('id', $depMas)->get('c_department')->result_array();
        $dep = array();
        foreach ($data_dep as $value) {
            $dep[] = $value['name'];
        }
        return implode(', ', $dep);
    } else {
        return '';
    }
}

function get_days_working($data_day)
{
    $days_id_array = explode(',', $data_day);
    $days = array();
    foreach ($days_id_array as $value) {
        $timestamp = strtotime('next Sunday');
        for ($i = 0; $i < 7; $i++) {
            if ($value == $i) {
                $days[] = strftime('%A', $timestamp);
            }
            $timestamp = strtotime('+1 day', $timestamp);
        }
    }
    $datadays = implode(", ", $days);

    return $datadays;
}

function get_count_absen($pin, $tanggalAwal, $tanggalAkhir)
{
    $CI = get_instance();
    /*Backup*/
    //$jumlahAbsen = $CI->db->get_where('b_finger_absen', array('pin' => $pin, 'tanggal>=' => $tanggalAwal, 'tanggal<=' => $tanggalAkhir, 'kode' => 'AB'))->num_rows();

    $CI->db->select('count(id) as jumlah');
    $CI->db->from('b_finger_absen');
    $CI->db->where('pin', $pin);
    $CI->db->where('tanggal >=', $tanggalAwal);
    $CI->db->where('tanggal <=', $tanggalAkhir);
    $CI->db->where("(kode = 'AB' OR kode = 'P')", NULL, FALSE);

    $jumlahAbsen = $CI->db->get()->row()->jumlah;

    return $jumlahAbsen;
}

function createPath($path, $mode)
{
    return is_dir($path) || mkdir($path, $mode, true);
}

function SendMessage_tele($id_telegram, $pesan, $parse_mode, $versi = '')
{
    $CI = get_instance();
    $CI->load->helper("telegram");
    if ($versi == '2') {
        $token = "villa_bot_$versi";
    } else {
        $token = "villa_bot";
    }
    $telegram   = new Telegram($CI->config->item($token));
    $content    = array(
        'chat_id'       => $id_telegram,
        'text'          => $pesan,
        'parse_mode'    => $parse_mode
    );
    return $telegram->sendMessage($content);
}

function SendDocument_tele($id_telegram, $caption, $file_path, $nama_file, $versi = '')
{
    $CI = get_instance();
    $CI->load->helper("telegram");
    if ($versi == '2') {
        $token = "villa_bot_$versi";
    } else {
        $token = "villa_bot";
    }
    $telegram   = new Telegram($CI->config->item($token));
    $content    = array(
        'chat_id'       => $id_telegram,
        'caption'       => $caption,
        'parse_mode'    => "markdown",
        'document'      => new CURLFile($file_path, "application/pdf", $nama_file)
    );
    return $telegram->sendDocument($content);
}

function quotes($stt = '')
{
    $CI = get_instance();
    $data = array();
    $CI->db->order_by('id_quotes', 'RANDOM');
    $CI->db->limit(30);
    $cek = $CI->db->get('a_quotes');
    foreach ($cek->result() as $key => $value) {
        if ($value->nama != '') {
            $nama = "#" . $value->nama;
        } else {
            $nama = '';
        }
        $isi = $value->isi_quotes . "” $nama";
        $data[] = "“$isi";
    }
    if ($stt == 1) {
        return $data;
    } else {
        return $data[array_rand($data)];
    }
}

function cek_status_vs_saat_ini()
{ $CI = get_instance();
  $name_host = explode(':',$CI->db->hostname)[0];
  $name_db   = $CI->db->database;
  $cek_detail_dbnya='1';
  if ($_SERVER['HTTP_HOST']=='192.168.9.2' || $_SERVER['HTTP_HOST']=='villacorp.systems') {
    $cek = ENVIRONMENT;
    $id_telenya='-1001280427070';
    if ($cek!='production') {
      SendMessage_tele($id_telenya, "STATUS VS saat ini <b>$cek</b>\nTolong diubah lagi ke <b>production</b>\nTerimakasih!", "html");
    }
    if ($name_host!='192.168.9.2' && $name_host!='localhost') {
      SendMessage_tele($id_telenya, "DATABASE VS saat ini\n<b>HOST: $name_host\nDB: $name_db</b>\nTolong diubah <b>SECEPATNYA</b>\nTerimakasih!", "html");
    }
    $cek_detail_dbnya='0';
  }
  return array('name_host'=>$name_host,'name_db'=>$name_db,'cek_detail_dbnya'=>$cek_detail_dbnya);
}

function css_watermark()
{?>
  <style>
  #bg-watermark{
      position:fixed; z-index:99999; background:rgba(0,0,0,0.8); display:block;
      padding:10px; padding-top:5px; margin-bottom: -15px; right:0; bottom:0; cursor: pointer;
  }
  #text-watermark{
      color:lightgrey; font-size:20px; cursor: pointer;
  }
  </style>
<?php
}

function watermarknya()
{ $v = cek_status_vs_saat_ini();?>
  <?php if ($v['cek_detail_dbnya'] == 1): ?>
    <div id="bg-watermark" onclick="bg_wm()" class="noselect">
      <p id="text-watermark">
        <i class="fa fa-desktop"></i>&nbsp;<?php echo $v['name_host']; ?>
        <span id="h-wm"><br /></span>
        <i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo $v['name_db']; ?>
      </p>
    </div>
    <script type="text/javascript">
      function setCookie(key, value, expiry) {
          var expires = new Date();
          expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
          document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
      }

      function getCookie(key) {
          var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
          return keyValue ? keyValue[2] : null;
      }

      function eraseCookie(key) {
          var keyValue = getCookie(key);
          setCookie(key, keyValue, '-1');
      }
      bg_wm(1);
      function bg_wm(aksi='')
      {
        bg = $('#bg-watermark');
        if (aksi=='1') {
          if (getCookie('watermark')==1) {
            $('#h-wm').html('<br />');
            $('#text-watermark').css('font-size',20);
          }else {
            $('#h-wm').html('&nbsp;');
            $('#text-watermark').css('font-size',14);
          }
        }else {
          if (getCookie('watermark')==0) {
            setCookie('watermark','1','1');
            bg.removeClass('min-wm');
            $('#h-wm').html('<br />');
            $('#text-watermark').css('font-size',20);
          }else {
            setCookie('watermark','0','1');
            bg.addClass('min-wm');
            $('#h-wm').html('&nbsp;');
            $('#text-watermark').css('font-size',14);
          }
        }
      }
    </script>
  <?php endif; ?>
<?php
}
