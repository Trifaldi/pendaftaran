<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skill_bahasa_lain extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
    $this->load->model('M_personal');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{

	$uniqe = $this->session->userdata('uniqe');
    $this->load->helper('url');
    $data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
    $data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
    $data ['subview'] = 'steep/skill_bahasa_lain';
  	$this->load->view('step',$data);


	}

	public function create()
	{	

		$uniqe = $this->session->userdata('uniqe');
				// Ambil data yang dikirim dari form
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$non_type = $_POST['non_type']; // Ambil data non_type dan masukkan ke variabel non_type
		$non_ket = $_POST['non_ket']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$non_pilta = $_POST['non_pilta'];
		$type_bhsasing = $_POST['type_bhsasing'];
		$point = $_POST['point'];
		$kategory = $_POST['kategori'];
		 // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();

		$x = 0;
		while($x<count($non_type)){

			array_push($data, array(
				'id_unix'=> $id_unix[$x],
				'non_type'=> $non_type[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'non_ket'=> $non_ket[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'non_pilta'=> $non_pilta[$x],
				'type_bhsasing'=> $type_bhsasing[$x],
				'point'=> $point[$x],
				'kategori'=> $kategory[$x],
			));
			$x++;
		}
		//log_r($data);
		$this->load->model('M_personal');
		$this->db->query(" UPDATE b_personal SET proses_status = 3 WHERE uniqe = '$uniqe' ");
		//if ($uniqe !== $id_unix) {
		$insert = $this->M_personal->save_batch_skill_lain($data);

		if ($insert == true ) {
			redirect('riwayat_pekerjaan');
		}
	}

	function update(){
		//$uniqe = $this->session->userdata('uniqe');
				// Ambil data yang dikirim dari form
		$kategori = $_POST['kategori'];
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$non_type = $_POST['non_type']; // Ambil data non_type dan masukkan ke variabel non_type
		$non_ket = $_POST['non_ket']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$non_pilta = $_POST['non_pilta'];
		$type_bhsasing = $_POST['type_bhsasing'];
		$point = $_POST['point'];
		$data = array();

		$x = 0;
		foreach($kategori as $dataid_kat){
			array_push($data, array(
				'kategori'=> $dataid_kat,
				'id_unix'=> $id_unix[$x],
				'non_type'=> $non_type[$x],
				'non_ket'=> $non_ket[$x],
				'non_pilta'=> $non_pilta[$x],
				'type_bhsasing'=> $type_bhsasing[$x],
				'point'=> $point[$x]
			));
			$x++;
		}

		//log_r($data);
		$this->db->where_in('id_unix',$id_unix);
		$update = $this->db->update_batch('p_nonformal', $data,'kategori');
		if ($update == true ) {
			redirect('riwayat_pekerjaan');
		} else {
			redirect('skill_bahasa_lain');
		}
	}

}
