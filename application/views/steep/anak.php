	<form class="responsive-height" action="javascript:void(0)" method="POST" id="form-user4">
		<div class="col-md-12">
			<div class="form-group pasangan">
				<label >Nama Anak</label>
				<input type="hidden" name="fam_kategori" value="3">
				<input type="hidden" id="fam_ket" name="fam_ket" value="Istri">
				<input type="hidden" id="fam_ket" name="fam_jekel" value="Wanita">
				<input class="form-control" type="text" id="fam_nama" name="fam_nama" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return harusHuruf('event')">
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label >No Hp</label>
				<input type="text" name="fam_no_hp" id="fam_no_hp" class="form-control" maxlength="16" onkeypress="return angka(event)">
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label >Usia</label>
				<input type="text" name="fam_usia" id="fam_usia" class="form-control" maxlength="3">
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label >Pendidikan</label>
				<select class="form-control" name="fam_pen" id="fam_pen">
					<option>Pilih..</option>
					<?php foreach ($pendidikan as $key => $value) { ?>
						<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
					<?php } ?>
				</select>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label >Job</label>
				<select class="form-control" onchange="myFunctionSaudara(event)" id="fam_job" name="fam_job">
					<option>Pilih..</option>
					<option value="Bekerja">Bekerja</option>
					<option value="Tidak Bekerja">Tidak Bekerja</option>
				</select>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
		<p id="suadara"></p>

		<p id="demo"></p>
