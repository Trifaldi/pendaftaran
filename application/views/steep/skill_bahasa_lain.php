<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
<?php if ( $personal->proses_status > 2 ) { ?>
	<form class="responsive-height" name="myForm" action="<?= base_url('skill_bahasa_lain/update'); ?>" method="POST" style="
    background: white;">
	<?php } else { ?>
		<form class="responsive-height" name="myForm" action="<?= base_url('skill_bahasa_lain/create'); ?>" method="POST"style="
    background: white;">
		<?php } ?>

		<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
			<div class="col-md-12">
				<div class="form-wizard-nav animated bounceInLeft">
					<div class="step active complete" data-form="#form-1"></div>
					<div class="step active complete" data-form="#form-2"></div>
					<div class="step active complete" data-form="#form-3"></div>
					<div class="step active complete" data-form="#form-4"></div>
					<div class="step active complete" data-form="#form-5"></div>
					<div class="step active complete" data-form="#form-6"></div>
					<div class="step active complete" data-form="#form-7"></div>
					<div class="step active complete" data-form="#form-8"></div>
					<div class="step active complete" data-form="#form-9"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
				</div>
			</div>

			<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

				<div class="row">
					<div class="col-md-12">
						<label class="form-group">Skill</label>
					</div>
					<div class="col-sm-6 jarak_kanan animated bounceInRight">
						<div class="col-md-12">
							<div class="form-group">
								<label >Bhs. Lainnya</label>
								<input class="form-control" type="text" placeholder=" Indonesia, mandarin">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label >Comunication</label>
								<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
								<input type="hidden" name="type_bhsasing[]" value="komunication">
								<input type="hidden" name="non_ket[]">
								<input type="hidden" name="kategori[]" value="7">
								<input type="hidden" name="non_pilta[]">
								<input class="form-control" type="hidden" name="non_type[]" value="bahasa lainnya" readonly>
								<select class="form-control" name="point[]" required>
									<option value="">Pilih...</option>
									<option value="Sangat Baik">Sangat Baik</option>
									<option value="Baik">Baik</option>
									<option value="Cukup">Cukup</option>
									<option value="Kurang">Kurang</option>
								</select>

								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
							<div class="form-group">
								<label >Read</label>
								<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
								<input type="hidden" name="type_bhsasing[]" value="read">
								<input type="hidden" name="non_ket[]">
								<input type="hidden" name="kategori[]" value="8">
								<input type="hidden" name="non_pilta[]">
								<input class="form-control" type="hidden" name="non_type[]" value="bahasa lainnya" readonly>
								<select class="form-control" name="point[]" required>
									<option value="">Pilih...</option>
									<option value="Sangat Baik">Sangat Baik</option>
									<option value="Baik">Baik</option>
									<option value="Cukup">Cukup</option>
									<option value="Kurang">Kurang</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
							<div class="form-group">
								<label >Write</label>
								<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
								<input type="hidden" name="type_bhsasing[]" value="Write">
								<input type="hidden" name="non_ket[]">
								<input type="hidden" name="kategori[]" value="9">
								<input type="hidden" name="non_pilta[]">
								<input class="form-control" type="hidden" name="non_type[]" value="bahasa lainnya" readonly>
								<select class="form-control" name="point[]" required>
									<option value="">Pilih...</option>
									<option value="Sangat Baik">Sangat Baik</option>
									<option value="Baik">Baik</option>
									<option value="Cukup">Cukup</option>
									<option value="Kurang">Kurang</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 jarak_kiri animated bounceInLeft">
						<div class="col-md-12">
							<div class="form-group">
								<label >Skill Komputer</label>
								<div class="help-block form-text text-muted form-control-feedback"></div>
								<input class="form-control" type="hidden" name="non_type[]" value="Skill Komputer" >
								<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
								<input type="hidden" name="type_bhsasing[]" value="">
								<input type="hidden" name="non_ket[]">
								<input type="hidden" name="kategori[]" value="10">
								<input type="hidden" name="non_pilta[]">
								<select class="form-control" name="point[]">
									<option value="">Pilih...</option>
									<option value="Sangat Baik">Sangat Baik</option>
									<option value="Baik">Baik</option>
									<option value="Cukup">Cukup</option>
									<option value="Kurang">Kurang</option>
								</select>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label >Skill Lainnya</label>
								<div class="help-block form-text text-muted form-control-feedback"></div>
								<input class="form-control" type="text" name="non_type[]">
								<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
								<input type="hidden" name="type_bhsasing[]" value="">
								<input type="hidden" name="non_ket[]">
								<input type="hidden" name="kategori[]" value="11">
								<input type="hidden" name="non_pilta[]">
							</div>
						</div>
						<div class="col-md-12">
							<select class="form-control" name="point[]" required>
								<option value="">Pilih...</option>
								<option value="Sangat Baik">Sangat Baik</option>
								<option value="Baik">Baik</option>
								<option value="Cukup">Cukup</option>
								<option value="Kurang">Kurang</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content-box" style="margin-top: 134px;">
			<a href="<?php echo base_url(); ?>skill_bahasa" class="btn btn-warning">Back</a>
			<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
		</div>
	</form>
