<!DOCTYPE html>
<html class="app">
<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<meta content="template language" name="keywords">
	<meta content="John Doe" name="author">
	<meta content="Admin Template" name="description">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<link href="<?= base_url();?>employye.png" rel="shortcut icon">
	<link href="<?= base_url();?>apple-touch-icon.png" rel="apple-touch-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/bootstrap/dist/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/batch-icons/style.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/dashicons/style.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/dripicons/webfont.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/eightyshades/style.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/foundation-icons/foundation-icons.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/metrize-icons/style.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/simple-line-icons/css/simple-line-icons.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/themify-icons/themify-icons.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/type-icons/style.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/weather-icons/css/weather-icons.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/animate/animate.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/fullcalendar/dist/fullcalendar.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/datatable/media/css/jquery.dataTables.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/datatable/media/css/dataTables.bootstrap4.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/dropzone/dist/dropzone.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/select2/dist/css/select2.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/bootstrap-daterangepicker/daterangepicker.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/fancybox/dist/jquery.fancybox.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/sweetalert/dist/sweetalert.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/exort/uploader.min.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/plugins/jquery-treegrid/jquery.treegrid.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/css/main.css"/>
	<link rel="stylesheet" href="<?= base_url();?>template/css/custom.css"/>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/jquery/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/jquery-count-to/jquery.countTo.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/moment/min/moment.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/fullcalendar/dist/fullcalendar.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/chart.js/dist/Chart.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/datatable/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/datatable/media/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/dropzone/dist/dropzone.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/select2/dist/js/select2.full.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/bootstrap-validator/dist/validator.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/fancybox/dist/jquery.fancybox.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/disabler-enabler/disabler.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/disabler-enabler/enabler.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/super-datagrid/datagrid.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/exort/uploader.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/jquery-treegrid/jquery.treegrid.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/tether/dist/js/tether.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= base_url();?>template/js/app.js"></script>
</head>
<body>
	<div class="modal fade animated bounceInCenter" id="myModal" role="dialog">
		<div class="modal-dialog modal-lg" style="top: 120px;">
			<div class="modal-content animated bounceInLeft">
				<form method="POST" action="<?php echo base_url();?>siapa_anda/save" >
					<div class="modal-header">
						
						<h4 class="modal-title">
							<p style="">BACALAH PERNYATAAN IN DENGAN TELITI</p>
							<p style="color: red;"><i>PLEASE READ THIS STATEMENT CAREFULY</i></p>
						</h4>
					</div>
					<div class="modal-body">
						<p>Saya menyatakan bahwa informasi yang saya berikan pada formulir ini benar, lengkap dan akurat. Saya mengerti jika terdapat pemalsuan atau pengurangan informasi dapat mengakibatkan pemutusan hubungan kerja. Saya memberi wewenang kepada perusahaan untuk menyelidiki tingkah laku, reputasi dan latar belakang pekerjaan, jika diperlukan</p>
						<br>
						<br>
						<p>I hereby affirm that the information given by me on this application for employment is true, complete, and accurate. I understand that Any falsification or omission will be considered sufficient cause for dismissal. I authorize a though investigation to be made in connection with this application concerning my character, general reputation, employment and educational background.
						Whichever may be applicable.</p>
						<br>
						<p class="pull right" ><input type="checkbox" id="myCheckbox" class="form-control" value="Setuju"  name="26penyataan"></p>
					</div>
					
					<div class="modal-footer" id="buttonnext">
						
					</div>
				</form>
			</div>
		</div>
	</div>
	<style>
		#bg-watermark{
			position:fixed; z-index:99999; background:rgba(0,0,0,0.8); display:block;
			padding:10px; padding-top:4px; padding-bottom:4px; margin-bottom: 43%; right:0; top:0; cursor: pointer;
			margin-top:10px; padding:10px;
		}
		#text-watermark{
			color:black; font-size:20px; cursor: pointer;
		}
	</style>
	<style type="text/css">
		.select2-container--default .select2-selection--single {
			background-color: #fff;
			border: 1px solid #0000ff;
			border-top: none;
			border-left: none;
			border-right: none;
			border-radius: 0px;
		}
		.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
			background-color: #0275d8;
		}
		.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
		label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
		.form-group{margin-bottom:30px; margin-top: 15px;}
		::-webkit-input-placeholder { /* Edge */
			color: #a2a5a7;
		}
		:-ms-input-placeholder { /* Internet Explorer 10-11 */
			color: #a2a5a7;
		}
		::placeholder {
			color: #a2a5a7 !important;
		}
		.responsive-height{ min-height: 480px; margin-bottom: -10px;}
		@media screen and (max-width: 700px)
		{
			.responsive-height{ min-height: 400px; }
		}
		@media screen and (max-width: 800px)
		{
			.responsive-height{ min-height: 500px; }
		}
		.jarak_kiri{ padding-left:40px; }
		.jarak_kanan{ padding-right:40px; }
		@media screen and (max-width: 575px)
		{
			.jarak_kiri{ padding-left:0px;padding-right:0px; }
			.jarak_kanan{ padding-left:0px;padding-right:0px; }
		}
	</style>

	<div id="bg-watermark" onclick="bg_wm()" class="animated bounceInLeft" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;background: #5bc0de;">
		<p id="text-watermark" style="font-size: 20px;">
			<i class="fa fa-user" style="font-size: 20px;"></i>&nbsp;<span id="h-wm"><br></span>
		</div>
		<div class="wrapper" style="margin-top: 0px;" onclick="bg_wn()">
			<div class="main">
				<style type="text/css">
					.select2-container--default .select2-selection--single {
						background-color: #fff;
						border: 1px solid #0000ff;
						border-top: none;
						border-left: none;
						border-right: none;
						border-radius: 0px;
					}
					.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
						background-color: #0275d8;
					}
					.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
					label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
					.form-group{margin-bottom:30px; margin-top: 15px;}
					::-webkit-input-placeholder { /* Edge */
						color: #a2a5a7;
					}
					:-ms-input-placeholder { /* Internet Explorer 10-11 */
						color: #a2a5a7;
					}
					::placeholder {
						color: #a2a5a7 !important;
					}
					.responsive-height{ min-height: 480px; margin-bottom: -10px;}
					@media screen and (max-width: 700px)
					{
						.responsive-height{ min-height: 400px; }
					}
					@media screen and (max-width: 800px)
					{
						.responsive-height{ min-height: 500px; }
					}
					.jarak_kiri{ padding-left:40px; }
					.jarak_kanan{ padding-right:40px; }
					@media screen and (max-width: 575px)
					{
						.jarak_kiri{ padding-left:0px;padding-right:0px; }
						.jarak_kanan{ padding-left:0px;padding-right:0px; }
					}
				</style>
				<div class="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
					<?php $this->load->view($subview);?>
				</div>
			</div>
		</div>
		<form>
			<div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<strong>Are you sure to delete this record?</strong>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="id_fam" id="id_fam" class="form-control">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
							<button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript">
			function setCookie(key, value, expiry) {
				var expires = new Date();
				expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
				document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
			}
			function getCookie(key) {
				var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
				return keyValue ? keyValue[2] : null;
			}
			function eraseCookie(key) {
				var keyValue = getCookie(key);
				setCookie(key, keyValue, '-1');
			}
			bg_wm(1);
			function bg_wm(aksi='')
			{
				bg = $('#bg-watermark');
				if (aksi=='1') {
					if (getCookie('watermark')==1) {
						$('#h-wm').html('<br/><i class="fa fa-phone">&nbsp;&nbsp;</i></p>');
						$('#text-watermark').css('font-size',20);
					}else {
						$('#h-wm').html('&nbsp;');
						$('#text-watermark').css('font-size',14);
					}
				}else {
					if (getCookie('watermark')==0) {
						setCookie('watermark','1','1');
						bg.removeClass('min-wm');
						$('#h-wm').html('<?php echo $this->session->userdata('fullname')?><br/><i class="fa fa-phone">&nbsp;&nbsp;</i><?php echo $this->session->userdata('hp_1')?></p>');
						$('#text-watermark').css('font-size',20);
					}else {
						setCookie('watermark','0','1');
						bg.addClass('min-wm');
						$('#h-wm').html('&nbsp;');
						$('#text-watermark').css('font-size',14);
					}
				}
			}
			bg_wn(0);
			function bg_wn(aksi='')
			{
				setCookie('watermark','0','1');
				bg.addClass('min-wm');
				$('#h-wm').html('&nbsp;');
				$('#text-watermark').css('font-size',14);
			}
		</script>
		<script type="text/javascript">
			const checkbox = document.getElementById('myCheckbox')
			checkbox.addEventListener('change', (event) => {
				if (event.target.checked) {
					document.getElementById("buttonnext").innerHTML=
					'<button type="submit" class="btn btn-success">Next</button>';
				} else {
					alert('Anda Yakin');
					document.getElementById("buttonnext").innerHTML=
					'';
				}
			})
		</script>
	</body>
	</html>