<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_personal extends CI_Model {


	public function __construct()
	{
		parent::__construct();

	}

    function simpan($data){
        return $this->db->insert('b_personal', $data);
    }
    public function update($data, $uniqe){
        $query = $this->db->update('b_personal', $data, array('uniqe'=>$uniqe));
        if($query){return true;}else{return false;
        }
    }
    public function save_batch($data){
        return $this->db->insert_batch('p_familiy', $data);
    }

    public function update_batch($id_unix,$fam_ket,$data){
        $data = array('id_unix' => $id_unix, 'fam_ket' => $fam_ket );
        return $this->db->update_batch('p_familiy', $data);
    }
    function hapus($table,$where){
        return $this->db->delete($table,$where);
    }

    function get_personal_by_kode($uniq){
        $hsl=$this->db->query("SELECT * FROM b_personal WHERE uniqe='$uniq'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'fullname' => $data->fullname,
                    'no_ktp' => $data->no_ktp,
                    'place_birth' => $data->place_birth,
                    'date_birth' => $data->date_birth,
                    'jenis_kelamin' => $data->jenis_kelamin,
                    'address' => $data->address,
                    'hp_1' => $data->hp_1,
                    'hp_2' => $data->hp_2,
                    'religion' => $data->religion,
                    'marital' => $data->marital,
                    'hobby' => $data->hobby,
                    'kacamata' => $data->kacamata,
                );
            }
        }
        return $hasil;
    }

    function check_uniqe($uniqe){
     $this->db->select('uniqe');
     $this->db->where('uniqe',$uniqe);
     $query =$this->db->get('b_personal');
     $row = $query->row();
     if ($query->num_rows > 0){
       return $row->uniqe;
   }else{
       return "";
   }
}

    public function get_saudara(){
         $this->load->library('session');
        $id_unix = $this->session->userdata('uniqe');
        $this->db->where('fam_kategori', 4);
        $this->db->where('id_unix', $id_unix);
        $query = $this->db->get('p_familiy');

        return $query->result();

    }

    public function save_saudara(){

        $id_unix = $this->session->userdata('uniqe');
        $id_fam = $this->input->post('id_fam');
        $this->db->where('id_fam',$id_fam);
        $id = $this->db->get('p_familiy');
        $data = array(
                'id_unix' => $this->session->userdata('uniqe'),
                'id_fam' => $this->input->post('id_fam'),
                'fam_ket' => $this->input->post('fam_ket'),
                'fam_nama' => $this->input->post('fam_nama'),
                'fam_jekel' => $this->input->post('fam_jekel'),
                'fam_no_hp' => $this->input->post('fam_no_hp'),
				'fam_usia' => $this->input->post('fam_usia'),
                'fam_pen' => $this->input->post('fam_pen'),
                'fam_job' => $this->input->post('fam_job'),
                'fam_per' => $this->input->post('fam_per'),
                'fam_kategori' => $this->input->post('fam_kategori'),
        );

        if ($id->num_rows() > 0 ) {
            $result=$this->db->update('p_familiy',$data, ['id_fam' => $id_fam]);
        }else{
            $result=$this->db->insert('p_familiy',$data);
        }
        
        return $result;
        //log_r($id_fam);
    }

    function delete_product(){
        $id_fam=$this->input->post('id_fam');
        $this->db->where('id_fam', $id_fam);
        $result=$this->db->delete('p_familiy');
        return $result;
    }

		public function get_keluarga(){
         $this->load->library('session');
        $id_unix = $this->session->userdata('uniqe');
        $this->db->where('fam_kategori', 3);
        $this->db->where('id_unix', $id_unix);
        $query = $this->db->get('p_familiy');

        return $query->result();

    }

    public function save_keluarga(){
        $data = array(
                'id_unix' => $this->session->userdata('uniqe'),
                'fam_ket' => $this->input->post('fam_ket'),
                'fam_nama' => $this->input->post('fam_nama'),
                'fam_jekel' => $this->input->post('fam_jekel'),
                'fam_no_hp' => $this->input->post('fam_no_hp'),
								'fam_usia' => $this->input->post('fam_usia'),
                'fam_pen' => $this->input->post('fam_pen'),
                'fam_job' => $this->input->post('fam_job'),
                'fam_per' => $this->input->post('fam_per'),
                'fam_kategori' => $this->input->post('fam_kategori'),
        );
        $result=$this->db->insert('p_familiy',$data);
        return $result;
    }

		public function get_emergency(){
         $this->load->library('session');
        $id_unix = $this->session->userdata('uniqe');
        $this->db->where('fam_kategori', 5);
        $this->db->where('id_unix', $id_unix);
        $query = $this->db->get('p_familiy');

        return $query->result();

    }

    public function save_emergency(){
        $data = array(
                'id_unix' => $this->session->userdata('uniqe'),
                'fam_ket' => $this->input->post('fam_ket'),
                'fam_nama' => $this->input->post('fam_nama'),
                'fam_jekel' => $this->input->post('fam_jekel'),
                'fam_no_hp' => $this->input->post('fam_no_hp'),
								'fam_usia' => $this->input->post('fam_usia'),
                'fam_pen' => $this->input->post('fam_pen'),
                'fam_job' => $this->input->post('fam_job'),
                'fam_per' => $this->input->post('fam_per'),
                'fam_kategori' => $this->input->post('fam_kategori'),
        );
        $result=$this->db->insert('p_familiy',$data);
        return $result;
    }
    public function save_batch_skill($data){
        return $this->db->insert_batch('p_nonformal', $data);
    }
        public function save_batch_skill_lain($data){
        return $this->db->insert_batch('p_nonformal', $data);
    }

}
