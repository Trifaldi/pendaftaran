<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<form class="responsive-height" style="background: white;" name="myForm" action="javascript.void" method="POST" id="form-insert" >
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav animated bounceInLeft">
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
			</div>
		</div>

		<div class="col-md-12 animated bounceInRight">
			<div class="form-group">
				<label >Secara singkat, siapakah Anda ?</label>
				<div class="form-group ">
					<select type="text" class="form-control" name="23siapaanda" required>
						<option value="">Pilih..</option>
						<option value="Pengambil keputusan (decision maker)">Pengambil keputusan (decision maker)</option>
						<option value="Penyelesai/pemecah masalah (problem solver)">Penyelesai/pemecah masalah (problem solver)</option>
						<option value="Penganalisa masalah (analyst)">Penganalisa masalah (analyst)</option>
					</select>
				</div>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
			<div class="form-group">
				<label >Apakah Anda pernah terlibat dalam tindakan kriminal atau dijatuhi hukuman oleh pengadilan karena tindakan kriminal ?</label>
				<div class="form-group ">
					<select type="text" class="form-control" name="24kriminal" required>
						<option value="">Pilih..</option>
						<option value="YA">YA</option>
						<option value="TIDAK">TIDAK</option>
					</select>
				</div>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	</div>
	<div class="content-box" style="margin-top: 275px;">
		<a href="<?php echo base_url(); ?>gagal" class="btn btn-warning">Back</a>
		<button type="button" class="btn btn-success pull-right btn_save" data-toggle="modal" data-target="#myModal">Next</button>
	</div>
</form>	
<script type="text/javascript">
	$(document).ready(function(){

		$('.btn_save').click(function(){
			var data = $('#form-insert').serialize();

			$.ajax({
				url: '<?php echo base_url('siapa_anda/create');?>',
				type: 'POST',
				async: false,
				dataType: 'JSON',
				data: data,
				success: function(data){
					$('#myModal').modal('show');
				}
			});
			return false;
		});
	});
</script>

