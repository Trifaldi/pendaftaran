<script type="text/javascript" src="<?= base_url();?>assets/datetimepicker/bootstrap-datetimepicker.js"></script>
<link rel="stylesheet" href="<?= base_url();?>assets/datetimepicker/bootstrap-datetimepicker.css"/>
<style type="text/css"></style>
<div class="content-box" class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
	<div class="col-md-12">
		<div class="form-wizard-nav animated bounceInLeft">
			<div class="step" data-form="#form-1"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
		</div>
	</div>

	<form class="responsive-height" name="myForm" action="<?= base_url('profil/create'); ?>" method="POST" id="form-user1">
		<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

			<div class="row">
				<div class="col-sm-6 jarak_kanan animated bounceInRight">
					<div class="col-md-12">
						<div class="form-group">
							<label for="yourname">Nama Lengkap</label>
							<input class="form-control" id="nama" type="text" name="fullname" value="<?php echo $personal->fullname ?>" onkeyup="this.value = this.value.toUpperCase()"  onkeypress="return harusHuruf(event)" >
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label >No_KTP</label>
							<input class="form-control" type="text" name="no_ktp" id="no_ktp" minlength="19" maxlength="19"  value="<?php echo $personal->no_ktp ?>" onkeypress="return angka(event)" pattern=".{0}|.{19,19}" required >
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label style="margin-bottom: 0px">Tempat Lahir</label>
							<select id="placeholder" class="form-control select2 required" name="place_birth" required>
								<option selected disabled ></option>
								<?php foreach($kota as $cat){
									$id         = $cat->id;
									$distrik         = $cat->distrik;
									$kota         = $cat->kota;?>
									<option <?php if($id == $personal->place_birth){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $kota;?>&nbsp; - &nbsp;<?php echo $distrik;?></option>
								<?php } ?>
							</select>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label >Tanggal Lahir</label>
							<input class="form-control" type="text" name="date_birth" placeholder="tgl/bln/tahun" id="datepicker" value="<?php echo $personal->date_birth ?>" required>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="row">
								<div class="col-sm-6" style="top: 10px">
									<div class="form-group">
										<input <?php $jenis_kelamin = 'PRIA'; if($jenis_kelamin==$personal->jenis_kelamin){ echo 'checked=""'; } ?> name="jenis_kelamin" type="radio" value="PRIA" required> PRIA
									</div>
								</div>
								<div class="col-sm-6" style="top: 10px">
									<div class="form-group">
										<input <?php $jenis_kelamin = 'WANITA'; if($jenis_kelamin==$personal->jenis_kelamin){ echo 'checked=""'; } ?> name="jenis_kelamin" type="radio" value=" WANITA" required> WANITA
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label >Alamat</label>
							<input class="form-control" type="text" name="address"onkeyup="this.value = this.value.toUpperCase()" value="<?php echo $personal->address ?>" required>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<script>
											//  Bind the event handler to the "submit" JavaScript event
											$('.yourname').click(function(){
											// Get the Login Name value and trim it
											var name = $('#yourname').val();
											// Check if empty of not
											if (name.length < 1) {
												alert('Text-field is empty.');
												return false;
											}
										});
									</script>
									<script>
										function harusHuruf(evt){
											var charCode = (evt.which) ? evt.which : event.keyCode
											if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
												return false;
											return true;
										}
										function angka(evt){
											var charCode = (evt.which) ? evt.which : event.keyCode
											if (charCode <33 || charCode > 57) {
												return false;
											}
											return true;
										}
									</script>
									<!-- <script src="http://localhost/calon_karyawan/assets/bootstrap/js/bootstrap.min.js"></script> -->
									<script type="text/javascript">
										$(document).ready(function() {
											$('#datepicker').datetimepicker({
												format: 'YYYY-MM-DD',
												minDate: '1565-01-01',
												maxDate: '2004-01-01',


											})<?php if ($personal->date_birth == null) {
												echo ".val('')";
											} else {

											} ?>
										});
									</script>
									<script type="text/javascript">
										$('#no_ktp').click(function(event) {
											var val = $('#no_ktp').val();
											if(val==''){
												$('#no_ktp').val();
											}

										});
										$('#no_ktp').focus(function(event) {
											$('#no_ktp').val();
										});
										$("#no_ktp").focusout(function(){
											$('#no_ktp').val();
										});
										$("#no_ktp").keyup(function(){
											var val = $('#no_ktp').val();
											//var valx = '085834246342';
											var valx = val.replace(/-/gi, "");
											console.log(valx);
											arr = valx.match(/.{1,4}/g);
											//bersihkan array jika ada yg undefined
											var i = 0;
											var string = "";
											if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
												string = arr[0];
											}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
												string = arr[0]+"-"+arr[1];
											}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
												string = arr[0]+"-"+arr[1]+"-"+arr[2];
											}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
												string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
											}

											//string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
											console.log(string);
											$('#no_ktp').val('');
											$('#no_ktp').val(string);
										});
									</script>
									<script type="text/javascript">
										$("#placeholder").select2({
											placeholder: "Pilih..",
											allowClear: true
										});
									</script>				<!-- </div> -->
								</div>
								<div class="col-sm-6 jarak_kiri animated bounceInLeft">
									<div class="col-md-12">
										<div class="form-group">
											<label>No Handpone</label>
											<input class="form-control"  type="text" name="hp_1" value="<?php echo $personal->hp_1; ?>" readonly>
											<div class="help-block form-text text-muted form-control-feedback"></div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>No Handpone II</label>
											<input class="form-control" pattern=".{0}|.{12,16}"  type="text" name="hp_2" placeholder="Input No Handpone II" id="no_hp" placeholder="08" maxlength="16"onkeypress="return angka(event)" value="<?php echo $personal->hp_2; ?>">
											<div class="help-block form-text text-muted form-control-feedback"></div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Agama</label>
											<select placeholder="Pilih" class="form-control" name="religion" required>
												<option></option>
												<option <?php $agama = 'ISLAM'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="ISLAM">ISLAM</option>
												<option <?php $agama = 'KRISTEN'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KRISTEN">KRISTEN</option>
												<option <?php $agama = 'KATOLIK'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KATOLIK">KATOLIK</option>
												<option <?php $agama = 'BUDHA'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="BUDHA">BUDHA</option>
												<option <?php $agama = 'KHONG HUCU'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KHONG HUCU">KHONG HUCU</option>
											</select>
											<div class="help-block form-text text-muted form-control-feedback"></div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Status Pernikahan</label>
											<select class="form-control" id="martial" name="marital" required>
												<option></option>
												<?php foreach($martial as $cat){
													$id         = $cat->id;
													$title      = $cat->keterangan;?>
													<option <?php if($id == $personal->marital){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $title; ?></option>
												<?php } ?>
											</select>
											<div class="help-block form-text text-muted form-control-feedback"></div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Anda berkacamata </label>
											<div class="row">
												<div class="col-sm-6" style="top: 10px">
													<div class="form-group">
														<input <?php $mata = 'YA'; if($mata==$personal->kacamata){ echo 'checked=""'; } ?>  name="kacamata" type="radio" value="YA" required> YA
													</div>
												</div>
												<div class="col-sm-6" style="top: 10px">
													<div class="form-group">
														<input <?php $mata = 'TIDAK'; if($mata==$personal->kacamata){ echo 'checked=""'; } ?> name="kacamata" type="radio" value="TIDAK" required> TIDAK
													</div>
												</div>
											</div>
											<div class="help-block form-text text-muted form-control-feedback"></div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Hobby</label>
											<input class="form-control" type="text" name="hobby" placeholder="Input Hobby" onkeyup="this.value = this.value.toUpperCase()" value="<?php echo $personal->hobby ?>" required>
											<div class="help-block form-text text-muted form-control-feedback"></div>
										</div>
									</div>

									<script type="text/javascript">
										$('#no_hp').click(function(event) {
											var val = $('#no_hp').val();
											if(val==''){
												$('#no_hp').val('08');
											}
										});
										$('#no_hp').focus(function(event) {
											$('#no_hp').val();
										});
										$("#no_hp").focusout(function(){
											$('#no_hp').val();
										});
										$("#no_hp").keyup(function(){
											var val = $('#no_hp').val();
											//var valx = '085834246342';
											var valx = val.replace(/-/gi, "");
											//console.log(valx);
											arr = valx.match(/.{1,4}/g);
											//bersihkan array jika ada yg undefined
											var i = 0;
											var string = "";
											if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
												string = arr[0];
											}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
												string = arr[0]+"-"+arr[1];
											}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
												string = arr[0]+"-"+arr[1]+"-"+arr[2];
											}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
												string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
											}

											//string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
											//console.log(string);
											$('#no_hp').val('');
											$('#no_hp').val(string);
											if(val=='08'){
												$('#no_hp').val('08');
											}else if(val=='0'){
												$('#no_hp').val('08');
											}else if(val==''){
												$('#no_hp').val('08');
											}
										});
									</script>					<!-- </div> -->
								</div>
							</div>


						</div>
							<div class="content-box-footer">
								<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
							</div>
						
					</form>

					<script type="text/javascript">
						$('#no_hp').click(function(event) {
							var val = $('#no_hp').val();
							if(val==''){
								$('#no_hp').val('08');
							}
						});
						$('#no_hp').focus(function(event) {
							$('#no_hp').val();
						});
						$("#no_hp").focusout(function(){
							$('#no_hp').val();
						});
						$("#no_hp").keyup(function(){
							var val = $('#no_hp').val();
						//var valx = '085834246342';
						var valx = val.replace(/-/gi, "");
						console.log(valx);
						arr = valx.match(/.{1,4}/g);
						//bersihkan array jika ada yg undefined
						var i = 0;
						var string = "";
						if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
							string = arr[0];
						}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
							string = arr[0]+"-"+arr[1];
						}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
							string = arr[0]+"-"+arr[1]+"-"+arr[2];
						}else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
							string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
						}

						//string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
						console.log(string);
						$('#no_hp').val('');
						$('#no_hp').val(string);
						if(val=='08'){
							$('#no_hp').val('08');
						}else if(val=='0'){
							$('#no_hp').val('08');
						}else if(val==''){
							$('#no_hp').val('08');
						}
					});
				</script>
