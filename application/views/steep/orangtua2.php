<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
	<?php if ($personal->proses_status > 0 ) { ?>
		<form class="responsive-height" name="myForm" action="<?= base_url('orangtua/update'); ?>" method="POST" >
		<?php }else { ?>
			<form class="responsive-height" name="myForm" action="<?= base_url('orangtua/create'); ?>" method="POST" >
			<?php } ?>

			<div class="col-md-12">
				<div class="form-wizard-nav animated bounceInLeft">
					<div class="step active complete" data-form="#form-1"></div>
					<div class="step active complete" data-form="#form-2"></div>
					<div class="step active complete"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
					<div class="step"></div>
				</div>
			</div>

			<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

				<div class="row">
					<div class="col-sm-6 jarak_kanan animated bounceInRight">
						<div class="col-md-12">
							<div class="form-group">
								<label >Nama Ayah</label>
								<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe');?>">
								<input type="hidden" name="fam_ket[]" value="Ayah" >
								<input type="hidden" name="fam_kategori[]" value="1" >
								<input type="hidden" name="fam_jekel[]" value="Pria" >
								<input class="form-control" type="text" value="<?php echo $ayah->fam_nama ?>" name="fam_nama[]" onkeypress="return harusHuruf(event)" required="required" onkeyup="this.value = this.value.toUpperCase()">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Status</label>
								<select name="fam_sttus[]" class="form-control" id="mySelect" onchange="myFunction(event)" >
									<option value="">Pilih</option>
									<option <?php $status='Masih Hidup';if($status==$ayah->fam_sttus){echo 'selected="selected"';} ?> value="Masih Hidup">Masih Hidup</option>
									<option <?php $status='Almarhum';if($status==$ayah->fam_sttus){echo 'selected="selected"';} ?> value="Almarhum">Almarhum</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<?php if ($ayah->fam_sttus == 'Masih Hidup') { ?>
							<div class="col-md-12">
								<div class="form-group">
									<label >No Hape Ayah</label>
									<input class="form-control" value="<?php echo $ayah->fam_no_hp?>" pattern=".{0}|.{12,16}" onclick="myNumber()" id="hp_ayah" type="text" onkeypress="return angka(event)" name="fam_no_hp[]" placeholder="No Hp Ayah" data-mask="0000-0000-0000-0">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Usia</label>
									<input class="form-control" value="<?php echo $ayah->fam_usia?>" type="text" name="fam_usia[]" maxlength="3" onkeypress="return angka(event)" required="required">
									<div class="help-block form-text text-muted form-control-feedback">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Pendidikan</label>
									<select name="fam_pen[]" class="form-control" >
										<option value="">Pilih..</option>
										<?php foreach ($pendidikan as $key => $value) { ?>
											<option <?php if($value->id_edu == $ayah->fam_pen){ echo 'selected="selected"'; } ?> value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
										<?php } ?>
									</select>
									<div class="help-block form-text text-muted form-control-feedback">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Pekerjaan</label>
									<select name="fam_job[]" class="form-control" >
										<option value="">Pilih..</option>
										<option <?php $pekerjaan='Bekerja';if($pekerjaan==$ayah->fam_job){echo 'selected="selected"';} ?> value="Bekerja">Bekerja</option>
										<option <?php $pekerjaan='Tidak Bekerja';if($pekerjaan==$ayah->fam_job){echo 'selected="selected"';} ?> value="Tidak Bekerja">Tidak Bekerja</option>
									</select>
									<div class="help-block form-text text-muted form-control-feedback">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Nama Perusahaan</label>
									<input class="form-control" value="<?php echo $ayah->fam_per;?>" type="text" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
						<?php }else { ?>
							<div id="demo"></div>
						<?php } ?>
					</div>
					<div class="col-sm-6 jarak_kiri animated bounceInLeft">
						<div class="col-md-12">
							<div class="form-group">
								<label >Nama Ibu</label>
								<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe');?>">
								<input type="hidden" name="fam_ket[]" value="Ibu">
								<input type="hidden" name="fam_kategori[]" value="2">
								<input type="hidden" name="fam_jekel[]" value="Wanita">
								<input class="form-control" type="text" value="<?php echo $ibu->fam_nama ?>" name="fam_nama[]" onkeypress="return harusHuruf(event)" onkeyup="this.value = this.value.toUpperCase()" required="required">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Status</label>
								<select name="fam_sttus[]" class="form-control" id="mySelect3" onchange="myFunction3(event)" required="required">
									<option value="">Pilih..</option>
									<option <?php $status='Masih Hidup';if($status==$ibu->fam_sttus){echo 'selected="selected"';} ?> value="Masih Hidup">Masih Hidup</option>
									<option <?php $status='Almarhum';if($status==$ibu->fam_sttus){echo 'selected="selected"';} ?> value="Almarhum">Almarhum</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<?php if ($ibu->fam_sttus == 'Masih Hidup') { ?>
							<div class="col-md-12">
								<div class="form-group">
									<label >No Hape Ibu</label>
									<input class="form-control" value="<?php echo $ibu->fam_no_hp?>"  pattern=".{0}|.{12,16}" maxlength="16" onclick="myNumberIbu()" id="hp_ibu" type="text" name="fam_no_hp[]" placeholder="No Hape Ibu" data-mask="0000-0000-0000-0">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Usia</label>
									<input class="form-control" type="text" value="<?php echo $ibu->fam_usia?>" maxlength="3" name="fam_usia[]" onkeypress="return angka(event)" >
									<div class="help-block form-text text-muted form-control-feedback">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Pendidikan</label>
									<select name="fam_pen[]" class="form-control">
										<option value="">Pilih..</option>
										<?php foreach ($pendidikan as $key => $value) { ?>
											<option <?php if($value->id_edu == $ibu->fam_pen){ echo 'selected="selected"'; } ?> value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
										<?php } ?>
									</select>
									<div class="help-block form-text text-muted form-control-feedback">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Pekerjaan</label>
									<select name="fam_job[]" class="form-control" >
										<option value="">Pilih..</option>
										<option <?php $pekerjaan='Bekerja';if($pekerjaan==$ibu->fam_job){echo 'selected="selected"';} ?> value="Bekerja">Bekerja</option>
										<option <?php $pekerjaan='Tidak Bekerja';if($pekerjaan==$ibu->fam_job){echo 'selected="selected"';} ?> value="Tidak Bekerja">Tidak Bekerja</option>
									</select>
									<div class="help-block form-text text-muted form-control-feedback">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Nama Perusahaan</label>
									<input class="form-control" type="text" value="<?php echo $ibu->fam_per;?>" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
						<?php }else { ?>
							<p id="demo3"></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class="content">
			<a href="<?php echo base_url(); ?>sosmed" class="btn btn-warning">Back</a>
			<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
		</div>
	</form>
	<script type="text/javascript">
		function myFunction(e) {
			var x = document.getElementById("mySelect").value;
			if (x == 'Masih Hidup') {
				document.getElementById("demo").innerHTML='<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >No Hape Ayah</label>'+
				'<input class="form-control" pattern=".{0}|.{12,16}" maxlength="16"onclick="myNumber()" id="hp_ayah" type="text" onkeypress="return angka(event)" name="fam_no_hp[]" placeholder="No Hp Ayah" data-mask="0000-0000-0000-0" required="required">'+
				'<div class="help-block form-text text-muted form-control-feedback"></div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Usia</label>'+
				'<input class="form-control" type="text" name="fam_usia[]" maxlength="3" onkeypress="return angka(event)" required="required">'+
				'<div class="help-block form-text text-muted form-control-feedback">'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Pendidikan</label>'+
				'<select name="fam_pen[]" class="form-control" required="required">'+
				'<option value="">Pilih..</option>'+
				'<?php foreach ($pendidikan as $key => $value) { ?>'+
				'<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>'+
				'<?php } ?>'+
				'</select>'+
				'<div class="help-block form-text text-muted form-control-feedback">'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Pekerjaan</label>'+
				'<select name="fam_job[]" class="form-control" id="mySelect2" onchange="myFunction2(event)" required="required">'+
				'<option value="">Pilih..</option>'+
				'<option value="Bekerja">Bekerja</option>'+
				'<option value="Tidak Bekerja">Tidak Bekerja</option>'+
				'</select>'+
				'<div class="help-block form-text text-muted form-control-feedback">'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Nama Perusahaan</label>'+
				'<input class="form-control" type="text" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()" required="required">'+
				'<div class="help-block form-text text-muted form-control-feedback"></div>'+
				'</div>'+
				'</div>';
			} else {
				document.getElementById("demo").innerHTML='';
			}
		}
	</script>
	<script>
		function myNumber() {
			document.getElementById("hp_ayah").value = "08";
		}
		function myNumberIbu() {
			document.getElementById("hp_ibu").value = "08";
		}
	</script>
	<script type="text/javascript">

		function myFunction3(e) {
			var x = document.getElementById("mySelect3").value;
			if (x == 'Masih Hidup') {
				document.getElementById("demo3").innerHTML='<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >No Hape Ibu</label>'+
				'<input class="form-control"  pattern=".{0}|.{12,16}" maxlength="16" onclick="myNumberIbu()" id="hp_ibu" type="text" name="fam_no_hp[]" placeholder="No Hape Ibu" data-mask="0000-0000-0000-0" required="required">'+
				'<div class="help-block form-text text-muted form-control-feedback"></div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Usia</label>'+
				'<input class="form-control" type="text" maxlength="3" name="fam_usia[]" onkeypress="return angka(event)" required="required" >'+
				'<div class="help-block form-text text-muted form-control-feedback">'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Pendidikan</label>'+
				'<select name="fam_pen[]" class="form-control" required="required">'+
				'<option value="">Pilih..</option>'+
				'<?php foreach ($pendidikan as $key => $value) { ?>'+
				'<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>'+
				'<?php } ?>'+
				'</select>'+
				'<div class="help-block form-text text-muted form-control-feedback">'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Pekerjaan</label>'+
				'<select name="fam_job[]" class="form-control" id="mySelect4" onchange="myFunction4(event)" required="required" >'+
				'<option value="">Pilih..</option>'+
				'<option value="Bekerja">Bekerja</option>'+
				'<option value="Tidak Bekerja">Tidak Bekerja</option>'+
				'</select>'+
				'<div class="help-block form-text text-muted form-control-feedback">'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="col-md-12">'+
				'<div class="form-group">'+
				'<label >Nama Perusahaan</label>'+
				'<input class="form-control" type="text" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()" required="required">'+
				'<div class="help-block form-text text-muted form-control-feedback"></div>'+
				'</div>'+
				'</div>';
			} else {
				document.getElementById("demo3").innerHTML='';
			}

		}
	</script>
	<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>

	<script>
		function harusHuruf(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode
			if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
				return false;
			return true;
		}
		function angka(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode <33 || charCode > 57) {
				return false;
			}
			return true;
		}
	</script>
