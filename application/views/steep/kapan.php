<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<form class="responsive-height" name="myForm" action="<?= base_url('kapan/create'); ?>" method="POST" style="background: white;" >
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav animated bounceInLeft">
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step active complete"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
			</div>
		</div>

		<div class="col-md-12 animated bounceInRight">
			<div class="form-group">
				<label >Bila diterima, kapan Anda dapat mulai bekerja ?</label>
				<div class="form-group">
					<input type="text" class="form-control" id="datepicker" name="12kapan" required>	

				</div>
				
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	</div>
	<div class="content-box" style="margin-top: 350px;">
		<a href="<?php echo base_url(); ?>gaji" class="btn btn-warning">Back</a>
		<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
	</div>
</form>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#datepicker" ).datepicker({
			dateFormat: "yy-mm-dd",
			minDate: new Date(),
		});
	} );
</script>
