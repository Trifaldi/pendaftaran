<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
<?php if ( $personal->proses_status > 1 ) { ?>
	<form class="responsive-height" name="myForm" action="<?= base_url('skill_bahasa/update'); ?>" method="POST" style="
    background: white;">
<?php } else { ?>
	<form class="responsive-height" name="myForm" action="<?= base_url('skill_bahasa/create'); ?>" method="POST" style="
    background: white;">
<?php } ?>
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav animated bounceInLeft">
				<div class="step active complete" data-form="#form-1"></div>
				<div class="step active complete" data-form="#form-2"></div>
				<div class="step active complete" data-form="#form-3"></div>
				<div class="step active complete" data-form="#form-4"></div>
				<div class="step active complete" data-form="#form-5"></div>
				<div class="step active complete" data-form="#form-6"></div>
				<div class="step active complete" data-form="#form-7"></div>
				<div class="step active complete" data-form="#form-8"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
				<div class="step"></div>
			</div>
		</div>

		<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

			<div class="row">
				<div class="col-md-12">
					<label class="form-group">Pendidikan Non Formal</label>
				</div>
				<div class="col-sm-6 jarak_kanan animated bounceInRight">
					<div class="col-md-12">
						<div class="form-group">
							<div class="form-group row">
							<label class="col-sm-4 control-label">Kursus</label>
							<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
							<input type="hidden" name="kategori[]" value="1">
							<input type="hidden" name="non_type[]" value="khurus">
							<input type="hidden" name="type_bhsasing[]">
							<input type="hidden" name="point[]">
							<div class="col-sm-8">
								<input type="text" name="non_ket[]" class="form-control" placeholder="Keterangan" required="required">
								<br>
								<input type="text" name="non_pilta[]" class="form-control single-daterange" placeholder="Tahun">
							</div>
						</div>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<div class="form-group row">
							<label class="col-sm-4 control-label">Traning</label>
							<input type="hidden" name="type_bhsasing[]">
							<input type="hidden" name="kategori[]" value="2">
							<input type="hidden" name="point[]">
							<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
							<input type="hidden" name="non_type[]" value="traning">
							<div class="col-sm-8">
								<input type="text" name="non_ket[]" class="form-control" placeholder="Keterangan">
								<br>
								<input  type="text" name="non_pilta[]" class="form-control single-daterange " placeholder="Tahun">

							</div>
						</div>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<div class="form-group row">
							<label class="col-sm-4 control-label">Seminar</label>
							<input type="hidden" name="type_bhsasing[]">
							<input type="hidden" name="point[]">
							<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
							<input type="hidden" name="kategori[]" value="3">
							<input type="hidden" name="non_type[]" value="seminar">
							<div class="col-sm-8">
								<input type="text" name="non_ket[]" class="form-control" placeholder="Keterangan">
								<br>
								<input type="text" name="non_pilta[]" class="form-control single-daterange" placeholder="Tahun">

							</div>
						</div>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 jarak_kiri animated bounceInLeft">
					<div class="col-md-12">
						<div class="form-group">
							<label >Bhs. Inggris </label>
							<input class="form-control" type="text"  value="bahasa inggris" readonly>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label >Comunication</label>
							<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
							<input type="hidden" name="type_bhsasing[]" value="komunication">
							<input type="hidden" name="kategori[]" value="4">
							<input type="hidden" name="non_ket[]">
							<input type="hidden" name="non_pilta[]">
							<input class="form-control" type="hidden" name="non_type[]" value="bahasa inggris" readonly>
							<select class="form-control" name="point[]" required>
								<option value="">Pilih...</option>
								<option value="Sangat Baik">Sangat Baik</option>
								<option value="Baik">Baik</option>
								<option value="Cukup">Cukup</option>
								<option value="Kurang">Kurang</option>
							</select>
							
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
						<div class="form-group">
							<label >Read</label>
							<input type="hidden" name="kategori[]" value="5">
							<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
							<input type="hidden" name="type_bhsasing[]" value="Read">
							<input type="hidden" name="non_ket[]">
							<input type="hidden" name="non_pilta[]">
							<input class="form-control" type="hidden" name="non_type[]" value="bahasa inggris" readonly>
							<select class="form-control" name="point[]" required>
								<option value="">Pilih...</option>
								<option value="Sangat Baik">Sangat Baik</option>
								<option value="Baik">Baik</option>
								<option value="Cukup">Cukup</option>
								<option value="Kurang">Kurang</option>
							</select>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
						<div class="form-group">
							<label >Write</label>
							<input type="hidden" name="kategori[]" value="6">
							<input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe') ?>">
							<input type="hidden" name="type_bhsasing[]" value="Write">
							<input type="hidden" name="non_ket[]">
							<input type="hidden" name="non_pilta[]">
							<input class="form-control" type="hidden" name="non_type[]" value="bahasa inggris" readonly>
							<select class="form-control" name="point[]" required>
								<option value="">Pilih...</option>
								<option value="Sangat Baik">Sangat Baik</option>
								<option value="Baik">Baik</option>
								<option value="Cukup">Cukup</option>
								<option value="Kurang">Kurang</option>
							</select>
							<div class="help-block form-text text-muted form-control-feedback"></div>
						</div>
					</div>

				</div>
			</div>


		</div>	
	</div>
	<div class="content-box">
		<a href="<?php echo base_url(); ?>pendidikan" class="btn btn-warning">Back</a>
		<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
	</div>
</form>
