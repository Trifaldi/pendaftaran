<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
    $this->load->model('M_personal');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{

		$uniqe = $this->session->userdata('uniqe');
    $this->load->helper('url');
    $data ['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
    $data ['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
    $data ['subview'] = 'steep/pendidikan';
  	$this->load->view('step',$data);


	}

	public function create()
	{		
		$check_id = $this->session->userdata('uniqe');
		$id_unix['unix'] = $this->db->query("SELECT * FROM p_education WHERE id_unix = '$check_id'")->row();
		$data = array(
			'id_unix'		=> $this->session->userdata('uniqe'),
			'penter' 		=> $this->input->post('penter'), 
			'sek_penter'  	=> $this->input->post('sek_penter'),
			'jur_penter'  	=> $this->input->post('jur_penter'),
			'drth_penter'  	=> $this->input->post('drth_penter'),
			'sata_penter'	=> $this->input->post('sata_penter'),
			'pense'  		=> $this->input->post('pense'),
			'sek_pense' 	=> $this->input->post('sek_pense'),
			'srth_pense' 	=> $this->input->post('srth_pense'),
			'sata_pense'  	=> $this->input->post('sata_pense'),
		);
		//log_r($check_unix);
		//$this->load->model('M_personal');
		if ($check_id == $id_unix['unix']->id_unix) {
			$insert = $this->db->update('p_education', $data, ['id_unix' => $check_id]);
		} else {
			$insert = $this->db->insert('p_education',$data);
		}
		
		
		if ($insert == true ) {
			redirect('skill_bahasa');
		}
	}

}
