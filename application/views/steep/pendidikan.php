<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
<script type="text/javascript" src="<?= base_url();?>assets/datetimepicker/bootstrap-datetimepicker.js"></script>
<link rel="stylesheet" href="<?= base_url();?>assets/datetimepicker/bootstrap-datetimepicker.css"/>
<form class="responsive-height" name="myForm" action="<?= base_url('pendidikan/create'); ?>" method="POST" style="
background: white;">

<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
	<div class="col-md-12">
		<div class="form-wizard-nav animated bounceInLeft">
			<div class="step active complete" data-form="#form-1"></div>
			<div class="step active complete" data-form="#form-2"></div>
			<div class="step active complete" data-form="#form-3"></div>
			<div class="step active complete" data-form="#form-4"></div>
			<div class="step active complete" data-form="#form-5"></div>
			<div class="step active complete" data-form="#form-6"></div>
			<div class="step active complete" data-form="#form-7"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
			<div class="step"></div>
		</div>
	</div>

	<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

		<div class="row">
			<div class="col-sm-6 jarak_kanan animated bounceInRight" >
				<div class="col-md-12">
					<div class="form-group">
						<label >Pendidikan Terakhir</label>
						<select name="penter" class="form-control select" id="mySelect" required onchange="myFunction(event)" >
							<option value="">Pilih</option>								
							<?php foreach ($pendidikan as $key => $value) { ?>
								<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
							<?php } ?>
						</select>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label >Nama Sekolah</label>
						<input class="form-control" type="text" name="sek_penter" required>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
					<div class="form-group">
						<label >Jurusan</label>
						<input class="form-control" type="text" name="jur_penter">
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
					<div class="form-group">
						<label >Dari Tahun</label>
						<input class="form-control" type="text" id="datepicker" name="drth_penter" required>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
					<div class="form-group">
						<label >Sampai Dengan</label>
						<input class="form-control" id="datepicker2" type="text" name="sata_penter" required>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 jarak_kiri animated bounceInLeft">
				<div class="col-md-12">
					<div class="form-group">
						<label >Pendidikan Sebelumnya</label>
						<select name="pense" class="form-control select" id="mySelect3" onchange="myFunction3(event)" required="required">
							<option value="">Pilih..</option>
							<?php foreach ($pendidikan as $key => $value) { ?>
								<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
							<?php } ?>
						</select>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label >Nama Sekolah</label>
						<input class="form-control" type="text" name="sek_pense" required>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
					<div class="form-group">
						<label >Dari Tahun</label>
						<input class="form-control" type="text" id="datepicker3" name="srth_pense" required>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
					<div class="form-group">
						<label >Sampai Dengan</label>
						<input class="form-control" type="text" id="datepicker4" name="sata_pense" required>
						<div class="help-block form-text text-muted form-control-feedback"></div>
					</div>
				</div>

			</div>
		</div>


	</div>
</div>
<div class="content-box">
	<a href="<?php echo base_url(); ?>emergency" class="btn btn-warning">Back</a>
	<button  class="tombol-simpan1 btn btn-success pull-right submit" type="submit" name="submit">Next</button>
</div>
</form>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').datetimepicker({
			format: 'YYYY-MM-DD',
		})
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker2').datetimepicker({
			format: 'YYYY-MM-DD',
		})
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker3').datetimepicker({
			format: 'YYYY-MM-DD',
		})
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker4').datetimepicker({
			format: 'YYYY-MM-DD',
		})
	});
</script>