$(document).ready(function() {
    $('.summernote').summernote({
        height: 300,
        toolbar: [
            //[groupname, [button list]]

            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['picture']],
            ['table', ['table']],
            ['misc', ['undo', 'redo', 'codeview', 'fullscreen']]
        ]
    });
});