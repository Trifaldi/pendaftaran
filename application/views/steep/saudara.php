<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css">
<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
  <div class="col-md-12">
   <div class="form-wizard-nav animated bounceInLeft">
    <div class="step active complete" data-form="#form-1"></div>
    <div class="step active complete" data-form="#form-2"></div>
    <div class="step active complete"></div>
    <div class="step active complete"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
    <div class="step"></div>
  </div>
</div>

<div class="col-md-12" id="form-1" style="display: block;padding-left: 50px;padding-right: 50px;">

 <div class="row">
  <div class="col-sm-6 jarak_kanan animated bounceInRight">
    <form class="responsive-height" action="javascript:void(0)" method="POST" id="form-user4">
      <div class="col-md-12">
        <div class="form-group">
          <label >Nama Saudara</label>
          <input type="hidden" id="id_unix" name="id_unix" value="<?php echo $this->session->userdata('uniqe');?>">
          <input type="hidden" name="fam_kategori" value="4">
          <input type="hidden" id="fam_ket" name="fam_ket" value="Saudara">
          <input type="hidden" id="id_fam" name="id_fam">
          <input class="form-control" type="text" id="fam_nama" name="fam_nama" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return harusHuruf('event')">
          <div class="help-block form-text text-muted form-control-feedback"></div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label >Jenis Kelamin</label>
          <select name="fam_jekel" class="form-control" id="fam_jekel">
            <option>Pilih..</option>
            <option value="Pria">Pria</option>
            <option value="Wanita">Wanita</option>
          </select>
          <div class="help-block form-text text-muted form-control-feedback"></div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label >No Hp Saudara</label>
          <input type="text" name="fam_no_hp" id="fam_no_hp" class="form-control" maxlength="16" onkeypress="return angka(event)">
          <div class="help-block form-text text-muted form-control-feedback"></div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label >Usia</label>
          <input type="text" name="fam_usia" id="fam_usia" class="form-control" maxlength="3">
          <div class="help-block form-text text-muted form-control-feedback"></div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label >Pendidikan</label>
          <select class="form-control" name="fam_pen" id="fam_pen">
            <option>Pilih..</option>
            <?php foreach ($pendidikan as $key => $value) { ?>
             <option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
           <?php } ?>
         </select>
         <div class="help-block form-text text-muted form-control-feedback"></div>
       </div>
     </div>
     <div class="col-md-12">
      <div class="form-group">
        <label >Job</label>
        <select class="form-control" onchange="myFunctionSaudara(event)" id="fam_job" name="fam_job">
          <option>Pilih..</option>
          <option value="Bekerja">Bekerja</option>
          <option value="Tidak Bekerja">Tidak Bekerja</option>
        </select>
        <div class="help-block form-text text-muted form-control-feedback"></div>
      </div>
    </div>
    <p id="suadara"></p>

    <button type="button" type="submit" id="btn_save" class="btn btn-primary pull-right">Save</button>

  </form>
</div>
<div class="col-sm-6 jarak_kiri animated bounceInLeft">
  <style type="text/css">
    thead{
      background: #4dc3ff;
    }
  </style>
  <table class="table" id="mydata">
    <thead>
      <tr>
        <th>Nama Saudara</th>
        <th>No Handohone</th>
        <th>Jenis Kelamin</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody id="show_data">

    </tbody>
  </table>

</div>
</div>


</div>
<div class="content-box-footer">
  <a href="<?php echo base_url(); ?>orangtua" class="btn btn-warning">Back</a>
  <a href="<?php echo base_url(); ?>keluarga" id="btn_next" class="btn btn-success pull-right">Next</a>
</div>
</div>
<!--MODAL DELETE-->
<!--END MODAL DELETE-->
<script>
  $(document).ready(function(){
    <?php if ($saudara->fam_kategori == '4') {?>
      $('#btn_next').show();
    <?php } else { ?>
      $('#btn_next').hide();
    <?php } ?>
    
    $("#btn_save").click(function(){
      $('#btn_next').show();
    });
  });
</script>
<script type="text/javascript">

  function myFunctionSaudara(e) {
    var x = document.getElementById("fam_job").value;
    if (x == 'Bekerja') {
      document.getElementById("suadara").innerHTML='<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Nama Perusahaan</label>'+
      '<input class="form-control" type="text" id="fam_per" name="fam_per" onkeyup="this.value = this.value.toUpperCase()">'+
      '<div class="help-block form-text text-muted form-control-feedback"></div>'+
      '</div>'+
      '</div>';
    } else {
      document.getElementById("suadara").innerHTML='';
    }

  }

</script>
<script type="text/javascript">
  $('#fam_no_hp').click(function(event) {
    var val = $('#fam_no_hp').val();
    if(val==''){
      $('#fam_no_hp').val('08');
    }
  });
  $('#fam_no_hp').focus(function(event) {
    $('#fam_no_hp').val();
  });

  $("#fam_no_hp").focusout(function(){
    $('#fam_no_hp').val();
  });

  $("#fam_no_hp").keyup(function(){
    var val = $('#fam_no_hp').val();
  //var valx = '085834246342';
  var valx = val.replace(/-/gi, "");
  console.log(valx);
  arr = valx.match(/.{1,4}/g);

  //bersihkan array jika ada yg undefined
  var i = 0;
  var string = "";
  if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
    string = arr[0];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
    string = arr[0]+"-"+arr[1];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
    string = arr[0]+"-"+arr[1]+"-"+arr[2];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
    string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  }


  //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  console.log(string);
  $('#fam_no_hp').val('');
  $('#fam_no_hp').val(string);
  if(val=='08'){
    $('#fam_no_hp').val('08');
  }else if(val=='0'){
    $('#fam_no_hp').val('08');
  }else if(val==''){
    $('#fam_no_hp').val('08');
  }
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    show_all();

    //$('#mydata').dataTable();

    function show_all(){
      $.ajax({
        type : 'ajax',
        url  : '<?php echo base_url();?>saudara/get',
        async : false,
        dataType : 'json',
        success  : function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<tr>'+
            '<td>'+data[i].fam_nama+'</td>'+
            '<td>'+data[i].fam_no_hp+'</td>'+
            '<td style="text-align:center;">'+data[i].fam_jekel+'</td>'+
            '<td style="text-align:center;">'+
            '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-id_fam="'+data[i].id_fam+'" data-fam_nama="'+data[i].fam_nama+'" data-fam_no_hp="'+data[i].fam_no_hp+'" data-fam_usia="'+data[i].fam_usia+'" data-fam_jekel="'+data[i].fam_jekel+'" data-fam_pen="'+data[i].fam_pen+'" data-fam_job="'+data[i].fam_job+'" data-fam_per="'+data[i].fam_per+'"><i class = "fa fa-arrows-alt"></i></a>'+' '+
            '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id_fam="'+data[i].id_fam+'"><i class = "fa fa-trash"></i></a>'+
            '</td>'+
            '</tr>';
          }
          $('#show_data').html(html);
        }
      });
    }

    $('#btn_save').click(function(){
      var data = $('#form-user4').serialize();

      $.ajax({
        url: '<?php echo base_url('saudara/save');?>',
        type: 'POST',
        async: false,
        dataType: 'JSON',
        data: data,
        success: function(data){
          $('[name="fam_nama"]').val("");
          $('[name="fam_jekel"]').val("");
          $('[name="fam_no_hp"]').val("");
          $('[name="fam_usia"]').val("");
          $('[name="fam_pen"]').val("");
          $('[name="fam_job"]').val("");
          $('[name="fam_per"]').val("");
          show_all();
        }
      });
      return false;
    });

    $('#show_data').on('click','.item_edit',function(){
      var id_fam = $(this).data('id_fam');
      var fam_nama = $(this).data('fam_nama');
      var fam_jekel = $(this).data('fam_jekel');
      var fam_no_hp = $(this).data('fam_no_hp');
      var fam_usia = $(this).data('fam_usia');
      var fam_pen = $(this).data('fam_pen');
      var fam_job = $(this).data('fam_job');
      var fam_per = $(this).data('fam_per');

      $('#form-user4').serialize('show');
      $('[name="id_fam"]').val(id_fam);
      $('[name="fam_nama"]').val(fam_nama);
      $('[name="fam_jekel"]').val(fam_jekel);
      $('[name="fam_no_hp"]').val(fam_no_hp);
      $('[name="fam_usia"]').val(fam_usia);
      $('[name="fam_pen"]').val(fam_pen);
      $('[name="fam_job"]').val(fam_job);
      $('[name="fam_per"]').val(fam_per);
    });


    $('#show_data').on('click','.item_delete',function(){
      var id_fam = $(this).data('id_fam');

      $('#Modal_Delete').modal('show');
      $('[name="id_fam"]').val(id_fam);
    });

        //delete record to database
        $('#btn_delete').on('click',function(){
          var id_fam = $('#id_fam').val();
          $.ajax({
            type : "POST",
            url  : "<?php echo site_url('saudara/delete')?>",
            dataType : "JSON",
            data : {id_fam:id_fam},
            success: function(data){
              $('[name="id_fam"]').val("");
              $('#Modal_Delete').modal('hide');
              show_all();
            }
          });
          return false;
        });

      });
    </script>
    <script type="text/javascript">
      $(document).ready(function() {

        $('#form-user4').submit(function(e) {
         e.preventDefault();
         var first_name = $('#fam_nama').val();

         $(".error").remove();

         if (first_name.length < 1) {
          $('#fam_nama').after('<span class="error">This field is required</span>');
        }

      });

      });

    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
